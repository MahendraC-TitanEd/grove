####################################################################################################
## VPC: A virtual private cloud which will hold everything else
####################################################################################################
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.1.1"

  name = "${var.cluster_name} VPC"
  cidr = "10.0.0.0/16"

  # "Amazon EKS requires subnets in at least two Availability Zones."
  azs = data.aws_availability_zones.available.names
  # "We recommend a VPC with public and private subnets so that Kubernetes can create public load balancers in the
  #  public subnets that load balance traffic to pods running on nodes that are in private subnets."
  # https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  enable_vpn_gateway = false

  # "Your VPC must have DNS hostname and DNS resolution support. Otherwise, your nodes cannot register with your
  #  cluster."
  # https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(local.default_tags, {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  })

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                    = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"           = "1"
  }
}

####################################################################################################
## Security Groups
####################################################################################################

# Allow SSH into the worker nodes from any other host in the VPC (such as a bastion)
resource "aws_security_group" "k8s_worker_node_ssh_access" {
  name_prefix = "${var.cluster_name}_worker_ssh"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
  tags = local.default_tags
}

resource "aws_security_group_rule" "webhook_admission_inbound" {
  type                     = "ingress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  security_group_id        = module.eks.node_security_group_id
  source_security_group_id = module.eks.cluster_primary_security_group_id
}
resource "aws_security_group_rule" "webhook_admission_outbound" {
  type                     = "egress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  security_group_id        = module.eks.node_security_group_id
  source_security_group_id = module.eks.cluster_primary_security_group_id
}
