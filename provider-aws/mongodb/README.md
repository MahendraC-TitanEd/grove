# mongodb

This module provisions an AWS MongoDB Atlas cluster.

## Environment Variables

- `MONGODB_ATLAS_PUBLIC_KEY`: MongoDB Atlas public key
- `MONGODB_ATLAS_PRIVATE_KEY`: MongoDB Atlas private key

## Variables

### Required

- `mongodbatlas_project_id`: MongoDB Atlas Project ID
- `mongodbatlas_cluster_name`: MongoDB Atlas cluster name

### Provided by Grove

- `cluster_name`: Grove cluster name
- `region_name`: AWS region

- `tutor_instances`: List of tutor instances passed by Grove. Should not be set explicitly.

### Optional

- `provider_name`: The cloud provider to deploy the cluster. Defaults to `AWS`.
- `mongodb_version`: The MongoDB database version. Defaults to `4.4`.
- `instance_size`: The Atlas instance size. Defaults to `M10`.
- `cluster_type`: The Atlas cluster type. Possible values are `REPLICASET`, `SHARDED`, `GEOSHARDED`. Usable values are subject to `instance_size` limitations. Defaults to `REPLICASET`.
- `num_shards`: The number of replica shards. Replication is subject to `instance_size` limitations. Defaults to `1`.
- `electable_nodes`: The number of nodes that can be elected master node. Defaults to `3`.
- `read_only_nodes`: The number of read-only nodes. Disabled (`null`) by default.
- `analytics_nodes`: The number of analytics nodes. Disabled (`null`) by default.
- `backup_enabled`: Enables Cloud Backup functionality. Defaults to `true`.
- `encryption_at_rest`: Only required if using custom encryption keys for the Atlas project. Atlas already encrypts data at rest by default. Defaults to `false`.
- `disk_size_gb`: Custom disk size for the cluster nodes, in gigabytes. Defaults to `null`, which uses the default disk size given by the current `instance_size`. Must be between 10 and 4096. Can't be used in instances with NVME SSDs.
- `disk_iops`: Custom disk IOPS. Defaults to `null`, which uses the default IOPS for the current `instance_size`. Possible values are subject to `instance_size` limitations. Can't be used in instances with NVME SSDs.
- `volume_type`: Storage volume type. Possible values are `STANDARD` and `PROVISIONED`. Only required when `disk_iops` is set to a higher value than the default.
- `auto_scaling_disk_gb_enabled`: Enables auto-scaling the disk size. Defaults to `true`.
- `auto_scaling_compute_enabled`: Enables auto-scaling the compute nodes. Defaults to `false`.
- `auto_scaling_min_instances`: Minimum number of nodes to use when `auto_scaling_compute_enabled` is set to `true`.
- `auto_scaling_max_instances`: Maximum number of nodes to use when `auto_scaling_compute_enabled` is set to `true`.

## Outputs

- `connection_string`: The URL for clients to connect to the cluster.
- `instances`: A mapping from each Tutor instance name to a unique `database`, `username` and `password`.
