output "connection_string" {
  value = mongodbatlas_cluster.cluster.connection_strings[0].standard_srv
}

output "instances" {
  sensitive = true
  value = {
    for instance, user in var.users :
    instance => {
      username       = user.username
      password       = try(mongodbatlas_database_user.users[user.username].password, "")
      database       = user.database
      forum_database = "${instance}-cs_comments_service"
    }
  }
}
