module "plugin" {
  source = "../provider-plugin"

  provider_region = var.aws_region

  cluster_name   = var.cluster_name
  cluster_domain = var.cluster_domain
  cluster_arn    = module.eks.cluster_arn
  cluster_id     = module.eks.cluster_id
  cluster_config = data.external.kubeconfig.result.output

  vpc_id = module.vpc.vpc_id

  tutor_instances        = var.tutor_instances
  tutor_instance_buckets = { for instance in var.tutor_instances : instance => module.instance_edxapp_bucket[instance] }

  gitlab_cluster_agent_token         = var.gitlab_cluster_agent_token
  gitlab_group_deploy_token_username = var.gitlab_group_deploy_token_username
  gitlab_group_deploy_token_password = var.gitlab_group_deploy_token_password

  configs              = module.edx_instances.configs
  allowed_cors_origins = module.edx_instances.allowed_cors_origins
}
