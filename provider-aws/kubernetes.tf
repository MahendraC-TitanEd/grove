####################################################################################################
## The Kubernetes Cluster itself
####################################################################################################

locals {
  cluster_provider = "aws"
  cluster_version  = "1.27"

  autoscaler_chart_version = "9.29.2" # https://github.com/kubernetes/autoscaler/releases
  autoscaler_role_name     = "cluster-autoscaler-${var.cluster_name}"

  velero_aws_plugin_tag = "v1.7.1" # https://github.com/vmware-tanzu/velero/releases

  default_tags = {
    ManagedBy = "Grove"
    Terraform = "true"
  }
}

# The EKS module doesn't generate a kubeconfig anymore. It's
# up to the user to manage.
# https://github.com/terraform-aws-modules/terraform-aws-eks/blob/681e00aafea093be72ec06ada3825a23a181b1c5/docs/UPGRADE-18.0.md#list-of-backwards-incompatible-changes
data "external" "kubeconfig" {
  depends_on = [module.eks.cluster_id]
  program = [
    "/bin/sh",
    "${path.module}/kube-config.sh",
    module.eks.cluster_id,
    var.aws_region
  ]
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu-eks/k8s_${local.cluster_version}/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# Declare the kubeconfig as an output - access it anytime with "/tf output -raw kubeconfig"
output "kubeconfig" {
  value     = data.external.kubeconfig.result.output
  sensitive = true
}

module "ebs_csi_irsa_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name             = "${var.cluster_name}-ebs-csi"
  attach_ebs_csi_policy = true

  oidc_providers = {
    ex = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:ebs-csi-controller-sa"]
    }
  }
}

module "eks" {
  source                    = "terraform-aws-modules/eks/aws"
  version                   = "~> 18.31.2"
  cluster_name              = var.cluster_name
  cluster_version           = local.cluster_version
  subnet_ids                = module.vpc.private_subnets
  vpc_id                    = module.vpc.vpc_id
  enable_irsa               = true # Required for autoscaling
  manage_aws_auth_configmap = true
  create_aws_auth_configmap = true

  cluster_addons = {
    "aws-ebs-csi-driver" = {
      resolve_conflicts        = "OVERWRITE"
      service_account_role_arn = module.ebs_csi_irsa_role.iam_role_arn
    }
  }

  # Extend cluster security group rules
  cluster_security_group_additional_rules = {
    egress_nodes_ephemeral_ports_tcp = {
      description                = "To node 1025-65535"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }

  # Extend node-to-node security group rules
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }

    ingress_4443_from_controlplane = {
      description                   = "Cluster API to Nodegroup for metrics server"
      protocol                      = "-1"
      from_port                     = 0
      to_port                       = 4443
      type                          = "ingress"
      source_cluster_security_group = true
    }

    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  self_managed_node_group_defaults = {
    vpc_security_group_ids       = [aws_security_group.k8s_worker_node_ssh_access.id]
    iam_role_additional_policies = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
    autoscaling_group_tags = {
      "k8s.io/cluster-autoscaler/enabled" : true,
      "k8s.io/cluster-autoscaler/${var.cluster_name}" : "owned",
    }
  }

  self_managed_node_groups = {
    worker_group = {
      name = "${var.cluster_name}-workers"

      ami_id        = var.ami_id == "" ? data.aws_ami.ubuntu.id : var.ami_id
      min_size      = var.min_worker_node_count
      max_size      = var.max_worker_node_count
      desired_size  = var.min_worker_node_count
      instance_type = var.worker_node_size

      bootstrap_extra_args = "--kubelet-extra-args '--node-labels=node.kubernetes.io/lifecycle=spot'"
    }
  }

  tags = local.default_tags
}

####################################################################################################
## Cluster Autoscaler - this scales the nodes up and down as needed
####################################################################################################

# based on IRSA example, https://github.com/terraform-aws-modules/terraform-aws-eks/tree/master/examples/irsa

module "iam_assumable_role_admin" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "3.13.0"
  create_role                   = true
  role_name                     = local.autoscaler_role_name
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.cluster_autoscaler.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:${local.autoscaler_role_name}"]

  tags = local.default_tags
}

resource "aws_iam_policy" "cluster_autoscaler" {
  name_prefix = "cluster-autoscaler"
  description = "EKS cluster-autoscaler policy for cluster ${module.eks.cluster_id}"
  policy      = data.aws_iam_policy_document.cluster_autoscaler.json

  tags = local.default_tags
}

# Permissions required for the autoscaler
data "aws_iam_policy_document" "cluster_autoscaler" {
  statement {
    sid    = "clusterAutoscalerAll"
    effect = "Allow"
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "ec2:DescribeLaunchTemplateVersions",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "clusterAutoscalerOwn"
    effect = "Allow"

    actions = [
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "autoscaling:UpdateAutoScalingGroup",
    ]

    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/kubernetes.io/cluster/${module.eks.cluster_id}"
      values   = ["owned"]
    }

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
      values   = ["true"]
    }
  }
}

# Get the account ID, required for the helm chart below
data "aws_caller_identity" "current" {}

resource "helm_release" "k8s_autoscaler" {
  name       = "cluster-autoscaler"
  repository = "https://kubernetes.github.io/autoscaler"
  chart      = "cluster-autoscaler"
  version    = local.autoscaler_chart_version
  namespace  = "kube-system"
  depends_on = [
    module.eks.cluster_id,
    aws_iam_policy.cluster_autoscaler,
    module.iam_assumable_role_admin
  ]

  # Configure the helm chart:
  set {
    name  = "awsRegion"
    value = var.aws_region
  }
  set {
    name  = "rbac.create"
    value = true
  }
  set {
    name  = "rbac.serviceAccount.name"
    value = local.autoscaler_role_name
  }
  set {
    name  = "rbac.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${local.autoscaler_role_name}"
  }
  set {
    name  = "autoDiscovery.clusterName"
    value = var.cluster_name
  }
  set {
    name  = "autoDiscovery.enabled"
    value = true
  }
}

####################################################################################################
## Kubernetes Dashboard (and metrics server for autoscaling)
####################################################################################################

module "k8s_dashboard" {
  depends_on           = [helm_release.k8s_autoscaler]
  source               = "../provider-modules/k8s-dashboard"
  service_account_name = "eks-admin"
}

####################################################################################################
## Integrate kubernetes cluster with gitlab project
####################################################################################################

module "gitlab" {
  depends_on                 = [helm_release.k8s_autoscaler]
  source                     = "../provider-modules/k8s-gitlab-connector"
  gitlab_cluster_agent_token = var.gitlab_cluster_agent_token
}

####################################################################################################
## Create k8s secret for GitLab container registry access
####################################################################################################

module "k8s_gitlab_container_registry" {
  depends_on = [helm_release.k8s_autoscaler]
  for_each   = toset(var.tutor_instances)

  source = "../provider-modules/k8s-gitlab-container-registry"

  namespace                          = each.key
  container_registry_server          = var.container_registry_server
  dependency_proxy_server            = var.dependency_proxy_server
  gitlab_group_deploy_token_username = var.gitlab_group_deploy_token_username
  gitlab_group_deploy_token_password = var.gitlab_group_deploy_token_password
}


####################################################################################################
## Create ingress controller
####################################################################################################

module "ingress" {
  depends_on           = [helm_release.k8s_autoscaler]
  source               = "../provider-modules/k8s-nginx-ingress"
  ingress_namespace    = "kube-system"
  cluster_provider     = local.cluster_provider
  cluster_domain       = var.cluster_domain
  global_404_html_path = var.global_404_html_path
}

####################################################################################################
## Create monitoring pods
####################################################################################################

module "k8s_monitoring" {
  depends_on                         = [module.ingress, kubectl_manifest.cert_manager]
  source                             = "../provider-modules/k8s-monitoring"
  alert_manager_config               = var.alert_manager_config
  cluster_domain                     = var.cluster_domain
  enable_monitoring_ingress          = var.enable_monitoring_ingress
  fluent_bit_aws_config              = yamldecode(var.fluent_bit_aws_config)
  fluent_bit_aws_kube_log_config     = yamldecode(var.fluent_bit_aws_kube_log_config)
  fluent_bit_aws_tracking_log_config = yamldecode(var.fluent_bit_aws_tracking_log_config)
  opensearch_index_retention_days    = var.opensearch_index_retention_days
  opensearch_persistence_size        = var.opensearch_persistence_size
}

# The OpenSearch dashboard password - access it with "/tf output -raw opensearch_dashboard_admin_password"
output "opensearch_dashboard_admin_password" {
  value     = module.k8s_monitoring.opensearch_dashboard_admin_password
  sensitive = true
}

# The monitoring basic auth password - access it with "/tf output -raw monitoring_ingress_password"
output "monitoring_ingress_password" {
  value     = module.k8s_monitoring.monitoring_ingress_password
  sensitive = true
}

####################################################################################################
## Cert manager resources
####################################################################################################

# If using the kubectl_manifest resource then we get an error
# every time we do a `terraform plan`:
# No matches for kind "ClusterIssuer" in group "cert-manager.io"
# See https://github.com/hashicorp/terraform-provider-kubernetes/issues/1367
resource "kubectl_manifest" "cert_manager" {
  yaml_body = templatefile("${path.module}/assets/kubernetes/cluster-issuer.yml", {
    namespace          = "kube-system",
    notification_email = var.lets_encrypt_notification_inbox
  })
}


####################################################################################################
## Create OpenFAAS resources
####################################################################################################

module "k8s_openfaas" {
  count          = var.enable_openfaas ? 1 : 0
  source         = "../provider-modules/k8s-openfaas"
  depends_on     = [module.vpc]
  cluster_domain = var.cluster_domain
}

output "openfaas_ingress_password" {
  value     = module.k8s_openfaas
  sensitive = true
}

####################################################################################################
## Create metrics server resources
####################################################################################################

module "k8s_metrics_server" {
  source               = "../provider-modules/k8s-metrics-server"
  depends_on           = [module.vpc]
  service_account_name = "metrics-server"
}


####################################################################################################
## Shared Elasticsearch resources
####################################################################################################

module "k8s_elasticsearch" {
  source               = "../provider-modules/k8s-elasticsearch"
  depends_on           = [module.vpc]
  tutor_instances      = var.tutor_instances
  count                = var.enable_shared_elasticsearch ? 1 : 0
  elasticsearch_config = yamldecode(var.elasticsearch_config)
}

output "elasticsearch" {
  value     = module.k8s_elasticsearch
  sensitive = true
}

####################################################################################################
## Velero backup resources
####################################################################################################

module "k8s_velero" {
  source = "../provider-modules/k8s-backup"
  configuration = [
    templatefile("${path.module}/assets/velero/velero.yml", {
      accessKeyID          = var.aws_access_key
      secretAccessKey      = var.aws_secret_access_key
      backupStorageBucket  = module.velero_backups.s3_bucket.id
      backupStorageRegion  = module.velero_backups.s3_bucket.region
      volumeSnapshotRegion = module.velero_backups.s3_bucket.region
      awsPluginTag         = local.velero_aws_plugin_tag
    })
  ]
}
