resource "random_string" "rds_root_username" {
  length  = 16
  special = false
  numeric = false
}

resource "random_password" "rds_root_password" {
  length           = 32
  special          = true
  override_special = "!#%&*()-_=+[]{}<>:?" # removes @ from special char list
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "${var.cluster_name} rds subnet group"
  subnet_ids = var.rds_subnet_ids

  tags = merge(var.default_tags, {
    name = "${var.cluster_name} rds subnet group"
  })
}

resource "aws_security_group" "rds_security_group" {
  name   = "${var.cluster_name} rds security group"
  vpc_id = var.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = var.ingress_cidr_blocks
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.default_tags
}


resource "aws_kms_key" "rds_encryption" {
  count       = var.rds_storage_encrypted ? 1 : 0
  description = "${title(var.cluster_name)} RDS Encryption"
}

resource "random_string" "rds_final_snapshot_suffix" {
  length  = 16
  special = false
  numeric = false
}

resource "aws_db_instance" "rds_instance" {
  lifecycle {
    ignore_changes = [
      final_snapshot_identifier,
    ]
  }

  identifier             = var.rds_identifier
  allocated_storage      = var.rds_min_storage
  max_allocated_storage  = var.rds_max_storage
  engine                 = "mysql"
  engine_version         = var.rds_mysql_version
  ca_cert_identifier     = var.rds_ca_cert_identifier
  instance_class         = var.rds_instance_class
  username               = random_string.rds_root_username.result
  password               = random_password.rds_root_password.result
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds_security_group.id]

  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false

  backup_retention_period = var.rds_backup_retention_period

  storage_encrypted = var.rds_storage_encrypted
  kms_key_id        = var.rds_storage_encrypted ? aws_kms_key.rds_encryption[0].arn : ""

  final_snapshot_identifier = "${var.cluster_name}-db-final-snapshot-${random_string.rds_final_snapshot_suffix.result}"

  tags = var.default_tags
}
