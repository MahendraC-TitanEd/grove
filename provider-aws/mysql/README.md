# mysql

This module provisions an AWS RDS MySQL instance.

## Variables

- `cluster_name`: Grove cluster name
- `rds_subnet_ids`: List of subnets for RDS
- `vpc_id`: VPC id for RDS
- `rds_identifier`: The identifier of the RDS instance.
- `rds_instance_class`: RDS instance class. Defaults to `db.t3.micro`. Check [here](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html) for more.
- `rds_mysql_version`: MySQL version, defaults to `8.0`
- `rds_min_storage`: Minimum storage size for RDS. Defaults to 10 GB.
- `rds_max_storage`: Maximum storage size for RDS. Defaults to 15 GB.
- `rds_ca_cert_identifier`: The identifier of the CA certificate for the RDS instance.
- `ingress_cidr_blocks`: List of CIDR blocks to be added in ingress rule of the RDS instance.
- `rds_backup_retention_period`: The backup retention period. Defaults to `35`.
- `rds_storage_encrypted`: Specifies whether the DB instance is encrypted. If this set to `true`, it will create a `aws_kms_key` resource and use that to encrypt the DB instance. Defaults to `true`.

## Outputs

- `rds_instance` - Result of [`aws_db_instance`](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance#attributes-reference).
- `root_username`: RDS root username
- `root_password`: RDS root password
- `host`: RDS host address
- `port`: RDS port
- `mysql_endpoint`: RDS endpoint - format - `<RDS_HOST>:<RDS_PORT>`
