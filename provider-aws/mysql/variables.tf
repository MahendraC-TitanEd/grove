variable "default_tags" {
  type        = map(any)
  description = "Map of Default Tags"
}

variable "cluster_name" { type = string }

variable "rds_subnet_ids" { type = list(string) }
variable "ingress_cidr_blocks" { type = list(string) }

variable "vpc_id" { type = string }

variable "rds_instance_class" { type = string }
variable "rds_mysql_version" { type = string }
variable "rds_min_storage" { type = number }
variable "rds_max_storage" { type = number }
variable "rds_ca_cert_identifier" { type = string }

variable "rds_backup_retention_period" { type = number }

variable "rds_storage_encrypted" { type = bool }

variable "rds_identifier" { type = string }

