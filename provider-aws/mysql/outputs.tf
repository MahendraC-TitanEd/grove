output "rds_instance" {
  value     = aws_db_instance.rds_instance
  sensitive = true
}

output "root_username" {
  value     = random_string.rds_root_username.result
  sensitive = true
}

output "root_password" {
  value     = random_password.rds_root_password.result
  sensitive = true
}

output "host" {
  value = aws_db_instance.rds_instance.address
}

output "port" {
  value = aws_db_instance.rds_instance.port
}

output "mysql_endpoint" {
  value = aws_db_instance.rds_instance.endpoint
}
