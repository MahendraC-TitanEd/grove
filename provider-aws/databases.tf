####################################################################################################
## AWS MongoDB Atlas
####################################################################################################

module "mongodb" {
  source = "./mongodb"

  mongodbatlas_project_id   = var.mongodbatlas_project_id
  mongodbatlas_cluster_name = var.mongodbatlas_cluster_name != "" ? var.mongodbatlas_cluster_name : var.cluster_name

  cluster_name = var.cluster_name
  region_name  = var.aws_region

  users = {
    for instance in toset(var.tutor_instances) :
    instance => {
      username       = try(module.edx_instances.configs[instance]["MONGODB_USERNAME"], instance)
      database       = try(module.edx_instances.configs[instance]["MONGODB_DATABASE"], instance)
      forum_database = try(module.edx_instances.configs[instance]["FORUM_MONGODB_DATABASE"], "${instance}-cs_comments_service")
    }
  }

  provider_name                = var.mongodb_provider_name
  mongodb_version              = var.mongodb_version
  instance_size                = var.mongodb_instance_size
  cluster_type                 = var.mongodb_cluster_type
  num_shards                   = var.mongodb_num_shards
  electable_nodes              = var.mongodb_electable_nodes
  read_only_nodes              = var.mongodb_read_only_nodes
  analytics_nodes              = var.mongodb_analytics_nodes
  disk_size_gb                 = var.mongodb_disk_size_gb
  disk_iops                    = var.mongodb_disk_iops
  volume_type                  = var.mongodb_volume_type
  backup_enabled               = var.mongodb_backup_enabled
  encryption_at_rest           = var.mongodb_encryption_at_rest
  auto_scaling_disk_gb_enabled = var.mongodb_auto_scaling_disk_gb_enabled
  auto_scaling_compute_enabled = var.mongodb_auto_scaling_compute_enabled
  auto_scaling_min_instances   = var.mongodb_auto_scaling_min_instances
  auto_scaling_max_instances   = var.mongodb_auto_scaling_max_instances
  backup_retention_period      = var.mongodb_backup_retention_period

  vpc_id           = module.vpc.vpc_id
  vpc_cidr_block   = module.vpc.vpc_cidr_block
  atlas_cidr_block = var.mongodb_atlas_cidr_block
  aws_account_id   = data.aws_caller_identity.current.account_id
  route_table_id   = module.vpc.private_route_table_ids[0]
}

output "mongodb" {
  value     = module.mongodb
  sensitive = true
}

####################################################################################################
## AWS RDS MySQL
####################################################################################################

module "mysql" {
  source         = "./mysql"
  cluster_name   = var.cluster_name
  rds_subnet_ids = concat(module.vpc.private_subnets, module.vpc.public_subnets)
  vpc_id         = module.vpc.vpc_id

  rds_identifier              = var.rds_identifier != "" ? var.rds_identifier : "${var.cluster_name}-db"
  rds_instance_class          = var.rds_instance_class
  rds_mysql_version           = var.rds_mysql_version
  rds_ca_cert_identifier      = var.rds_ca_cert_identifier
  rds_min_storage             = var.rds_min_storage
  rds_max_storage             = var.rds_max_storage
  rds_backup_retention_period = var.rds_backup_retention_period
  rds_storage_encrypted       = var.rds_storage_encrypted

  ingress_cidr_blocks = ["10.0.0.0/8"]

  default_tags = local.default_tags
}

output "mysql" {
  value     = module.mysql
  sensitive = true
}
