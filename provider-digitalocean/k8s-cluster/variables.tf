variable "cluster_name" {
  type        = string
  description = "DigitalOcean cluster name."
}

variable "min_worker_node_count" {
  type        = number
  default     = 3
  description = "Minimum number of running Kubernetes worker nodes."
}

variable "max_worker_node_count" {
  type        = number
  default     = 5
  description = "Maximum number of running Kubernetes worker nodes."
}

variable "worker_node_size" {
  type        = string
  default     = "s-2vcpu-4gb"
  description = "Kubernetes worker node size."
}

variable "region" {
  type        = string
  description = "DigitalOcean region to create the resources in."
  validation {
    condition = contains([
      "ams3",
      "blr1",
      "fra1",
      "lon1",
      "nyc1",
      "nyc2",
      "nyc3",
      "sfo3",
      "sgp1",
      "tor1",
    ], var.region)
    error_message = "The DigitalOcean region to create the cluster in."
  }
}

variable "vpc_uuid" {
  type        = string
  description = "VPC network UUID to create the cluster in."
}

variable "vpc_ip_range" {
  type        = string
  description = "VPC IP range."
}
