##############################################################################
# Velero backup bucket
##############################################################################

module "velero_backups" {
  source        = "./spaces"
  bucket_prefix = "velero-backup-${var.do_region}"
  do_region     = var.do_region
  force_destroy = false
  versioning    = false
}

output "velero_backups_bucket" {
  sensitive = false
  value     = module.velero_backups.spaces_bucket.name
}

####################################################################################################
## An S3-like bucket to store the Tutor "state" (env folders)
####################################################################################################

module "tutor_env" {
  source        = "./spaces"
  bucket_prefix = "tenv-${replace(var.cluster_name, "_", "-")}"
  do_region     = var.do_region
}

output "tutor_env_bucket" {
  sensitive = false
  value     = module.tutor_env.spaces_bucket.name
}

####################################################################################################
## Create the necessary S3 buckets
####################################################################################################

module "instance_edxapp_bucket" {
  for_each = toset(var.tutor_instances)

  source                 = "./spaces"
  bucket_prefix          = join("-", ["edx", replace(var.cluster_name, "_", "-"), replace(each.key, "_", "-")])
  do_region              = var.do_region
  is_root_objects_public = true

  allowed_cors_origins = module.edx_instances.allowed_cors_origins[each.key]
}

output "edxapp_bucket" {
  sensitive = true
  value     = { for instance in var.tutor_instances : instance => module.instance_edxapp_bucket[instance] }
}
