terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "digitalocean_ssh_key" "key_pair" {
  name       = "${var.cluster_name}-${var.name}-key-pair"
  public_key = tls_private_key.private_key.public_key_openssh
}

resource "digitalocean_droplet" "droplet" {
  image             = var.image
  name              = var.cluster_name != null ? "${var.cluster_name}-${var.name}" : var.name
  region            = var.region
  size              = var.size
  backups           = var.backups
  ssh_keys          = concat([digitalocean_ssh_key.key_pair.fingerprint], var.ssh_keys)
  tags              = var.cluster_name != null ? concat(["fw-${var.cluster_name}"], var.tags) : var.tags
  user_data         = var.user_data
  volume_ids        = var.volume_ids
  monitoring        = true
  droplet_agent     = true
  graceful_shutdown = true
  resize_disk       = true
}
