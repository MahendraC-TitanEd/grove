####################################################################################################
## The Kubernetes Cluster itself
####################################################################################################

locals {
  cluster_provider = "digitalocean"
}

# This _must_ be in a separate module in order for us to use the credentials it provides
# to then deploy additional resources onto the cluster.
# https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/kubernetes_cluster#kubernetes-terraform-provider-example
# "When using interpolation to pass credentials from a digitalocean_kubernetes_cluster resource to the Kubernetes provider, the cluster resource generally should not be created in the same Terraform module where Kubernetes provider resources are also used. "

module "k8s_cluster" {
  source = "./k8s-cluster"

  cluster_name          = var.cluster_name
  max_worker_node_count = var.max_worker_node_count
  min_worker_node_count = var.min_worker_node_count
  worker_node_size      = var.worker_node_size
  region                = var.do_region
  vpc_uuid              = digitalocean_vpc.main_vpc.id
  vpc_ip_range          = var.vpc_ip_range
}

# Declare the kubeconfig as an output - access it anytime with "/tf output -raw kubeconfig"
output "kubeconfig" {
  value     = module.k8s_cluster.kubeconfig.raw_config
  sensitive = true
}

####################################################################################################
## Integrate kubernetes cluster with the GitLab project
####################################################################################################

module "k8s_gitlab_connector" {
  source                     = "../provider-modules/k8s-gitlab-connector"
  gitlab_cluster_agent_token = var.gitlab_cluster_agent_token
}

####################################################################################################
## Create k8s secret for GitLab container registry access
####################################################################################################

module "k8s_gitlab_container_registry" {
  for_each = toset(var.tutor_instances)

  source = "../provider-modules/k8s-gitlab-container-registry"

  namespace                          = each.key
  container_registry_server          = var.container_registry_server
  dependency_proxy_server            = var.dependency_proxy_server
  gitlab_group_deploy_token_username = var.gitlab_group_deploy_token_username
  gitlab_group_deploy_token_password = var.gitlab_group_deploy_token_password
}

####################################################################################################
## Create ingress controller
####################################################################################################

module "ingress" {
  source = "../provider-modules/k8s-nginx-ingress"

  ingress_namespace     = "kube-system"
  cluster_provider      = local.cluster_provider
  cluster_domain        = var.cluster_domain
  global_404_html_path  = var.global_404_html_path
  nginx_resource_quotas = lookup(yamldecode(var.k8s_resource_quotas), "nginx", {})
}

####################################################################################################
## Create monitoring pods
####################################################################################################

module "k8s_monitoring" {
  # Without the depends_on the cluster is deleted before this module when doing a `terraform destroy`.
  # Since it gets deleted before it's due, it'll cause errors.
  depends_on                         = [module.k8s_cluster.cluster_id, kubectl_manifest.cert_manager]
  source                             = "../provider-modules/k8s-monitoring"
  alert_manager_config               = var.alert_manager_config
  cluster_domain                     = var.cluster_domain
  enable_monitoring_ingress          = var.enable_monitoring_ingress
  fluent_bit_aws_config              = yamldecode(var.fluent_bit_aws_config)
  fluent_bit_aws_kube_log_config     = yamldecode(var.fluent_bit_aws_kube_log_config)
  fluent_bit_aws_tracking_log_config = yamldecode(var.fluent_bit_aws_tracking_log_config)
  opensearch_index_retention_days    = var.opensearch_index_retention_days
  opensearch_persistence_size        = var.opensearch_persistence_size
  opensearch_resource_quotas         = lookup(yamldecode(var.k8s_resource_quotas), "monitoring-opensearch", {})
}

# The OpenSearch dashboard password - access it with "/tf output -raw opensearch_dashboard_admin_password"
output "opensearch_dashboard_admin_password" {
  value     = module.k8s_monitoring.opensearch_dashboard_admin_password
  sensitive = true
}

# The monitoring basic auth password - access it with "/tf output -raw monitoring_ingress_password"
output "monitoring_ingress_password" {
  value     = module.k8s_monitoring.monitoring_ingress_password
  sensitive = true
}

####################################################################################################
## Create OpenFAAS resources
####################################################################################################

module "k8s_openfaas" {
  count          = var.enable_openfaas ? 1 : 0
  source         = "../provider-modules/k8s-openfaas"
  depends_on     = [digitalocean_vpc.main_vpc]
  cluster_domain = var.cluster_domain
}

output "openfaas_ingress_password" {
  value     = module.k8s_openfaas
  sensitive = true
}

####################################################################################################
## Create metrics server resources
####################################################################################################

module "k8s_metrics_server" {
  source               = "../provider-modules/k8s-metrics-server"
  depends_on           = [digitalocean_vpc.main_vpc]
  service_account_name = "metrics-server"
}

####################################################################################################
## Cert manager resources
####################################################################################################

# If using the kubectl_manifest resource then we get an error
# every time we do a `terraform plan`:
# No matches for kind "ClusterIssuer" in group "cert-manager.io"
# See https://github.com/hashicorp/terraform-provider-kubernetes/issues/1367
resource "kubectl_manifest" "cert_manager" {
  yaml_body = templatefile("${path.module}/assets/kubernetes/cluster-issuer.yml", {
    namespace          = "kube-system",
    notification_email = var.lets_encrypt_notification_inbox
  })
}


####################################################################################################
## Shared Elasticsearch resources
####################################################################################################

module "k8s_elasticsearch" {
  source               = "../provider-modules/k8s-elasticsearch"
  depends_on           = [digitalocean_vpc.main_vpc]
  tutor_instances      = var.tutor_instances
  count                = var.enable_shared_elasticsearch ? 1 : 0
  elasticsearch_config = yamldecode(var.elasticsearch_config)
}

output "elasticsearch" {
  value     = module.k8s_elasticsearch
  sensitive = true
}

####################################################################################################
## Velero backup resources
####################################################################################################

module "k8s_velero" {
  source = "../provider-modules/k8s-backup"
  configuration = [
    templatefile("${path.module}/assets/velero/velero.yml", {
      accessToken           = var.do_token
      spacesAccessKeyID     = var.do_access_key_id
      spacesSecretAccessKey = var.do_secret_access_key
      backupStorageBucket   = module.velero_backups.spaces_bucket.name
      backupStorageRegion   = module.velero_backups.spaces_bucket.region
      volumeSnapshotRegion  = module.velero_backups.spaces_bucket.region
      awsPluginTag          = "v1.7.1"
      digitaloceanPluginTag = "v1.1.0"
    })
  ]
}
