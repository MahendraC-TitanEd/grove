variable "cluster_name" { type = string }

variable "min_worker_node_count" {
  type        = number
  default     = 3
  description = "Minimum number of running Kubernetes worker nodes."
}

variable "max_worker_node_count" {
  type        = number
  default     = 5
  description = "Maximum number of running Kubernetes worker nodes."
}

variable "worker_node_size" {
  type        = string
  default     = "s-2vcpu-4gb"
  description = "Kubernetes worker node size."
}

variable "do_token" { type = string }

variable "do_access_key_id" { type = string }

variable "do_secret_access_key" { type = string }

variable "do_region" {
  type        = string
  default     = "sfo3"
  description = "DigitalOcean region to create the resources in."
  validation {
    condition = contains([
      "ams3",
      "blr1",
      "fra1",
      "lon1",
      "nyc1",
      "nyc2",
      "nyc3",
      "sfo3",
      "sgp1",
      "tor1",
    ], var.do_region)
    error_message = "The DigitalOcean region to create the cluster in."
  }
}

variable "mysql_cluster_instance_size" {
  type    = string
  default = "db-s-1vcpu-1gb"
}
variable "mysql_cluster_node_count" {
  type    = number
  default = 1
}
variable "mysql_cluster_engine_version" {
  type    = string
  default = "8"
}

variable "mongodb_cluster_instance_size" {
  type    = string
  default = "db-s-1vcpu-1gb"
}
variable "mongodb_cluster_node_count" {
  type    = number
  default = 3
}
variable "mongodb_cluster_engine_version" {
  type    = string
  default = "4"
}

variable "container_registry_server" { default = "registry.gitlab.com" }
variable "dependency_proxy_server" { default = "gitlab.com" }
variable "gitlab_group_deploy_token_username" { type = string }
variable "gitlab_group_deploy_token_password" { type = string }
variable "gitlab_cluster_agent_token" {
  type        = string
  description = "Token retrieved for Gitlab cluster agent"
}

variable "tutor_instances" { type = list(string) }

variable "tutor_instances_dir" {
  type    = string
  default = "../../instances/"
}

variable "vpc_ip_range" {
  type        = string
  description = "VPC IP range."
  default     = ""
}

variable "droplet_default_image" {
  type    = string
  default = "ubuntu-20-04-x64"
}

variable "alert_manager_config" {
  type        = string
  description = "Alert Manager configuration as a YAML-encoded string"
  default     = "{}"
  validation {
    condition     = can(yamldecode(var.alert_manager_config))
    error_message = "The alert_manager_config value must be a valid YAML-encoded string."
  }
}

variable "enable_monitoring_ingress" {
  type        = bool
  default     = false
  description = "Whether to enable ingress for monitoring services."
}

variable "enable_openfaas" {
  type        = bool
  default     = false
  description = "Whether to enable OpenFAAS."
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain used as the base for monitoring services."
}

variable "lets_encrypt_notification_inbox" {
  type        = string
  default     = "contact@example.com"
  description = "Email to send any email notifications about Letsencrypt"
}

variable "global_404_html_path" {
  type        = string
  default     = ""
  description = "Path in tools-container to html page to show when provisioning instances or if there's a 404 on the ingress."
}

variable "enable_shared_elasticsearch" {
  type        = bool
  default     = false
  description = "Enable the shared Elasticsearch cluster."
}

variable "fluent_bit_aws_config" {
  type        = string
  default     = "{}"
  description = "AWS credentials for Fluent-bit to use when uploading logs to S3 or Cloudwatch."

  validation {
    condition     = can(yamldecode(var.fluent_bit_aws_config))
    error_message = "The fluent_bit_aws_config provided was invalid."
  }
}

variable "fluent_bit_aws_tracking_log_config" {
  type        = string
  default     = "{}"
  description = "Fluent-bit configuration for uploading tracking logs to an S3-compatible backend or Cloudwatch."

  validation {
    condition     = can(yamldecode(var.fluent_bit_aws_tracking_log_config))
    error_message = "The fluent_bit_aws_tracking_log_config provided was invalid."
  }
}

variable "fluent_bit_aws_kube_log_config" {
  type        = string
  default     = "{}"
  description = "Fluent-bit configuration for uploading kubernetes logs to an S3-compatible backend or Cloudwatch."

  validation {
    condition     = can(yamldecode(var.fluent_bit_aws_kube_log_config))
    error_message = "The fluent_bit_aws_kube_log_config provided was invalid."
  }
}

variable "elasticsearch_config" {
  type = string
  validation {
    condition     = can(yamldecode(var.elasticsearch_config))
    error_message = "The elasticsearch_config provided was invalid."
  }
  description = "Configuration for the shared ElasticSearch cluster."
  default     = "{}"
}

variable "k8s_resource_quotas" {
  type = string
  validation {
    condition     = can(yamldecode(var.k8s_resource_quotas))
    error_message = "The k8s_resource_quotas provided was invalid."
  }
  description = "Resource configuration for the cluster."
  default     = "{}"
}

variable "opensearch_index_retention_days" {
  type        = number
  default     = 180
  description = "Number of days to retain index data in OpenSearch."
}

variable "opensearch_persistence_size" {
  type        = string
  default     = "8Gi"
  description = "Size of the persistent volume claim for OpenSearch."
}

variable "new_relic_account_id" {
  type        = number
  description = "Your New Relic Account ID."
  default     = 0
}

variable "new_relic_api_key" {
  type        = string
  description = "Your New Relic API Key."
  # We use this default so that the New Relic provider thinks it has a valid API key
  default = "NRAK-XXXXXXXXX"
}

variable "new_relic_region_code" {
  type        = string
  description = "The region your New Relic account is in."
  default     = "US"
}

variable "new_relic_recipients" {
  type        = string
  description = "Comma-separated list of emails"
  default     = ""
}
