variable "namespace" {
  type        = string
  description = "Namespace of the resource."
}

variable "lets_encrypt_notification_inbox" {
  type        = string
  default     = "contact@example.com"
  description = "Email to send any email notifications about Letsencrypt."
}
