variable "new_relic_account_id" {
  type        = string
  description = "Your New Relic Account ID."
}

variable "new_relic_api_key" {
  type        = string
  description = "Your New Relic API Key."
}

variable "new_relic_region_code" {
  type        = string
  description = "The region your New Relic account is in."
}

variable "cluster_name" {
  type = string
}

variable "instance_name" {
  type = string
}

variable "tutor_config" {
  type = any
}

variable "email_recipients" {
  type = string
}
