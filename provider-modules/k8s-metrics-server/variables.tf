# https://github.com/kubernetes-sigs/metrics-server/blob/master/charts/metrics-server/Chart.yaml
variable "chart_version" {
  type    = string
  default = "3.11.0"
}

variable "service_account_name" {
  type = string
}
