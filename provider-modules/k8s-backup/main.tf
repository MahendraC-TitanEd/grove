locals {
  velero_app_version             = "5.0.2"  # https://github.com/vmware-tanzu/helm-charts/blob/main/charts/velero/Chart.yaml
  velero_aws_plugin_tag          = "v1.5.4" # https://github.com/vmware-tanzu/velero-plugin-for-aws/releases
  velero_digitalocean_plugin_tag = "v1.1.0" # https://github.com/digitalocean/velero-plugin/releases
}

##############################################################################
# Create Velero resources
##############################################################################

# The namespace defaults to velero, but can be overridden. The reason we are
# explicit about it is that if the namespace is changed, we need to update
# many other configurations.
resource "helm_release" "velero" {
  name             = "velero"
  repository       = "https://vmware-tanzu.github.io/helm-charts"
  chart            = "velero"
  version          = local.velero_app_version
  namespace        = "velero"
  create_namespace = true

  values = var.configuration
}
