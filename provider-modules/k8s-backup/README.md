# k8s-backup

This module installs Velero for Kubernetes backup and restore. Due to the
complex configuration needs of Velero, this module does not provide any
other resources, but requires the whole configuration to be provided.

# Variables

- `configuration`: Velero backup configuration

# Output

This module doesn't provide any output.
