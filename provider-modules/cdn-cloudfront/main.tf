locals {
  name = "${var.instance_name}-${var.service_name}"
}

terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = "~> 5.14"
      configuration_aliases = [aws.virginia]
    }
  }
}

resource "aws_cloudfront_cache_policy" "cache_policy" {
  name        = "${local.name}-cdn-cache-policy"
  comment     = "${local.name}-cdn-cache-policy"
  min_ttl     = 1
  default_ttl = var.cache_expiration
  max_ttl     = var.cache_expiration

  parameters_in_cache_key_and_forwarded_to_origin {
    enable_accept_encoding_gzip = true

    cookies_config {
      cookie_behavior = "none"
    }

    headers_config {
      header_behavior = "whitelist"
      headers {
        items = ["Origin", ]
      }
    }

    query_strings_config {
      query_string_behavior = "all"
    }
  }
}

resource "aws_cloudfront_origin_request_policy" "origin_request_policy" {
  name    = "${local.name}-request-policy"
  comment = "${local.name}-request-policy"

  cookies_config {
    cookie_behavior = "none"
  }

  headers_config {
    header_behavior = "whitelist"
    headers {
      items = ["Origin", ]
    }
  }
  query_strings_config {
    query_string_behavior = "all"
  }
}

resource "aws_cloudfront_response_headers_policy" "response_headers_policy" {
  name    = "${local.name}-header-policy"
  comment = "${local.name}-header-policy"

  cors_config {
    access_control_allow_credentials = false
    access_control_max_age_sec       = var.cache_expiration
    origin_override                  = true

    access_control_allow_headers {
      items = [
        "*",
      ]
    }

    access_control_allow_methods {
      items = [
        "ALL",
      ]
    }

    access_control_allow_origins {
      items = [
        "*",
      ]
    }
  }
}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  enabled         = true
  is_ipv6_enabled = true
  comment         = "${local.name}-distribution"
  aliases         = compact([var.alias])

  origin {
    domain_name = var.origin_domain
    origin_id   = var.origin_domain

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy   = "match-viewer"
      origin_read_timeout      = 30
      origin_ssl_protocols = [
        "TLSv1",
        "TLSv1.1",
        "TLSv1.2",
      ]
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.origin_domain

    compress = true

    viewer_protocol_policy     = "redirect-to-https"
    cache_policy_id            = aws_cloudfront_cache_policy.cache_policy.id
    origin_request_policy_id   = aws_cloudfront_origin_request_policy.origin_request_policy.id
    response_headers_policy_id = aws_cloudfront_response_headers_policy.response_headers_policy.id
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = var.alias == "" ? true : false
    acm_certificate_arn            = var.alias == "" ? null : aws_acm_certificate.alias_certificate[0].arn
    ssl_support_method             = var.alias == "" ? null : "sni-only"
  }
}

resource "aws_acm_certificate" "alias_certificate" {
  count             = var.alias == "" ? 0 : 1
  domain_name       = var.alias
  validation_method = "DNS"
  provider          = aws.virginia
}
