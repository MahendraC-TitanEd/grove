variable "instance_name" {
  description = "The instance name this CDN will apply to."
  type        = string
}

variable "service_name" {
  description = "The service this will apply to. Used for disambiguation."
  type        = string
}

variable "origin_domain" {
  description = "The domain of the static file origins that the CDN should pull from."
  type        = string
}

variable "cache_expiration" {
  description = "Time of cache expiration in seconds."
  type        = number
  default     = 31536000
}

variable "alias" {
  description = "The list of aliases for the Cloudfront domain."
  type        = string
  default     = ""
}
