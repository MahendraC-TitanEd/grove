# https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard
variable "dashboard_chart_version" {
  type    = string
  default = "6.0.8"
}

variable "service_account_name" {
  type = string
}
