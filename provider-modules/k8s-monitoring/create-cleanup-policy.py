"""
A script to create or update a cleanup policy for OpenSearch.

See https://opensearch.org/docs/latest/im-plugin/ism/api/#get-policy
for the API description
"""

import json
import os
import requests

OPENSEARCH_HOST = os.environ["OPENSEARCH_HOST"]
POLICY_ID = os.environ["POLICY_ID"]
POLICY_URL = f"https://{OPENSEARCH_HOST}/_plugins/_ism/policies/{POLICY_ID}"
ATTACH_POLICY_URL = f"https://{OPENSEARCH_HOST}/_plugins/_ism/add/"
POLICY_CONTENT = os.environ["POLICY"]


def raise_http_error(resp: requests.Response) -> None:
    """
    For the given response, print the text and throw
    and exception if the server returned an error.
    """
    if resp.status_code >= 400:
        print(resp.text)
    resp.raise_for_status()


def create_or_update_policy(
    session: requests.Session, policy_url: str, policy_content: str
) -> requests.Response:
    """
    Creates or updates the ISM policy.
    """
    resp = session.get(policy_url)

    if resp.status_code == 404:
        print("Cleanup policy does not exist, creating...")
        create_resp = session.put(policy_url, policy_content)
        raise_http_error(create_resp)
        return create_resp

    raise_http_error(resp)
    print("Cleanup policy exists, updating...")
    json = resp.json()
    seq_no = json["_seq_no"]
    primary_term = json["_primary_term"]

    update_resp = session.put(
        policy_url,
        data=policy_content,
        params={"if_seq_no": seq_no, "if_primary_term": primary_term},
    )
    raise_http_error(update_resp)
    return update_resp


def attach_policy_to_index(
    session: requests.Session, policy_id: str
) -> requests.Response:
    """
    Attaches a policy to the ISM index.
    """
    post_data = {"policy_id": policy_id}
    index_name = "fluent-bit*"
    resp = session.post(ATTACH_POLICY_URL + index_name, json=post_data)
    raise_http_error(resp)
    return resp


def get_session(username, password) -> requests.Session:
    """
    Set up session for communicating with OpenSearch host.
    """
    session = requests.Session()
    session.auth = (username, password)
    session.verify = False
    session.headers["Content-type"] = "application/json"
    return session


if __name__ == "__main__":
    username = "admin"
    session = get_session(username, os.environ["PASSWORD"])

    create_or_update_policy(session, POLICY_URL, POLICY_CONTENT)
    attach_policy_to_index(session, POLICY_ID)
