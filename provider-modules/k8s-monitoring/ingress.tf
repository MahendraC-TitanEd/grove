resource "random_password" "ingress_auth" {
  length           = 32
  special          = false
  override_special = ",.-_!"
}

resource "htpasswd_password" "ingress_auth" {
  password = random_password.ingress_auth.result
  salt     = substr(sha512(random_password.ingress_auth.result), 0, 8)
}

resource "kubernetes_secret" "ingress_auth" {
  metadata {
    name      = "${local.namespace_monitoring}-basic-auth"
    namespace = local.namespace_monitoring
    labels = {
      app = "k8s-monitoring"
    }
  }

  data = {
    "admin" = htpasswd_password.ingress_auth.bcrypt
  }
}

resource "kubernetes_ingress_v1" "monitoring_ingress" {
  count = var.enable_monitoring_ingress ? 1 : 0

  metadata {
    name      = "monitoring-ingress"
    namespace = local.namespace_monitoring
    annotations = {
      "nginx.ingress.kubernetes.io/auth-type"        = "basic",
      "nginx.ingress.kubernetes.io/auth-secret-type" = "auth-map",
      "nginx.ingress.kubernetes.io/auth-secret"      = "${local.namespace_monitoring}/${kubernetes_secret.ingress_auth.metadata.0.name}"
      "nginx.ingress.kubernetes.io/auth-realm"       = "Authentication is required"
      "nginx.ingress.kubernetes.io/ssl-redirect"     = "true"
      "nginx.ingress.kubernetes.io/enable-cors"      = "true"
      "kubernetes.io/tls-acme"                       = "true"
      "cert-manager.io/cluster-issuer"               = "letsencrypt-prod"
    }
  }

  spec {
    ingress_class_name = "nginx"

    tls {
      secret_name = "${local.namespace_monitoring}-ingress"
      hosts = [
        "prometheus.${var.cluster_domain}",
        "grafana.${var.cluster_domain}",
        "alert-manager.${var.cluster_domain}",
        "opensearch-dashboard.${var.cluster_domain}",
      ]
    }

    rule {
      host = "prometheus.${var.cluster_domain}"
      http {
        path {
          backend {
            service {
              name = "prometheus-operated"
              port {
                number = 9090
              }
            }
          }
          path = "/"
        }
      }
    }

    rule {
      host = "alert-manager.${var.cluster_domain}"
      http {
        path {
          backend {
            service {
              name = "alertmanager-operated"
              port {
                number = 9093
              }
            }
          }

          path = "/"
        }
      }
    }

    rule {
      host = "grafana.${var.cluster_domain}"
      http {
        path {
          backend {
            service {
              name = "prometheus-operator-grafana"
              port {
                number = 3000
              }
            }
          }

          path = "/"
        }
      }
    }

    rule {
      host = "opensearch-dashboard.${var.cluster_domain}"
      http {
        path {
          backend {
            service {
              name = "opensearch-dashboard-opensearch-dashboards"
              port {
                number = 5601
              }
            }
          }
          path = "/"
        }
      }
    }
  }
}

resource "kubernetes_ingress_v1" "opensearch_ingress" {
  count = var.enable_monitoring_ingress ? 1 : 0
  metadata {
    name      = "opensearch-ingress"
    namespace = local.namespace_monitoring
    annotations = {
      "nginx.ingress.kubernetes.io/ssl-redirect" = "true"
      "nginx.ingress.kubernetes.io/enable-cors"  = "true"
      "kubernetes.io/tls-acme"                   = "true"
      "cert-manager.io/cluster-issuer"           = "letsencrypt-prod"
      "nginx.ingress.kubernetes.io/backend-protocol" : "HTTPS"
    }
  }

  spec {
    ingress_class_name = "nginx"

    tls {
      secret_name = "opensearch-ingress-tls"
      hosts = [
        local.opensearch_endpoint
      ]
    }

    rule {
      host = local.opensearch_endpoint
      http {
        path {
          backend {
            service {
              name = "opensearch-cluster-master"
              port {
                number = 9200
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}
