---
envFrom:
  - secretRef:
      name: fluent-bit-aws-keys

config:
  inputs: |
    [INPUT]
        Name tail
        Path /var/log/containers/*.log
        multiline.parser docker, cri, python
        Mem_Buf_Limit 5MB
        Skip_Long_Lines On
        Tag kube.<namespace_name>.<pod_name>.<container_name>.<docker_id>
        Tag_Regex (?<pod_name>[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*)_(?<namespace_name>[^_]+)_(?<container_name>.+)-(?<docker_id>[a-z0-9]{64})\.log$
    [INPUT]
        Name systemd
        Tag host.*
        Systemd_Filter _SYSTEMD_UNIT=kubelet.service
        Read_From_Tail On
  customParsers: |
    [PARSER]
        Name    kube-parser
        Format  regex
        Regex   (?<namespace_name>[^.]+)\.(?<pod_name>[^.]+)\.(?<container_name>[^.]+)\.(?<docker_id>[^.]+)
    [PARSER]
        Name    tracking-log-parser
        Format  regex
        Regex   ^(?<date>[^ ]*) (?<time>[^ ]*) (?<log_level>[^ ]*) [^ ]* \[tracking\] \[.*\] \[.*\] [^ ]* [^ ]* (?<log>\{.*\})$
  filters: |
    [FILTER]
        Name kubernetes
        Match kube.*
        Keep_Log Off
        K8S-Logging.Parser On
        K8S-Logging.Exclude On
        Regex_Parser kube-parser
        Kube_Tag_Prefix kube.
    [FILTER]
        # This filter duplicates any log record that looks like a tracking log
        # and tags it with the tracking.<k8s-namespace> tag.
        Name rewrite_tag
        Match kube.*
        Rule $log .*(\[tracking\]).* tracking.$TAG[1] true
        Emitter_Name re_emitted
    [FILTER]
        # This filter takes a tracking log line as emitted by tutor and removes
        # the metadata from the front of the line so that only the JSON part remains.
        Name parser
        Match tracking.*
        Key_Name log
        Parser tracking-log-parser
  outputs: |
    [OUTPUT]
        Name opensearch
        Match kube.*
        Host opensearch-cluster-master
        Port 9200
        Retry_Limit 10
        HTTP_User admin
        HTTP_Passwd ${password}
        tls On
        tls.verify Off
        Index fluent-bit-logs
        Type _doc
        Replace_Dots On
        Buffer_Size 64KB
%{ if kube_logs_config.s3_bucket_name != "" && kube_logs_config.s3_bucket_name != null }
    [OUTPUT]
        Name s3
        Match kube.*
        bucket ${kube_logs_config.s3_bucket_name}
        region ${kube_logs_config.s3_region}
        s3_key_format ${kube_logs_config.s3_log_key_format}
        static_file_path On
        %{ if kube_logs_config.s3_endpoint != "" && kube_logs_config.s3_endpoint != null }
        endpoint ${kube_logs_config.s3_endpoint}
        %{ endif }
        use_put_object On
        total_file_size 10M
        upload_timeout 10m
        %{ if kube_logs_config.s3_compress_logs != "" && kube_logs_config.s3_compress_logs != null }
        compression ${ kube_logs_config.s3_compress_logs }
        %{ endif }
%{ endif }
%{ if tracking_logs_config.s3_bucket_name != "" && tracking_logs_config.s3_bucket_name != null }
    [OUTPUT]
        Name s3
        Match tracking.*
        log_key log
        bucket ${tracking_logs_config.s3_bucket_name}
        region ${tracking_logs_config.s3_region}
        s3_key_format ${tracking_logs_config.s3_log_key_format}
        static_file_path On
        %{ if tracking_logs_config.s3_endpoint != "" && tracking_logs_config.s3_endpoint != null }
        endpoint ${tracking_logs_config.s3_endpoint}
        %{ endif }
        use_put_object On
        total_file_size 10M
        upload_timeout 10m
        %{ if tracking_logs_config.s3_compress_logs != "" && tracking_logs_config.s3_compress_logs != null }
        compression ${ tracking_logs_config.s3_compress_logs }
        %{ endif }
%{ endif }
%{ if kube_logs_config.cloudwatch_group_name != "" && kube_logs_config.cloudwatch_group_name != null }
    [OUTPUT]
        Name cloudwatch_logs
        Match *
        region ${kube_logs_config.cloudwatch_region}
        log_group_name ${kube_logs_config.cloudwatch_group_name}
        log_stream_name fluent-bit-logs
        auto_create_group On
        workers 1
%{endif}
%{ if tracking_logs_config.cloudwatch_group_name != "" && tracking_logs_config.cloudwatch_group_name != null }
    [OUTPUT]
        Name cloudwatch_logs
        Match tracking.*
        region ${tracking_logs_config.cloudwatch_region}
        log_group_name ${tracking_logs_config.cloudwatch_group_name}
        log_stream_name fluent-bit-tracking-logs
        auto_create_group On
        workers 1
%{ endif }
