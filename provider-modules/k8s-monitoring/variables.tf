variable "alert_manager_config" {
  type        = string
  description = "Alert Manager configuration as a YAML-encoded string"
  default     = "{}"
  validation {
    condition     = can(yamldecode(var.alert_manager_config))
    error_message = "The alert_manager_config value must be a valid YAML-encoded string."
  }
}

variable "enable_monitoring_ingress" {
  type        = bool
  default     = false
  description = "Whether to enable ingress for monitoring services."
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain used as the base for monitoring services."
}

variable "fluent_bit_aws_config" {
  type = object({
    aws_access_key_id     = optional(string)
    aws_secret_access_key = optional(string)
  })
  default     = {}
  description = "AWS credentials for Fluent-bit to use when uploading logs to S3 or Cloudwatch."
}

variable "fluent_bit_aws_tracking_log_config" {
  type = object({
    cloudwatch_group_name = optional(string)
    cloudwatch_region     = optional(string)
    s3_bucket_name        = optional(string)
    s3_endpoint           = optional(string)
    s3_region             = optional(string)
    s3_log_key_format     = optional(string)
    s3_compress_logs      = optional(string)
  })
  default     = {}
  description = "Fluent-bit configuration for uploading tracking logs to an S3-compatible backend or Cloudwatch."
}

variable "fluent_bit_aws_kube_log_config" {
  type = object({
    cloudwatch_group_name = optional(string)
    cloudwatch_region     = optional(string)
    s3_bucket_name        = optional(string)
    s3_endpoint           = optional(string)
    s3_region             = optional(string)
    s3_log_key_format     = optional(string)
    s3_compress_logs      = optional(string)
  })
  default = {
    cloudwatch_group_name = null
    cloudwatch_region     = null
    s3_bucket_name        = null
    s3_endpoint           = null
    s3_region             = null
    s3_log_key_format     = null
  }
  description = "Fluent-bit configuration for uploading kubernetes logs to an S3-compatible backend or Cloudwatch."
}

variable "opensearch_resource_quotas" {
  type = object({
    limits   = optional(map(string))
    requests = optional(map(string))
  })
  default     = {}
  description = "Resource quotas for OpenSearch Helm chart."
}

variable "opensearch_num_replicas" {
  default     = 1
  type        = number
  description = "The number of replicas to create for the OpenSearch cluster."
}

variable "opensearch_index_retention_days" {
  type        = number
  default     = 180
  description = "Number of days to retain index data in OpenSearch."
}

variable "opensearch_persistence_size" {
  type        = string
  default     = "8Gi"
  description = "Size of the persistent volume claim for OpenSearch."
}
