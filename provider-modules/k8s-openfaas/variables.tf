variable "openfaas_chart_version" {
  type    = string
  default = "14.1.7"
}

# https://github.com/openfaas/faas-netes/releases
variable "openfaas_connector_chart_version" {
  type    = string
  default = "0.6.5"
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain name of the cluster."
}
