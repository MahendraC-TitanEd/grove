variable "namespace" {
  type = string
}

variable "container_registry_server" {
  type = string
}

variable "dependency_proxy_server" {
  type = string
}

variable "gitlab_group_deploy_token_username" {
  type = string
}

variable "gitlab_group_deploy_token_password" {
  type = string
}
