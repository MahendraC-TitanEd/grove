variable "elasticsearch_namespace" {
  type    = string
  default = "elasticsearch"
}

variable "tutor_instances" {
  type = list(string)
}

variable "elasticsearch_config" {
  type = object({
    heap_size         = optional(string)
    replicas          = optional(number)
    search_queue_size = optional(string)
    cpu_limit         = optional(string)
    memory_limit      = optional(string)

  })

  default = {}

  description = "ElasticSearch configuration."
}
