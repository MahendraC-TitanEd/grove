locals {
  elasticsearch_namespace = "elasticsearch"
  elasticsearch_version   = "8.5.1"
  config = {
    heap_size         = var.elasticsearch_config.heap_size == null ? "1g" : var.elasticsearch_config.heap_size
    replicas          = var.elasticsearch_config.replicas == null ? "2" : var.elasticsearch_config.replicas
    search_queue_size = var.elasticsearch_config.search_queue_size == null ? 5000 : var.elasticsearch_config.search_queue_size
    cpu_limit         = var.elasticsearch_config.cpu_limit == null ? "2000m" : var.elasticsearch_config.cpu_limit
    memory_limit      = var.elasticsearch_config.memory_limit == null ? "4Gi" : var.elasticsearch_config.memory_limit
  }
}

resource "random_password" "admin_password" {
  length  = 32
  special = false
}

resource "kubernetes_namespace" "elasticsearch_namespace" {
  metadata {
    name = local.elasticsearch_namespace
  }
}

resource "random_password" "elasticsearch_passwords" {
  for_each = toset(var.tutor_instances)
  length   = 32
  special  = false
}

resource "random_string" "elasticsearch_prefix_separator" {
  for_each = toset(var.tutor_instances)
  length   = 4
  special  = false
  upper    = false
}

resource "kubernetes_config_map" "root_ca_mount" {
  for_each = toset(var.tutor_instances)
  metadata {
    namespace = each.value
    name      = "elasticsearch-ca"
  }
  data = {
    "elasticsearch-ca.pem" = tls_self_signed_cert.elasticsearch_root_ca_cert.cert_pem
  }
}

resource "kubernetes_secret" "elasticsearch_credentials" {
  depends_on = [kubernetes_namespace.elasticsearch_namespace]
  metadata {
    name      = "elastic-credentials"
    namespace = local.elasticsearch_namespace
  }
  data = {
    "password" = random_password.admin_password.result
  }
  type = "Opaque"
}


resource "helm_release" "elasticsearch" {
  depends_on = [kubernetes_secret.elasticsearch_certificates, kubernetes_secret.elasticsearch_credentials]
  name       = "elasticsearch"
  repository = "https://helm.elastic.co"
  chart      = "elasticsearch"
  version    = local.elasticsearch_version
  namespace  = local.elasticsearch_namespace
  timeout    = 180

  set {
    name  = "esConfig.users"
    value = join("\n", [for username in var.tutor_instances : "${username}:${random_password.elasticsearch_passwords[username].bcrypt_hash}"])
  }

  set {
    name  = "esConfig.users_roles"
    value = join("\n", [for username in var.tutor_instances : "${username}_role:${username}"])
  }

  set {
    name  = "esConfig.roles\\.yml"
    value = templatefile("${path.module}/roles.yml", { usernames = var.tutor_instances, separators = random_string.elasticsearch_prefix_separator })
  }

  set {
    name  = "replicas"
    value = local.config.replicas
  }

  set {
    name  = "esJavaOpts"
    value = "-Xmx${local.config.heap_size} -Xms${local.config.heap_size}"
  }

  values = [
    templatefile("${path.module}/elasticsearch-values.yml", {
      search_queue_size = local.config.search_queue_size,
      cpu_limit         = local.config.cpu_limit,
      memory_limit      = local.config.memory_limit,
    })
  ]
}
