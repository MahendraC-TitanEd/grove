terraform {
  # Use GitLab Terraform backend.
  backend "http" {
    # The following variables will be set automatically on GitLab CI thanks to the GitLab Terraform image
    # (see https://gitlab.com/gitlab-org/terraform-images/-/blob/master/src/bin/gitlab-terraform.sh)
    # Note that CI_PROJECT_ID is the *numeric* project ID shown on the project's GitLab homepage; it is not the project name/path.

    # address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
    # lock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
    # unlock_address = "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.2"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14"
    }
  }
}

# Configure the LocalStack AWS provider
provider "aws" {
  access_key = "mock_access_key"
  secret_key = "mock_secret_key"
  region     = "us-east-1"

  s3_use_path_style           = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    iam = var.localstack_host
    s3  = var.localstack_host
  }
}

# Configure Kubernetes provider
provider "kubernetes" {
  host = var.minikube_host
}

# Configure Helm provider
provider "helm" {
  kubernetes {
    host = var.minikube_host
  }
}

provider "kubectl" {
  host = var.minikube_host
}
