####################################################################################################
## An S3 bucket to store the Tutor "state" (env folders)
####################################################################################################

module "tutor_env" {
  source        = "./s3"
  bucket_prefix = "tenv-${var.cluster_name}"
  tags = merge(local.default_tags, {
    Name = "Tutor State Bucket for ${var.cluster_name}"
  })
}

output "tutor_env_bucket" {
  sensitive = false
  value     = module.tutor_env.s3_bucket.id
}
