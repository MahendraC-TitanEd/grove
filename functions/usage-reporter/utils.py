"""
Utility functions applicable to the whole project
"""
import datetime
from pathlib import Path
from typing import List, Tuple
from zipfile import ZipFile


def get_report_directory(
    provider_name: str, month: datetime.datetime, is_monthly: bool = False
):
    """
    Using the provider_name and timestamp return the full directory
    where a report should be placed.
    """
    if is_monthly:
        return f"k8s-resource-usage-monthly/{provider_name}/"

    return f"k8s-resource-usage-daily/{provider_name}/{month:%Y%m}"


def get_report_filename(
    provider_name: str, timestamp: datetime.datetime, is_monthly: bool = False
):
    """
    Using the provider_name and timestamp return the full path
    including the directory where the csv file should be placed.
    """
    directory = get_report_directory(provider_name, timestamp, is_monthly)
    return f"{directory}/{timestamp:%Y%m%d-%H%M%S}.csv"


def get_secret(name: str) -> str:
    """
    Return a secret mapped as a file by OpenFAAS.

    Args:
        name: name of the secret

    Returns:
        Value secret value from the file.
    """

    return open(get_secret_path(name), "r").read().strip()


def get_secret_path(name: str) -> Path:
    """
    Return the given secrets' absolute path.

    Args:
        name: name of the secret

    Returns:
        Absolute path for the named secret.
    """

    return Path(f"/var/openfaas/secrets/{name}").absolute()


def compress_content(contents: List[Tuple[str, str]]) -> Path:
    """
    Compress multiple string content into a zip file and return the resulting zip file's
    path.

    Args:
        contents: list of filename-content pairs to compress

    Returns:
        Path for the resulting zip file.
    """

    zip_path = Path("build_logs.zip")

    with ZipFile(zip_path, "w") as f:
        for file_name, content in contents:
            f.writestr(f"{file_name}.txt", content)

    return zip_path
