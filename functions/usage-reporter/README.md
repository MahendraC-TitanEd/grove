# Usage reporter

This function creates a report of the resource usage of the namespaces in the cluster.

When invoked, a report is generated (and optionally uploaded to S3) of the vCPU minutes
used and memory used by a namespace.

## Prerequisites

1. Working Grove cluster
2. OpenFAAS CLI installed
3. S3 Details

## Installation

1. Get the OpenFAAS admin password by executing `./tf output -json openfaas_ingress_password`.
2. Change into the `grove/functions` library, e.g. `cd my-cluster/grove/functions/`
3. Login to the Grove hosted OpenFAAS and GitLab container registry

   ```shell
   export OPENFAAS_URL=https://openfaas.<YOUR_CLUSTER_HOSTNAME>
   faas-cli login --tls-no-verify -g $OPENFAAS_URL -u admin -p "<OPENFAAS_ADMIN_PASSWORD>"
   docker login <GITLAB_CI_REGISTRY> --username `<GITLAB_USERNAME>` --password `<GITLAB_PASSWORD>`
   ```

4. Create a secret with your AWS keys

   ```shell
   ./kubectl --namespace openfaas-fn create secret generic usage-report-aws-access-key-id \
      --from-literal usage-report-aws-access-key-id="<SECRET>"
   ./kubectl --namespace openfaas-fn create secret generic usage-report-aws-secret-key \
      --from-literal usage-report-aws-secret-key="<SECRET>"
   ```

5. Build and deploy the function as shown below. Note that if you did not override the function in any way, you don't need to build and push the image as that image is managed by OpenCraft. Remove the `ENDPOINT_URL` if you're using Amazon S3.

   ```shell
   faas-cli build -f usage-reporter.yml
   faas-cli push -f usage-reporter.yml
   faas-cli deploy \
      -f usage-reporter.yml \
      --env S3_BUCKET_NAME="<YOUR_S3_BUCKET_NAME>" \
      --env AWS_REGION="<YOUR_S3_REGION>" \
      --env AWS_ENDPOINT_URL="<YOUR_ENDPOINT_URL>" \
      --gateway "${OPENFAAS_URL}"
   ```

Now, the function is ready to use. You can call the function by using the basic auth credentials used for logging in to OpenFAAS UI by providing it in the URL. Example: `https://admin:<OPENFAAS_ADMIN_PASSWORD>@openfaas.<YOUR_CLUSTER_URL>/function/usage-reporter`.

## API spec

## Description

The function takes the parameters:

- `provider`:  refers to the data source used to retrieved the metrics data. Allowed values are either `prometheus` (default) or `k8s-metrics`. The Prometheus data source allows you to see data at any timestamp in the past whereas the k8s-metrics only allows you to see the recent data.
- `timestamp`: Timestamp instant to use with the Prometheus provider in IS0-8601 format.
- `num_months`: Optional float. If provided, the stats returned will be the average over the provided number of months, determined by averaging the generated reports over the timeframe. Note that the timeframe is calculated relative to the timestamp. For example:
  - `timestamp: 2022-10-01, num_months: 1` will average all reports generated from `midnight, 1 October` to `midnight, 1 November`.
  - `timestamp: 2022-10-01, num_months: -1` in turn, will consider reports generated between `1 September 2022` and midnight `1 October 2022`.

Responses are of the format:

- `message`: `success` if successful, otherwise it will contain an error message.
- `report_url`: Signed S3 URL to the CSV download of the report.
- `items`: List of namespaces with their equivalent resource usage:
  - `cpu_minutes`: The number of CPU minutes being used by the namespace. 1000 == 1CPU core.
  - `memory_megabytes`: The memory use of the namespace in Megabytes.
  - `equivalent_vms`: The number of VMs that this namespace is equivalent to. Matches against 4 CPUS and 8GiB RAM.
  - `namespace`: The namespace used.

### Example

Request:

```http
POST https://openfaas.your-cluster.opencraft.hosting/function/usage-reporter
Content-type: application/json
{"provider": "prometheus", "timestamp": "2022-10-03T07:15:13"}
```

Response

```json
{
  "message": "success",
  "report_url": "https://ams3.digitaloceanspaces.com/s3-bucket-name/usage_report_20221003_074200.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=SECRET...",
  "items": [
    {
      "namespace": "openfaas",
      "cpu_minutes": 42.5954185479343,
      "memory_megabytes": 143.06640625,
      "equivalent_vms": 0.017464160919189453
    },
    {
      "namespace": "kube-system",
      "cpu_minutes": 294.92264969226466,
      "memory_megabytes": 1276.7265625,
      "equivalent_vms": 0.15585041046142578
    },
    {
      "namespace": "openedx-instance",
      "cpu_minutes": 25.51627695671834,
      "memory_megabytes": 4177.3046875,
      "equivalent_vms": 0.5099248886108398
    },
    {
      "namespace": "monitoring",
      "cpu_minutes": 193.95196772751368,
      "memory_megabytes": 3023.42578125,
      "equivalent_vms": 0.36907052993774414
    },
    {
      "namespace": "gitlab-kubernetes-agent",
      "cpu_minutes": 0.3934916715014245,
      "memory_megabytes": 37.515625,
      "equivalent_vms": 0.0045795440673828125
    },
    {
      "namespace": "openfaas-fn",
      "cpu_minutes": 10.63140823657597,
      "memory_megabytes": 104.7890625,
      "equivalent_vms": 0.012791633605957031
    }
  ]
}
```
