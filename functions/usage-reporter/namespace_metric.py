"""
Module to store the NamespaceMetric dataclass.
"""
from dataclasses import dataclass

from kubernetes.utils.quantity import parse_quantity


@dataclass
class NamespaceMetric:
    """
    Helper class to work with the metrics API. Provides attributes
    and methods to retrieve and store data received from the
    metrics api.
    """

    namespace: str
    cpu_minutes: int
    memory_megabytes: float

    def combine(self, namespace_metric: "NamespaceMetric") -> "NamespaceMetric":
        """
        Adds the cpu_minutes and memory_megabytes of the namespace_metric
        to the existing values.
        """
        self.cpu_minutes += namespace_metric.cpu_minutes
        self.memory_megabytes += namespace_metric.memory_megabytes
        return self

    @classmethod
    def import_metric(cls, metric: dict) -> "NamespaceMetric":
        """
        Parses an item from the k8s metrics api and returns
        a new instance of this class with the retrieved values.
        """
        cpu_usage = sum(
            parse_quantity(container["usage"]["cpu"])
            for container in metric["containers"]
        )
        mem_usage = sum(
            parse_quantity(container["usage"]["memory"])
            for container in metric["containers"]
        )
        return NamespaceMetric(
            cpu_minutes=int(cpu_usage * 1000),
            memory_megabytes=float(mem_usage / 1024 / 1024),
            namespace=metric["metadata"]["namespace"],
        )

    def num_equivalent_vms(
        self, vm_cpu_count: int = 4, vm_memory_megabytes: int = 8192
    ) -> float:
        """
        Looks at the values stored to determine the number
        of VMs it would be equivalent to if using a VPS
        provider.
        """
        num_cpus = self.cpu_minutes / 1000
        num_vm_cpus_required = num_cpus / vm_cpu_count
        num_vms_required_for_mem = self.memory_megabytes / vm_memory_megabytes
        return max([num_vm_cpus_required, num_vms_required_for_mem])

    def asdict(self) -> dict:
        """
        Returns a dict representation of the class for exporting
        as json.
        """
        return {
            "namespace": self.namespace,
            "cpu_minutes": self.cpu_minutes,
            "memory_megabytes": self.memory_megabytes,
            "equivalent_vms": self.num_equivalent_vms(),
        }
