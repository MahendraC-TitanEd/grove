from datetime import datetime, timezone
import json
import logging
import os
import re
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from typing import Any


from .conf import config
from .digitalocean import DigitalOceanClient
from .exceptions import WebhookInvalidException
from .github import GitHubClient, PullRequest, PullRequestState
from .gitlab import (
    SANDBOX_INSTANCE_PATTERN,
    CreateOrUpdatePipelineParams,
    DestroyPipelineParams,
    GitLabClient,
    GitLabStatus,
    GitLabWebhook,
    GroveInstance,
)
from .kubernetes import KubernetesClient
from .openedx import MFEInfo, NamedRelease, get_mfe_name
from .utils import get_secret

logger = logging.getLogger(__name__)
logger.setLevel(config.log_level)

digitalocean_client = DigitalOceanClient(
    spaces_endpoint=config.digitalocean.get_spaces_endpoint(),
    spaces_region=config.digitalocean.spaces_endpoint_region,
    access_key_id=get_secret("pr-watcher-spaces-access-key-id"),
    secret_access_key=get_secret("pr-watcher-spaces-access-key-secret"),
)

github_client = GitHubClient(
    token=get_secret("pr-watcher-github-access-token"),
    user_agent=config.user_agent,
)

gitlab_client = GitLabClient(
    host=config.gitlab.host,
    access_token=get_secret("pr-watcher-gitlab-access-token"),
    trigger_token=get_secret("pr-watcher-gitlab-pipeline-token"),
    user_agent=config.user_agent,
)
kubernetes_client = KubernetesClient()

common_file_upload_params = {
    "spaces_endpoint": config.digitalocean.get_spaces_endpoint(),
    "bucket": config.log_bucket_name,
    "acl": config.digitalocean.spaces_upload_default_acl,
}


def __get_common_comment_params(job_id: int, webhook: GitLabWebhook) -> dict:
    """
    Returns the common parameters to be used in the comment.
    """

    upload_prefix = f"{webhook.instance.name}-{webhook.project['id']}-{job_id}"

    config_file_url = digitalocean_client.upload_file_to_spaces(
        webhook.instance.tutor_config.content_as_string(),
        f"{upload_prefix}-tutor-config.yml",
        **common_file_upload_params,
    )

    grove_file_url = digitalocean_client.upload_file_to_spaces(
        webhook.instance.tutor_config.content_as_string(),
        f"{upload_prefix}-grove-config.yml",
        **common_file_upload_params,
    )

    requirements_file_url = digitalocean_client.upload_file_to_spaces(
        webhook.instance.requirements.content_as_string(),
        f"{upload_prefix}-tutor-requirements.txt",
        **common_file_upload_params,
    )

    return {
        "tutor_config_link": config_file_url,
        "grove_config_link": grove_file_url,
        "requirements_link": requirements_file_url,
    }


def __get_success_comment(webhook: GitLabWebhook) -> str:
    """
    Returns the success comment to be posted on the PR.
    """
    lms_host = "https://" + webhook.instance.tutor_config.content_as_dict()["LMS_HOST"]
    cms_host = "https://" + webhook.instance.tutor_config.content_as_dict()["CMS_HOST"]

    job_id = gitlab_client.get_pipeline_job_id(webhook.jobs, GitLabStatus.SUCCESS)
    comment_params = __get_common_comment_params(job_id, webhook)

    if not all(comment_params.values()):
        logger.warning("Failed to upload some files to DigitalOcean Spaces.")
        return "Sandbox has been deployed successfully"

    return (
        "Sandbox deployment successful.\r\n\r\n"
        f"Sandbox LMS is available at {lms_host}\r\n"
        f"Sandbox Studio is available at {cms_host}\r\n\r\n"
        f"Instance Tutor config : {comment_params['tutor_config_link']}\r\n"
        f"Instance Grove config : {comment_params['grove_config_link']}\r\n"
        f"Instance Python requirements : {comment_params['requirements_link']}"
    )


def __get_failure_comment(webhook: GitLabWebhook) -> str:
    """
    Returns the failure comment to be posted on the PR.
    """
    job_id = gitlab_client.get_pipeline_job_id(webhook.jobs, GitLabStatus.FAILED)

    job_logs_link = digitalocean_client.upload_file_to_spaces(
        gitlab_client.get_pipeline_job_logs(job_id),
        f"{webhook.instance.name}-{webhook.project['id']}-{job_id}.log",
        **common_file_upload_params,
    )

    comment_params = __get_common_comment_params(job_id, webhook)

    if not all(comment_params.values()):
        logger.warning("Failed to upload some files to DigitalOcean Spaces.")
        return (
            "Sandbox deployment failed.\r\n\r\n"
            "Please check the settings and requirements and retry deployment."
        )

    return (
        "Sandbox deployment failed.\r\n\r\n"
        f"Check failure logs here {job_logs_link}\r\n\r\n"
        f"Instance Tutor config : {comment_params['tutor_config_link']}\r\n"
        f"Instance Grove config : {comment_params['grove_config_link']}\r\n"
        f"Instance Python requirements : {comment_params['requirements_link']}\r\n\r\n"
        "Please check the settings and requirements and retry deployment by "
        "updating the pull request description or pushing a new commit"
    )


def handle_gitlab_webhook(event: dict) -> None:
    """
    Handle the incoming GitLab webhook invocation request.

    If the request is not a pipeline status update notification or the request
    secret is invalid, we ignore the request.
    """
    received_secret = os.getenv("Http_X_Gitlab_Token")
    is_pipeline_notification = event.get("object_kind") == "pipeline"

    if get_secret("pr-watcher-gitlab-webhook-secret") != received_secret:
        raise RuntimeError("Invalid webhook secret: %s.", received_secret)

    if not is_pipeline_notification:
        raise RuntimeError("Invalid webhook object: %s.", event.get("object_kind"))

    webhook = GitLabWebhook(gitlab_client, event)

    if not webhook.is_valid:
        raise WebhookInvalidException("Invalid webhook event.")

    if webhook.is_instance_delete_pipeline:
        logger.info("Instance %s is being destroyed.", webhook.instance.name)
        return

    comment = None
    if webhook.is_deployment_succeeded:
        comment = __get_success_comment(webhook)
    elif webhook.is_deployment_failed:
        comment = __get_failure_comment(webhook)

    if comment is None:
        raise ValueError("Empty comment body.")

    github_client.post_comment(webhook.comments_url, comment)


def __handle_state_change(pull_request: PullRequest, instances: list[str]) -> Any:
    """
    Using the given pull request, create or update the corresponding instance.

    If the instance is already running, update the instance with the new
    settings and requirements. Otherwise, create a new instance with the given
    settings and requirements.

    If the instance exceeds the maximum allowed running time or the pull request
    is closed, destroy the instance.
    """
    instance = GroveInstance(gitlab_client, pull_request.sandbox_name)
    pr_is_active = pull_request.has_activity(config.sandbox_destroy_cutoff_days)

    stored_pr_info = (
        instance.grove_config.content_as_dict() if instance.grove_config else {}
    ).get(config.grove.watched_pr_key, {})

    is_pull_request_updated = (
        stored_pr_info.get("commit_ref") != pull_request.commit_sha
        or stored_pr_info.get("applied_settings") != pull_request.extra_settings
        or stored_pr_info.get("applied_requirements") != pull_request.tutor_requirements
    )

    is_pr_open = pull_request.state == PullRequestState.OPEN
    is_pr_active_not_defined = (not instance.is_defined) and pr_is_active
    is_pr_updated_defined = instance.is_defined and is_pull_request_updated

    if is_pr_open and (is_pr_active_not_defined or is_pr_updated_defined):
        logger.info("Deploying instance %s.", instance.name)

        is_mfe_pull_request = pull_request.repo_name.startswith(
            config.openedx.mfe_repo_name_prefix
        )
        mfe_info = MFEInfo(
            mfe_name=get_mfe_name(pull_request.fork_name),
            repository_url=pull_request.clone_url,
            branch=pull_request.branch_name,
        )

        edx_platform_url = (
            config.openedx.default_platform_url
            if is_mfe_pull_request
            else pull_request.clone_url
        )
        edx_platform_branch = (
            config.openedx.default_platform_branch
            if is_mfe_pull_request
            else pull_request.branch_name
        )

        return gitlab_client.trigger_instance_create_or_update_pipeline(
            params=CreateOrUpdatePipelineParams(
                instance=instance,
                pull_request=pull_request,
                edx_platform_url=edx_platform_url,
                edx_platform_branch=edx_platform_branch,
                mfe_info=None if not is_mfe_pull_request else mfe_info,
            ),
        )

    should_destroy = instance.is_defined and (
        (instance.name in instances and not pr_is_active)
        or (pull_request.state == PullRequestState.CLOSED)
    )

    if should_destroy:
        logger.info("Destroying instance %s.", instance.name)
        return gitlab_client.trigger_instance_destroy_pipeline(
            DestroyPipelineParams(instance=instance)
        )

    logger.debug("The instance state of %s did not change.", instance.name)


def handle_cron_invocation(event: str) -> None:
    """
    Handle the incoming cron job invocation request.

    Whenever a cron job triggers the function, the function checks the list of
    watched pull requests and creates or updates the corresponding instances.

    If an instance is not needed anymore, the function destroys the instance.
    """
    authorized_users = github_client.get_authorized_usernames(
        config.github.authorized_users,
        config.github.authorized_teams,
    )
    watched_repositories = github_client.get_watched_repositories(
        config.github.watched_organizations,
        config.openedx.watched_forks,
        config.sandbox_watched_topic,
    )
    namespaces = kubernetes_client.get_namespaces()

    instances = [ns for ns in namespaces if re.match(SANDBOX_INSTANCE_PATTERN, ns)]
    worker = partial(__handle_state_change, instances=instances)

    for repository in watched_repositories:
        executor = ThreadPoolExecutor()
        open_pull_requests = github_client.get_pull_requests(
            repository,
            authorized_users,
            PullRequestState.OPEN,
            config.sandbox_create_topic,
        )
        closed_pull_requests = github_client.get_pull_requests(
            repository,
            authorized_users,
            PullRequestState.CLOSED,
            config.sandbox_create_topic,
        )

        # Consume the iterator to trigger the execution of the workers.
        list(executor.map(worker, open_pull_requests + closed_pull_requests))


def handle(body: str) -> dict:
    """
    Handle the incoming invocation request.

    The request body is a string that may or may not be a valid JSON. If the
    body is not a valid JSON, we handle the request as a cron job invocation.

    Otherwise, we handle the request as a GitLab webhook invocation. When the
    function is called by a GitLab webhook, it is expected to be a pipeline
    status update notification. We handle the notification as the following:

    - If the notification is about a successful pipeline, we send a notification
        to the PR author in the form of a GitHub PR comment on the affected PR.
    - If the notification is about a failed pipeline, we collect the build logs,
        instance logs, and store them in a bucket. Then, we send a notification
        to the PR author in the form of a GitHub PR comment on the affected PR.

    The GitLab webhook secret is expected and validated. If the secret is not
    provided or invalid, we ignore the request and return a 200 response.

    The function returns a 200 response even if the execution is failed. This is
    to avoid GitLab from retrying the request or block the webhook due to failed
    executions.
    """

    try:
        body = json.loads(body)
        logger.debug("Handling request as a GitLab webhook invocation.")
        handler = handle_gitlab_webhook
    except json.JSONDecodeError:
        logger.debug("Request payload was not JSON: %s.", body)
        logger.debug("Handling request as a cron job invocation.")
        handler = handle_cron_invocation

    try:
        handler(body)
        return {"success": True, "message": "Request handled."}
    except WebhookInvalidException as exc:
        return {"success": False, "message": str(exc), "ingorable": True}
    except Exception as exc:
        logger.exception("Failed to handle the request. Exception: %s.", exc)
        return {"success": False, "message": str(exc), "ingorable": False}
