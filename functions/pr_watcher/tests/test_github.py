from unittest.mock import call, patch


from ..conf import config
from ..github import GitHubClient, PullRequest, PullRequestState
from ..openedx import NamedRelease


def test_pull_request_extra_settings(open_platform_pull_request_info):
    pr = PullRequest(open_platform_pull_request_info)
    assert pr.extra_settings == "EDXAPP_EXTRA_SETTINGS: my extra settings"


def test_pull_request_tutor_requirements(open_platform_pull_request_info):
    pr = PullRequest(open_platform_pull_request_info)
    requirements = list(filter(lambda x: x, pr.tutor_requirements.splitlines()))
    assert requirements == [
        "tutor>=16,<17",
        "tutor-discovery>=16,<17",
        "tutor-ecommerce>=16,<17",
        "tutor-mfe>=16,<17",
        "tutor-xqueue>=16,<17",
        "tutor-forum>=16,<17",
        "git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@v16.0.0",
        "git+https://github.com/hastexo/tutor-contrib-s3.git@v1.2.0",
    ]


def test_pull_request_sandbox_name(open_platform_pull_request_info):
    pr = PullRequest(open_platform_pull_request_info)
    assert pr.sandbox_name == "pr-608-0e7f8a"


def test_pull_request_named_release(open_platform_pull_request_info):
    pr = PullRequest(open_platform_pull_request_info)
    assert pr.named_release == NamedRelease.PALM


@patch("pr_watcher.github.requests")
def test_github_client_get_object(mock_requests, open_platform_pull_request_info):
    url = "https://api.github.com/repos/openedx/edx-platform/pulls/27788"
    mock_requests.get.return_value.json.return_value = open_platform_pull_request_info

    client = GitHubClient(
        token="test",
        user_agent=config.user_agent,
    )

    obj = client.get_object(url)

    assert obj == open_platform_pull_request_info
    mock_requests.get.assert_called_once_with(url, **client.api_params)


@patch("pr_watcher.github.requests")
def test_github_client_get_authorized_usernames(mock_requests, team_members):
    url = "https://api.github.com/orgs/openedx/teams/pr-authors/members"
    mock_requests.get.return_value.json.return_value = team_members

    client = GitHubClient(
        token="token",
        user_agent=config.user_agent,
    )

    obj = client.get_authorized_usernames(["kaustavb"], ["openedx/pr-authors"])

    assert obj == {"open-craft-grove", "gabor-boros", "kaustavb"}
    mock_requests.get.assert_called_once_with(url, **client.api_params)


@patch("pr_watcher.github.requests")
def test_github_client_get_watched_repositories(mock_requests, watched_repositories):
    url = f"https://api.github.com/search/repositories?q=topic:{config.sandbox_watched_topic} fork:true org:openedx"
    mock_requests.get.return_value.json.return_value = watched_repositories

    client = GitHubClient(
        token="token",
        user_agent=config.user_agent,
    )

    obj = client.get_watched_repositories(["openedx"], [], config.sandbox_watched_topic)

    assert obj == {"openedx/edx-platform"}
    mock_requests.get.assert_called_once_with(url, **client.api_params)


@patch("pr_watcher.github.requests")
def test_github_client_get_pull_requests(
    mock_requests,
    open_platform_pull_request_info,
    open_platform_pull_requests_info,
):
    url = 'https://api.github.com/search/issues?sort=created&q=is:pr is:open repo:open-craft/edx-platform label:"create-sandbox" author:kaustavb12'
    mock_requests.get.return_value.json.side_effect = [
        open_platform_pull_requests_info,
        open_platform_pull_request_info,
    ]

    client = GitHubClient(
        token="token",
        user_agent=config.user_agent,
    )

    prs = client.get_pull_requests(
        "open-craft/edx-platform",
        ["kaustavb12"],
        PullRequestState.OPEN,
        "create-sandbox",
    )

    assert [o.__dict__ for o in prs] == [
        PullRequest(open_platform_pull_request_info).__dict__
    ]

    calls = mock_requests.get.call_args_list
    assert call(url, **client.api_params) in calls
    assert call(open_platform_pull_request_info["url"], **client.api_params) in calls


@patch("pr_watcher.github.requests")
def test_github_client_post_comment(mock_requests):
    url = "https://api.github.com/repos/openedx/edx-platform/issues/27788/comments"
    mock_requests.post.return_value.json.return_value = {}

    client = GitHubClient(
        token="token",
        user_agent=config.user_agent,
    )

    client.post_comment(url, "test comment")

    mock_requests.post.assert_called_once_with(
        url,
        json={"body": "test comment"},
        **client.api_params,
    )
