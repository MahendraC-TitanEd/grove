from ..utils import truncate


def test_truncate():
    test_cases = (
        ("short text", "short text"),
        ("a" * 50, "a" * 50),
        ("a" * 51, "a" * 47 + "..."),
    )

    for text, expected in test_cases:
        assert truncate(text) == expected
