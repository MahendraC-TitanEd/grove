import json
import os
from base64 import b64encode
from pathlib import Path

from pytest import fixture

TEST_ASSETS_DIR = Path(__file__).parent / "assets"

MOCK_ENV_VARIABLES = {
    "OPENEDX_WATCHED_FORKS": '["open-craft/edx-platform"]',
    "OPENEDX_MFE_FORKS": '["open-craft/frontend-app-authn"]',
    "OPENEDX_DEFAULT_PLATFORM_URL": "https://github.com/open-craft/edx-platform",
    "GROVE_PROJECT_ID": "test",
    "GITHUB_AUTHORIZED_USERS": '["testuser"]',
    "GITHUB_AUTHORIZED_TEAMS": '["open-craft/testteam"]',
    "GITHUB_WATCHED_ORGANIZATIONS": '["open-craft"]',
    "DIGITALOCEAN_SPACES_ENDPOINT_REGION": "nyc3",
}


def pytest_configure(config):
    """
    Configure pytest before running any tests.
    """
    for k, v in MOCK_ENV_VARIABLES.items():
        os.environ.setdefault(k, v)


@fixture
def open_platform_pull_request_info() -> dict:
    """
    Returns the pull request information of an open pull request.
    """
    with open(TEST_ASSETS_DIR / "edx-platform-open-pr.json") as f:
        return json.loads(f.read())


@fixture
def open_platform_pull_requests_info() -> dict:
    """
    Returns the list of opened pull requests.
    """
    with open(TEST_ASSETS_DIR / "edx-platform-filtered-prs.json") as f:
        return json.loads(f.read())


@fixture
def merged_platform_pull_request_info() -> dict:
    """
    Returns the pull request information of a merged pull request.
    """
    with open(TEST_ASSETS_DIR / "edx-platform-merged-pr.json") as f:
        return json.loads(f.read())


@fixture
def watched_repositories() -> list[dict]:
    """
    Returns the list of repositories labeled with the watch topic.
    """
    with open(TEST_ASSETS_DIR / "watched-repositories.json") as f:
        return json.loads(f.read())


@fixture
def team_members() -> list[dict]:
    """
    Returns the list of team members.
    """
    with open(TEST_ASSETS_DIR / "team-members.json") as f:
        return json.loads(f.read())


@fixture
def gitlab_webhook_payload() -> dict:
    """
    Returns the payload of a GitLab webhook.
    """
    with open(TEST_ASSETS_DIR / "gitlab-webhook-payload.json") as f:
        return json.loads(f.read())


@fixture
def gitlab_tutor_config_file_info() -> dict:
    """
    Returns the pull request information of a config.yml file.
    """
    return {
        "file_name": "config.yml",
        "file_path": "/testdir/config.yml",
        "size": 10,
        "encoding": "utf-8",
        "content": b64encode(
            b"".join(
                [
                    b"LMS_HOST: maw-upgrade.staging.do.opencraft.hosting",
                    b"CMS_HOST: studio.maw-upgrade.staging.do.opencraft.hosting",
                ]
            )
        ),
        "content_sha256": "test-sha256",
        "ref": "test-ref",
        "blob_id": "test-blob-id",
        "commit_id": "test-commit-id",
        "last_commit_id": "test-last-commit-id",
    }


@fixture
def gitlab_tutor_config_file(gitlab_tutor_config_file_info):
    """
    Returns a GitLabFile instance for a config.yml file.
    """
    from pr_watcher.gitlab import GitLabFile

    return GitLabFile(gitlab_tutor_config_file_info)


@fixture
def gitlab_grove_config_file_info() -> dict:
    """
    Returns the pull request information of a grove.yml file.
    """
    return {
        "file_name": "grove.yml",
        "file_path": "/testdir/grove.yml",
        "size": 10,
        "encoding": "utf-8",
        "content": b64encode(
            b"""
PR_WATCHER:
    commit_ref: f507dbeca304aa7a943dee7466dde022eed1ad56
    applied_settings: ''
    applied_requirements: 'git+https://github.com/overhangio/tutor.git@nightly
        git+https://github.com/overhangio/tutor-discovery.git@nightly
        git+https://github.com/overhangio/tutor-ecommerce.git@nightly
        git+https://github.com/overhangio/tutor-mfe.git@nightly
        git+https://github.com/overhangio/tutor-xqueue.git@nightly
        git+https://github.com/overhangio/tutor-forum.git@nightly
        git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@main
        git+https://github.com/hastexo/tutor-contrib-s3.git@main'
    url: https://github.com/open-craft/edx-platform/pull/608
    comments_url: https://api.github.com/repos/open-craft/edx-platform/issues/608/comments
            """
        ),
        "content_sha256": "test-sha256",
        "ref": "test-ref",
        "blob_id": "test-blob-id",
        "commit_id": "test-commit-id",
        "last_commit_id": "test-last-commit-id",
    }


@fixture
def gitlab_grove_config_file(gitlab_grove_config_file_info):
    """
    Returns a GitLabFile instance for a grove.yml file.
    """
    from pr_watcher.gitlab import GitLabFile

    return GitLabFile(gitlab_grove_config_file_info)


@fixture
def gitlab_requirements_file_info() -> dict:
    """
    Returns the pull request information of a requirements.txt file.
    """
    return {
        "file_name": "requirements.txt",
        "file_path": "/testdir/requirements.txt",
        "size": 10,
        "encoding": "utf-8",
        "content": b64encode(
            b"".join(
                [
                    b"git+https://github.com/overhangio/tutor.git@nightly",
                    b"git+https://github.com/overhangio/tutor-discovery.git@nightly",
                    b"git+https://github.com/overhangio/tutor-ecommerce.git@nightly",
                    b"git+https://github.com/overhangio/tutor-mfe.git@nightly",
                    b"git+https://github.com/overhangio/tutor-xqueue.git@nightly",
                    b"git+https://github.com/overhangio/tutor-forum.git@nightly",
                    b"git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@main",
                    b"git+https://github.com/hastexo/tutor-contrib-s3.git@main",
                ]
            )
        ),
        "content_sha256": "test-sha256",
        "ref": "test-ref",
        "blob_id": "test-blob-id",
        "commit_id": "test-commit-id",
        "last_commit_id": "test-last-commit-id",
    }


@fixture
def gitlab_requirements_file(gitlab_requirements_file_info):
    """
    Returns a GitLabFile instance for a requirements.txt file.
    """
    from pr_watcher.gitlab import GitLabFile

    return GitLabFile(gitlab_requirements_file_info)
