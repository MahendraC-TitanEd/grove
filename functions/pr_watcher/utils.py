"""
Utility functions applicable to the whole project
"""

import os
from pathlib import Path
from time import time

# The default TTL is 10 minutes. This is enough time to let the function finish
# its execution, but not too much to keep the cache valid for too long.
DEFAULT_CACHE_TTL = 600

# Make the path configurable for testing
SECRET_DIR = os.getenv("KUBERNETES_SECRET_DIR", "/var/openfaas/secrets")


def get_secret(name: str, raise_error: bool = True) -> str:
    """
    Return a secret mapped as a file by OpenFAAS.

    Args:
        name: name of the secret

    Returns:
        Value secret value from the file.
    """

    if not is_secret_exists(name):
        if not raise_error:
            return ""
        raise ValueError(f"Secret {name} not found")

    return open(get_secret_path(name), "r").read().strip()


def get_secret_path(name: str) -> Path:
    """
    Return the given secrets' absolute path.

    Args:
        name: name of the secret

    Returns:
        Absolute path for the named secret.
    """

    return Path(f"{SECRET_DIR}/{name}").absolute()


def is_secret_exists(name: str) -> bool:
    """
    Return whether the given secret exists.

    Args:
        name: name of the secret

    Returns:
        True if the secret exists, False otherwise.
    """

    return get_secret_path(name).exists()


def get_ttl_hash(ttl: int = DEFAULT_CACHE_TTL) -> str:
    """
    Returns the hash of the current time divided by the TTL.

    This function is used to invalidate LRU cache after a certain amount of
    time, exploiting the fact that the LRU cache is based on the arguments of
    the function.
    """
    return round(time() / ttl)


def get_nonce() -> str:
    """
    Returns a random nonce so that the cache is invalidated.
    """
    return os.urandom(16).hex()


def truncate(text: str, max_length: int = 50) -> str:
    """
    Truncates the provided text to the provided max length.
    """
    if len(text) > max_length:
        return f"{text[:max_length-3]}..."
    return text
