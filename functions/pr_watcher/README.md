# PR watcher

This function watches pull requests in the provided repositories and deploys
those instances to the cluster.

## Prerequisites

1. Working Grove cluster
2. OpenFAAS CLI installed

## Installation

1. Get the OpenFAAS admin password by executing `./tf output -json openfaas_ingress_password`.
2. Change into the `grove/functions` library, e.g. `cd my-cluster/grove/functions/`
3. Login to the Grove hosted OpenFAAS and GitLab container registry

   ```shell
   export OPENFAAS_URL=https://openfaas.<YOUR_CLUSTER_HOSTNAME>
   faas-cli login --tls-no-verify -g $OPENFAAS_URL -u admin -p "<OPENFAAS_ADMIN_PASSWORD>"
   docker login <GITLAB_CI_REGISTRY> --username `<GITLAB_USERNAME>` --password `<GITLAB_PASSWORD>`
   ```

3. Create a service account for the pr-watcher function which will allow access to Kubernetes resources for the sake of retrieving build logs from jobs.

   ```shell
   # Create the service account -- the file path is intentionally starting with /workspace
   ./kubectl apply -f /workspace/grove/functions/pr_watcher/resources.yml
   ```

5. Create a secret with your [Github access token](https://github.com/settings/tokens) and optionally the GitHub webhook secret

   ```shell
   ./kubectl --namespace openfaas-fn create secret generic pr-watcher-github-access-token \
      --from-literal pr-watcher-github-access-token="<SECRET>"
   ```

6. Create a secret with your [Gitlab access token](https://https://gitlab.com/-/profile/personal_access_tokens) and optionally the GitLab webhook secret
   ```shell
   ./kubectl --namespace openfaas-fn create secret generic pr-watcher-gitlab-access-token \
      --from-literal pr-watcher-gitlab-access-token="<SECRET>"

   ./kubectl --namespace openfaas-fn create secret generic pr-watcher-gitlab-webhook-secret \
      --from-literal pr-watcher-gitlab-webhook-secret="<SECRET>"
   ```

7. Create a secret with your Gitlab pipeline trigger token, found in your projects CI/CD settings.
   ```shell
   ./kubectl --namespace openfaas-fn create secret generic pr-watcher-gitlab-pipeline-token \
      --from-literal pr-watcher-gitlab-pipeline-token=""
   ```

8. Create a secret with your [DigitalOcean Spaces Access Key ID](https://docs.digitalocean.com/products/spaces/how-to/manage-access/#access-keys)
   ```shell
   ./kubectl --namespace openfaas-fn create secret generic pr-watcher-spaces-access-key-id \
      --from-literal pr-watcher-spaces-access-key-id="<SECRET>"
   ```

9. Create a secret with your [DigitalOcean Spaces Access Key Secret](https://docs.digitalocean.com/products/spaces/how-to/manage-access/#access-keys)
   ```shell
   ./kubectl --namespace openfaas-fn create secret generic pr-watcher-spaces-access-key-secret \
      --from-literal pr-watcher-spaces-access-key-secret="<SECRET>"
   ```

10. If you did not override the function in any way, you don't need to build and push the image as that image is managed by OpenCraft. If you've made changes to the function, you need to build and push before deploying:

    ```shell
    faas-cli build -f pr-watcher.yml --build-arg ADDITIONAL_PACKAGE=git
    faas-cli push -f pr-watcher.yml
    ```
11. Deploy the function as shown below:

    ```shell
    faas-cli deploy \
       -f pr-watcher.yml \
       --env GROVE_PROJECT_ID='34602668' \
       --env OPENEDX_MFE_FORKS='[]' \
       --env OPENEDX_WATCHED_FORKS='[]' \
       --env GITHUB_AUTHORIZED_USERS='[]' \
       --env GITHUB_AUTHORIZED_TEAMS='["openedx-pr-sandbox-creators"]' \
       --env GITHUB_WATCHED_ORGANIZATIONS='["openedx"]' \
       --env PR_WATCHER_SANDBOX_WATCHED_TOPIC='grove-sandbox-enabled' \
       --env PR_WATCHER_SANDBOX_CREATE_TOPIC='create-sandbox' \
       --env PR_WATCHER_SANDBOX_DESTROY_CUTOFF_DAYS='14' \
       --gateway "${OPENFAAS_URL}"
    ```

Now, the function is ready to use. You can call the function by using the basic auth credentials used for logging in to OpenFAAS UI by providing it in the URL. Example: `https://admin:<OPENFAAS_ADMIN_PASSWORD>@openfaas.<YOUR_CLUSTER_URL>/function/pr-watcher`.

## How it works

When the function is invoked it checks for any Open or Closed PRs made by the users listed in the `GITHUB_AUTHORIZED_USERS` and users of teams listed in `GITHUB_AUTHORIZED_TEAMS` environment variable made to any repo listed in the `OPENEDX_WATCHED_FORKS` environment variable.

Defining the `GITHUB_WATCHED_ORGANIZATIONS` and `PR_WATCHER_SANDBOX_WATCHED_TOPIC` are completely optional. These settings allows the tagged repositories within the given organizations to be watched. For example, if `GITHUB_WATCHED_ORGANIZATIONS` is set to `open-craft` and the `PR_WATCHER_SANDBOX_WATCHED_TOPIC` is `has-sandbox`, then every repository owned by OpenCraft that has the `has-sandbox` topic will be watched.

The `PR_WATCHER_SANDBOX_CREATE_TOPIC` setting ensures that only those PRs will have sandboxes from authorized users that are tagged with the given label.

PRs need to contain a `**Settings**` block of the format in order for instance's to be created.:

~~~markdown
**Settings**
```yaml
SETTING: value
```
~~~

- Instances will be created for open pull requests and only updated once the instance has been added to the `grove-template` repository.
- Instances names are of the format pr-{pr-number}-{fork-hash} so that operators can watch more than one fork and not have collisions
- When an instance is created/updated a `PR_WATCHER` key is added to grove.yml to keep track of what was last deployed.

Once a PR is closed, the corresponding instance will be destroyed as well.

## Configuration

The environment variables that can be configured are:

- `USER_AGENT` (default: `"grove-cluster-watcher/v1.0.0"`): The user agent used for any outgoing requests with Gitlab/Github.
- TIME_ZONE( default: `"UTC"`): The timezone used for any timestamps.
- GITHUB_USERNAMES (default: `""`): `;`-separated list of GitHub usernames to watch. E.g. `"user1;user2"`
- GITHUB_TEAMS (default: `""`): `;`-separated list of `/`-separated GitHub org and teams pairs to watch. E.g. `"org1/team1;org1/team2;org2/team1"`
- SPACES_ENDPOINT (default: `"https://nyc3.digitaloceanspaces.com"`): DigitalOcean Spaces endpoint used to upload build failure logs.
- SPACES_REGION (default: `"nyc3"`): DigitalOcean Spaces region of bucket where build failure logs are uploaded.
- LOGS_BUCKET (default: `"grove-stage-build-logs"`): Name of DigitalOcean Spaces bucket where build failure logs are uploaded.
- EDX_WATCHED_FORKS (default: `"openedx/edx-platform"`): `;`-separated list of edx-platform forks to watch. Any PRs made against these repos will be checked.
- MFE_FORKS( default: `""`): `;`-separated list of MFE repos to watch. Any PRs made against these repos will be checked.
- DEFAULT_EDX_PLATFORM_URL( default: `"https://github.com/openedx/edx-platform.git"`): edx-platform fork to be used for MFE deployments by default.
- DEFAULT_EDX_PLATFORM_BRANCH( default: `"master"`): edx-platform branch to be used for MFE deployments by default.
- GITLAB_BRANCH_NAME (default: `"main"`): The main branch name of your `grove-template` fork.
- GROVE_TEMPLATE_PROJECT_ID( default: `""`): The Project ID of your `grove-template` fork.
- GITLAB_HOST( default: `"gitlab.com"`): The host name of your Gitlab instance, if not hosted on `gitlab.com`.
- MFE_REPO_NAME_PREFIX( default: `"frontend-app-"`): MFE repo name prefix. This is used to determine the name of the MFE from the repo name.
- MFE_PORT( default: `"18000"`): Default port to be used to deploy the MFE.

## Testing

Tests can be invoked by running `pytest` in the `pr_watcher` directory.