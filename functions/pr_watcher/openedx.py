from enum import StrEnum

from pydantic import BaseModel, Field

from .conf import config

NAMED_RELEASE_NUTMEG = "nutmeg"
NAMED_RELEASE_OLIVE = "olive"
NAMED_RELEASE_PALM = "palm"
NAMED_RELEASE_QUINCE = "quince"
NAMED_RELEASE_MASTER = "master"  # Special case for master branch

NAMED_RELEASE_LATEST_COMMON_VERSIONS: dict[str, str] = {
    NAMED_RELEASE_NUTMEG: "open-release/nutmeg.3",
    NAMED_RELEASE_OLIVE: "open-release/olive.4",
    NAMED_RELEASE_PALM: "open-release/palm.4",
    NAMED_RELEASE_QUINCE: "open-release/quince.1",
    NAMED_RELEASE_MASTER: "master",
}

NAMED_RELEASE_TUTOR_REQUIREMENTS: dict[str, list[str]] = {
    NAMED_RELEASE_NUTMEG: [
        "tutor>=14,<15",
        "tutor-discovery>=14,<15",
        "tutor-ecommerce>=14,<15",
        "tutor-mfe>=14,<15",
        "tutor-xqueue>=14,<15",
        "git+https://github.com/open-craft/tutor-forum.git@opencraft-release/nutmeg.2",
        "git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@v0.6.0",
        "git+https://github.com/hastexo/tutor-contrib-s3.git@v1.0.0",
    ],
    NAMED_RELEASE_OLIVE: [
        "tutor>=15,<16",
        "tutor-discovery>=15,<16",
        "tutor-ecommerce>=15,<16",
        "tutor-mfe>=15,<16",
        "tutor-xqueue>=15,<16",
        "tutor-forum>=15,<16",
        "git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@v0.6.0",
        "git+https://github.com/hastexo/tutor-contrib-s3.git@v1.1.0",
    ],
    NAMED_RELEASE_PALM: [
        "tutor>=16,<17",
        "tutor-discovery>=16,<17",
        "tutor-ecommerce>=16,<17",
        "tutor-mfe>=16,<17",
        "tutor-xqueue>=16,<17",
        "tutor-forum>=16,<17",
        "git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@v16.0.0",
        "git+https://github.com/hastexo/tutor-contrib-s3.git@v1.2.0",
    ],
    NAMED_RELEASE_QUINCE: [
        "tutor>=17,<18",
        "tutor-discovery>=17,<18",
        "tutor-ecommerce>=17,<18",
        "tutor-mfe>=17,<18",
        "tutor-xqueue>=17,<18",
        "tutor-forum>=17,<18",
        "git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@main",
        "git+https://github.com/open-craft/tutor-contrib-s3.git@99a3ad5acc13d817e87b47b3d65d6002f0487062",
    ],
    NAMED_RELEASE_MASTER: [
        "git+https://github.com/overhangio/tutor.git@nightly",
        "git+https://github.com/overhangio/tutor-discovery.git@nightly",
        "git+https://github.com/overhangio/tutor-ecommerce.git@nightly",
        "git+https://github.com/overhangio/tutor-mfe.git@nightly",
        "git+https://github.com/overhangio/tutor-xqueue.git@nightly",
        "git+https://github.com/overhangio/tutor-forum.git@nightly",
        "git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@main",
        "git+https://github.com/open-craft/tutor-contrib-s3.git@99a3ad5acc13d817e87b47b3d65d6002f0487062",
    ],
}


class MFEInfo(BaseModel):
    """
    Pull request information for the trigger payload for MFEs.
    """

    mfe_name: str = Field(..., description="Name of the MFE.")
    repository_url: str = Field(..., description="URL of the MFE repository.")
    branch: str = Field(..., description="Branch of the MFE.")
    port: str = Field(18000, description="Port of the MFE.")


class NamedRelease(StrEnum):
    """
    Possible named releases of Open edX that is supported by the pull request
    watcher.
    """

    NUTMEG = NAMED_RELEASE_NUTMEG
    OLIVE = NAMED_RELEASE_OLIVE
    PALM = NAMED_RELEASE_PALM
    QUINCE = NAMED_RELEASE_QUINCE
    MASTER = NAMED_RELEASE_MASTER
    UNKNOWN = "unknown"

    def __str__(self):
        return self.value

    def values() -> list[str]:
        """
        Returns all the possible values of the enum.
        """
        return [str(release) for release in NamedRelease]

    @property
    def release_name(self) -> str:
        """
        Returns the release name of the named release.

        If the named release is unknown, the release name of the master branch
        is returned.
        """
        return self.value

    @property
    def tutor_requirements(self) -> list[str]:
        """
        Returns the necessary tutor requirements with their versions for the
        given named release.

        If the named release is unknown, the tutor requirements for the master
        branch are returned.
        """
        return NAMED_RELEASE_TUTOR_REQUIREMENTS.get(
            self.value, NAMED_RELEASE_TUTOR_REQUIREMENTS[NAMED_RELEASE_MASTER]
        )

    @property
    def latest_common_version(self) -> str:
        """
        Returns the latest common version of the named release.

        If the named release is unknown, the latest common version of the master
        branch is returned.
        """
        return NAMED_RELEASE_LATEST_COMMON_VERSIONS.get(
            self.value, NAMED_RELEASE_LATEST_COMMON_VERSIONS[NAMED_RELEASE_MASTER]
        )


def get_mfe_name(repository: str) -> str:
    """
    Returns a resonable MFE name based on the repository name.
    """
    return repository.split(config.openedx.mfe_repo_name_prefix, 1)[-1]
