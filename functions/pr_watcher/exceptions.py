class WebhookInvalidException(Exception):
    """
    Exception raised when a webhook is invalid.
    """


class ObjectDoesNotExist(Exception):
    """
    Exception raised when accessing an object that does not exist.
    """


class RateLimitExceeded(Exception):
    """
    Exception raised when accessing an object and a rate limit is hit.
    """
