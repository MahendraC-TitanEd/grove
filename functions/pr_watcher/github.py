"""
GitHub module provides a client for interacting with the GitHub API.

The GitHub API is used to retrieve information about pull requests, get watched
repositories, and get the list of usernames in a team.
"""

from datetime import datetime
import hashlib
import logging
import re
from enum import StrEnum

import requests
from methodtools import lru_cache


from .exceptions import ObjectDoesNotExist, RateLimitExceeded
from .openedx import NamedRelease
from .utils import get_ttl_hash

logger = logging.getLogger(__name__)


class PullRequestState(StrEnum):
    """
    Possible states of a pull request.

    A pull request can have multiple states when it is opened or closed, but we
    are not making a distinction between them. We only care if it's open or not.
    """

    OPEN = "open"
    CLOSED = "closed"


class PullRequest:
    """
    Utility class representing a GitHub pull request.
    """

    def __init__(self, info: dict):
        self.number: int = info["number"]
        self.title: str = info["title"]
        self.state: PullRequestState = PullRequestState(info["state"])
        self.author: str = info["user"]["login"]
        self.body: str = info["body"] or ""

        self.fork_name: str = info["head"]["repo"]["full_name"]
        self.repo_name: str = info["base"]["repo"]["name"]
        self.branch_name: str = info["head"]["ref"]
        self.target_branch: str = info["base"]["ref"]
        self.commit_sha: str = info["head"]["sha"]

        # Last activity date on the PR regardless if that was a comment or some
        # code change.
        self.updated_at: datetime = datetime.strptime(
            info["updated_at"], "%Y-%m-%dT%H:%M:%SZ"
        )

        # Regardless its name, the HTML URL points to the pull request.
        self.html_url: str = info["_links"]["html"]["href"]
        self.clone_url: str = info["head"]["repo"]["clone_url"]
        self.comments_url: str = info["_links"]["comments"]["href"]

    @property
    def extra_settings(self) -> str:
        """
        Returns the extra settings from the PR body.
        """
        return self.__extract_from_body(r"[Ss]ettings", self.body) or ""

    @property
    def tutor_requirements(self) -> str:
        """
        Returns the tutor requirements from the PR body.
        """
        return self.__extract_from_body(
            r"[Tt]utor\ requirements", self.body
        ) or "\n".join(self.named_release.tutor_requirements)

    @property
    def sandbox_name(self) -> str:
        """
        Returns the name of the sandbox environment that should be created for
        this pull request.
        """
        hash_object = hashlib.sha1(self.fork_name.encode("utf8"))
        fork_hash = hash_object.hexdigest()[:6]
        return f"pr-{self.number}-{fork_hash}"

    @property
    def named_release(self) -> NamedRelease:
        """
        Returns the name of the target release.
        """
        branch_release_name = self.target_branch.split("/")[-1]
        return NamedRelease(branch_release_name.split(".")[0])

    def __str__(self) -> str:
        """
        Returns a string representation of the pull request.
        """
        return f"Pull request {self.title} (#{self.number}) by {self.author}"

    def has_activity(self, days: int) -> bool:
        """
        Returns whether the pull request has activity since the given date.
        """
        since_last_update = datetime.now() - self.updated_at
        return since_last_update.days <= days

    def __extract_from_body(self, key: str, body: str) -> str | None:
        """
        Extracts the value of a setting from the PR body.
        """
        pattern = (
            f"..{key}..((\\r)?\\n)+(```(?P<format>[a-z]+)?)\r?\n(?P<content>[^`]*?)```"
        )
        if match := re.search(pattern, body):
            return match.group("content").strip()


class GitHubClient:
    """
    Client for interacting with the GitHub API.
    """

    def __init__(self, token: str, user_agent: str) -> None:
        self.api_headers = {
            "Authorization": f"Bearer {token}",
            "Accept": "application/vnd.github+json",
            "User-Agent": user_agent,
            "Time-Zone": "UTC",
        }
        self.api_params = {"headers": self.api_headers, "timeout": 30}

    @lru_cache()
    def get_object(self, url: str, ttl=None) -> dict:
        """
        Send the request to the provided URL, attaching custom headers, and
        returns the deserialized object from the returned JSON.

        Raises ObjectDoesNotExist if github returns a 404 response.
        """
        del ttl  # TTL is used to invalidate the cache.

        resp = requests.get(url, **self.api_params)

        if resp.status_code == 404:
            raise ObjectDoesNotExist(f"404 response from {url}")

        remaining_requests = resp.headers.get("X-RateLimit-Remaining")
        if resp.status_code == 403 and remaining_requests == "0":
            raise RateLimitExceeded(f"Rate limit exceeded when querying {url}")

        resp.raise_for_status()
        return resp.json()

    def get_authorized_usernames(
        self, usernames: list[str], teams: list[str]
    ) -> set[str]:
        """
        Returns the unique list of usernames that can trigger PR sandboxes.

        The list is composed of the usernames defined in the configuration and the
        members of the allowed teams.
        """
        usernames = set(usernames)

        for team in teams:
            org, team = team.split("/")
            members = self.get_object(
                f"https://api.github.com/orgs/{org}/teams/{team}/members",
                get_ttl_hash(),
            )

            usernames.update([m["login"] for m in members])

        return usernames

    def get_watched_repositories(
        self,
        organizations: list[str],
        platform_repositories: list[str],
        topic: str,
    ) -> set[str]:
        """
        Returns the watched repositories.

        The list is composed of the repositories defined in the configuration and
        the repositories that have a given topic name defined.
        """
        tagged_repos = set(platform_repositories)

        if organizations and topic:
            query = [f"topic:{topic}", "fork:true"]
            query.extend(f"org:{org}" for org in organizations)

            url = f"https://api.github.com/search/repositories?q={' '.join(query)}"
            repos = self.get_object(url, get_ttl_hash())["items"]

            tagged_repos.update([repo["full_name"] for repo in repos])

        return tagged_repos

    def get_pull_requests(
        self,
        repository: str,
        authors: list[str] | None,
        state: PullRequestState,
        topic: str,
    ) -> list[PullRequest]:
        """
        Returns the list of pull requests for the given repository.
        """
        query = [
            "is:pr",
            f"is:{state}",
            f"repo:{repository}",
            f'label:"{topic}"',
        ]

        if authors:
            query.extend([f"author:{author}" for author in authors])

        order = "created" if state == PullRequestState.OPEN else "closed"
        url = f"https://api.github.com/search/issues?sort={order}&q={' '.join(query)}"
        pull_requests = self.get_object(url, get_ttl_hash())["items"]

        return [
            PullRequest(self.get_object(pr["pull_request"]["url"], get_ttl_hash()))
            for pr in pull_requests
        ]

    def post_comment(self, url: str, comment: str) -> None:
        """
        Posts a comment on the given URL.
        """
        requests.post(url, json={"body": comment}, **self.api_params)
