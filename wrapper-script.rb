#!/usr/bin/env ruby
require 'digest'
require 'json'
require 'yaml'

# This wrapper script helps run Terraform or kbuectl locally, ensuring that the correct version of Terraform/kubectl
# is used, the GitLab Terraform remote state backend is configured, and so on.
#
# Why is it written in Ruby?!?!?!
# Because Ruby seems to be the only scripting language installed on Mac and Linux by default
# that includes a YAML parser in its standard library :/


########################################################################################################################
# Make sure the user is running this script from the correct folder
########################################################################################################################

if $PROGRAM_NAME == "./tf"
    app = "terraform"
elsif $PROGRAM_NAME == "./kubectl"
    app = "kubectl"
elsif $PROGRAM_NAME == "./helm"
    app = "helm"
elsif $PROGRAM_NAME == "./tutor"
    app = "tutor"
elsif $PROGRAM_NAME == "./grove"
    app = "grove"
elsif $PROGRAM_NAME == "./shell"
    app = "shell"
elsif $PROGRAM_NAME == "./velero"
    app = "velero"
else
    raise "Unexpected program name #{$PROGRAM_NAME}\n"\
          "This should be run as ./tf or ./kubectl or ./helm or ./tutor or ./grove or ./velero from the 'control' folder of a project that was "\
          "forked from https://gitlab.com/opencraft/dev/grove-template/ . "\
          "It should look like:   cd my-cluster/control/ && ./tf init"
end

cluster_vars_file = "../cluster.yml"
if !(File.file?(cluster_vars_file))
    raise "Can't find #{cluster_vars_file}. \n"\
          "This should be run as ./tf or ./kubectl from the 'control' folder of a project that was "\
          "forked from https://gitlab.com/opencraft/dev/grove-template/ . "\
          "It should look like:   cd my-cluster/control/ && ./tf init"
end

private_vars_file = "../private.yml"
if !(File.file?(private_vars_file))
    raise "To use this script locally, you need to set up some secrets in ../private.yml"
end

########################################################################################################################
# Load and compute required environment variables, found in various YAML files
########################################################################################################################

cluster_vars = YAML.load(File.read(cluster_vars_file))["variables"]
private_vars = YAML.load(File.read(private_vars_file))["variables"]
env_vars = cluster_vars.merge(private_vars)
env_vars = env_vars.transform_values {|v| v.to_s }  # Cast to string since YAML may have given us some integer values

provider = cluster_vars["TF_VAR_cluster_provider"]
tf_root = "../grove/provider-#{provider}"

tf_main = "#{tf_root}/main.tf"
if !(File.file?(tf_main))
    raise "Cannot find any Terraform file #{tf_main}. Is TF_VAR_cluster_provider (#{provider}) in cluster.yml valid? Try 'aws'."
end


env_vars["TF_STATE_NAME"] = "tf-state-#{provider}"

# Set some derived values
required_local_vars = %w[GITLAB_PROJECT_NUMERIC_ID GITLAB_USERNAME GITLAB_PASSWORD]
required_local_vars.each do | local_var |
    unless env_vars[local_var]
        raise "You need to set #{local_var} in private.yml's variables section."
    end
end

env_vars["TF_VAR_gitlab_project_id"] = env_vars["GITLAB_PROJECT_NUMERIC_ID"]
env_vars["TF_USERNAME"] = env_vars["GITLAB_USERNAME"]
env_vars["TF_PASSWORD"] = env_vars["GITLAB_PASSWORD"]
env_vars["GITLAB_TOKEN"] = env_vars["GITLAB_PASSWORD"]
env_vars["TF_ROOT"] = "/workspace/grove/provider-#{provider}"

# Configuration required to connect to the GitLab Terraform state backend:
env_vars["TF_ADDRESS"] = "https://gitlab.com/api/v4/projects/#{env_vars["GITLAB_PROJECT_NUMERIC_ID"]}/terraform/state/#{env_vars["TF_STATE_NAME"]}"

########################################################################################################################
# Helper functions
########################################################################################################################

def check_docker_is_running()
    @docker_checked ||= false
    if @docker_checked
        return
    end
    # Helper function to check if Docker is running.
    system('docker stats --no-stream > /dev/null')
    if $? != 0
        raise "Docker is not running. We run Terraform and kubectl and aws-cli using Docker, for consistency and so "\
              "you don't have to install them on your system. Please start docker."
    end
    @docker_checked = true
end

def get_tools_image_tag()
    # Helper function to pull or build a docker container with aws-cli, kubectl, and tutor
    image_vars = YAML.load(File.read("../grove/tools-container/ci_vars.yml"))["variables"]
    registry = "registry.gitlab.com/opencraft/dev/grove/"
    name = image_vars["TOOLS_CONTAINER_IMAGE_NAME"]
    tag = image_vars["TOOLS_CONTAINER_IMAGE_VERSION"]
    image = "#{registry}#{name}:#{tag}"
    # Check if the image exists:
    unless system("docker image inspect #{image} > /dev/null 2>&1")
        system("docker pull #{image}")

        unless system("docker image inspect #{image} > /dev/null 2>&1")
            # Build the image
            print "\nBuilding Docker tools container image\n\n"
            system(image_vars, "docker build \
                                --build-arg KUBECTL_VERSION \
                                --build-arg GITLAB_TERRAFORM_IMAGES_VERSION \
                                --build-arg TERRAFORM_VERSION \
                                --build-arg HELM_VERSION \
                                --build-arg VELERO_VERSION \
                                --no-cache \
                                --platform=linux/amd64 \
                                -t #{image} \
                                ../grove/tools-container")
            if $? != 0
                raise "Failed to build #{image}"
            end
        end
    end
    return image
end

def get_kubeconfig_path()
    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate with kubernetes
    kubeconfig_file = "../kubeconfig-private.yml"
    if !(File.file?(kubeconfig_file))
        print "Generating required #{kubeconfig_file} using Terraform...\n"
        if ENV["NO_COMMAND_PRINT"] != "true"
            ENV["NO_COMMAND_PRINT"] = "true"
            delete_print_var = true
        end
        result = system("./tf output -raw kubeconfig | grep -v \"\\[DEBUG\\]\" > #{kubeconfig_file}")
        if delete_print_var
            ENV.delete("NO_COMMAND_PRINT")
        end
        if $? != 0
            # This can fail if Terraform isn't initialized, in which case the kubeconfig file
            # Would just contain a Terraform error message.
            File.delete(kubeconfig_file) if File.exist?(kubeconfig_file)
            raise "Failed to create kubeconfig file. You probably need to run './tf init' first."
        end
        if !(File.file?(kubeconfig_file))
            raise "Failed to generate kubeconfig"
        end
    end
    return kubeconfig_file
end

def run_tool(tool_name, args, env_vars, mount_workspace, extra_mounts = [], extra_args = [])
    # Run a tool using our docker tools container (which contains AWS, kubectl, Tutor, etc.)

    # Check if Docker is running.
    check_docker_is_running()
    # Build the image we need, if necessary
    tools_tag = get_tools_image_tag()  # TODO: just host this on Docker Hub?

    # set CI_PROJECT_DIR for local use
    env_vars["CI_PROJECT_DIR"] = "/workspace"

    # We specify --platform linux/amd64 to avoid a warning on ARM Mac hosts. Running the container as ARM is not supported.
    docker_args = ["run", "-i", "-t", "--rm", "--platform", "linux/amd64", "--workdir", "/workspace"]
    if mount_workspace
        docker_args.push("--mount", "type=bind,source=#{mount_workspace},destination=/workspace")
    end
    extra_mounts.each do |host_path, container_path|
        docker_args.push("--mount", "type=bind,source=#{File.expand_path(host_path)},destination=#{container_path}")
    end
    # Tell docker to pass through selected environment variables into the container:
    env_vars.each do |key, value|
        docker_args.push("-e", key)
    end
    docker_args.push(*extra_args, tools_tag, tool_name, *args)

    if ENV["NO_COMMAND_PRINT"] != "true"
        # Print the command we're running, for debugging:
        system(env_vars, "echo", "docker", *docker_args)
    end

    system(env_vars, "docker", *docker_args)
end

def confirm_tf_destroy_action(terraform_wrapper, env_vars)
    # Show the user the destroy plan and confirms with user if they really want to destroy

    def bold(text)
        # Render text in bold
        "\e[1m#{text}\e[22m" 
    end

    # Check if NO_COMMAND_PRINT env variabe is set. If no then set it.
    # This is required so that the command does not clutter the plan output.
    if ENV["NO_COMMAND_PRINT"] != "true"
        ENV["NO_COMMAND_PRINT"] = "true"
        delete_print_var = true
    end

    # Run terraform plan with -destroy flag
    run_tool(
        "bash", [terraform_wrapper] + ["plan", "-destroy"],
        env_vars,
        mount_workspace=File.expand_path("../"),
    )

    # Delete the plan created above, so that we don't accidently apply it through ./tf apply
    run_tool(
        "bash", ["-l", "-c", "rm "+env_vars["TF_ROOT"]+"/plan.cache"],
        env_vars,
        mount_workspace=File.expand_path("../"),
    )

    # Unset the NO_COMMAND_PRINT env variable
    if delete_print_var
        ENV.delete("NO_COMMAND_PRINT")
    end

    # Prompt the user to confirm destroy
    puts bold("\nDo you really want to destroy all resources?")
    puts "Only 'yes' will be accepted to confirm."
    print bold("Enter a value: ")
    confirm_run = STDIN.gets.chomp.strip
    if confirm_run == "yes"
        return true
    end
    puts "Destroy cancelled."
    return false
end

########################################################################################################################
# Terraform wrapper
########################################################################################################################
if app == "terraform"
    terraform_wrapper = "/workspace/grove/tools-container/scripts/tf.sh"
    action_confirmed = true

    # Confirm with user before destroy
    if ARGV.include? 'destroy' or ARGV.include? '-destroy'
        action_confirmed = confirm_tf_destroy_action(terraform_wrapper, env_vars)
    end

    if action_confirmed
        run_tool(
            "bash", [terraform_wrapper] + ARGV,
            env_vars,
            mount_workspace=File.expand_path("../"),
        )
    end

########################################################################################################################
# kubectl wrapper
########################################################################################################################
elsif app == "kubectl"
    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate
    kubeconfig_file = get_kubeconfig_path()

    # Expose the port used by kubectl proxy for dashboard etc.
    extra_args = []
    if ARGV.include? 'port-forward'
      extra_args = ["--publish", "127.0.0.1:8001:8001"]
    end

    run_tool(
        "kubectl", ARGV,
        env_vars,
        mount_workspace=File.expand_path("../"),
        extra_mounts=[
            [kubeconfig_file, "/home/grove/.kube/config"]
        ],
        extra_args = extra_args,
    )

########################################################################################################################
# Velero wrapper
########################################################################################################################
elsif app == "velero"
    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate
    kubeconfig_file = get_kubeconfig_path()

    run_tool(
        "velero", ARGV,
        env_vars,
        mount_workspace=File.expand_path("../"),
        extra_mounts=[
            [kubeconfig_file, "/home/grove/.kube/config"]
        ],
    )

########################################################################################################################
# helm wrapper
########################################################################################################################
elsif app == "helm"
    # Generate the kubeconfig-private.yml file if it's not present, so we can authenticate
    kubeconfig_file = get_kubeconfig_path()

    run_tool(
        "helm", ARGV,
        env_vars,
        mount_workspace=File.expand_path("../"),
        extra_mounts=[
            [kubeconfig_file, "/home/grove/.kube/config"]
        ],
    )

########################################################################################################################
# tutor wrapper
########################################################################################################################
elsif app == "tutor"
    kubeconfig_file = get_kubeconfig_path()
    instance_name = ARGV[0]
    env_vars["INSTANCE_NAME"] = instance_name

    # load grove config for the instance
    grove_config = YAML.load(File.read("../instances/#{instance_name}/grove.yml"))

    run_tool(
        "grove",["tutor", "exec", instance_name, '--format', 'json', ARGV.drop(1).join(" ").to_json] ,  # Pass through all arguments except the instance_name
        env_vars,
        mount_workspace=File.expand_path("../"),
        extra_mounts=[
            [kubeconfig_file, "/home/grove/.kube/config"]
        ],
    )

########################################################################################################################
# grove cli
########################################################################################################################
elsif app == "grove"
    kubeconfig_file = get_kubeconfig_path()
    extra_args = []
    if ARGV.include? "run-pipeline"
        # For run-pipeline, we need to make "docker" commands work within the tools container.
        #
        # Note: on MacOS, even if you are using ~/.docker/run/docker.sock, you must specify /var/run/docker.sock
        # as the mount source, because Docker for Mac detects that exact mount point and re-routes the mount to be
        # served from the Docker Linux VM, not your MacOS host. If you get "permission denied" when trying to open the
        # socket within the container as a non-root user like "grove", you can fix it with:
        #   docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock docker:dind chmod go+w /var/run/docker.sock
        # Using the "osxfs (legacy)" file sharing mode may be required.
        extra_args = ['--privileged', "--volume", "/var/run/docker.sock:/var/run/docker.sock"]
    end

    run_tool(
        "grove", ARGV,
        env_vars,
        mount_workspace=File.expand_path('../'),
        extra_mounts=[
            [kubeconfig_file, "/home/grove/.kube/config"]
        ],
        extra_args=extra_args
    )

########################################################################################################################
# grove tools container shell
########################################################################################################################
elsif app == "shell"
    kubeconfig_file = get_kubeconfig_path()
    run_tool(
        "bash", [],
        env_vars,
        mount_workspace=File.expand_path('../'),
        extra_mounts=[
            [kubeconfig_file, "/home/grove/.kube/config"]
        ],
    )
else
    raise "Unknown app"
end
