output "runner_role_arn" {
  value = module.gitlab-runner.runner_role_arn
}

output "runner_sg_id" {
  value = module.gitlab-runner.runner_sg_id
}

output "runner_eip" {
  value = module.gitlab-runner.runner_eip
}

output "runner_agent_role_arn" {
  value = module.gitlab-runner.runner_agent_role_arn
}

output "runner_agent_sg_id" {
  value = module.gitlab-runner.runner_agent_sg_id
}

output "runner_vpc_id" {
  value = module.vpc.vpc_id
}

output "runner_subnet_id" {
  value = element(module.vpc.public_subnets, 0)
}

output "runner_public_subnets" {
  value = module.vpc.public_subnets
}

output "runner_private_subnets" {
  value = module.vpc.private_subnets
}
