# aws-runners

Setting up GitLab runners on AWS for Grove.

## Variables

- `aws_region`: AWS region.
- `environment`: A name that identifies the environment, will used as prefix and for tagging.
- `runner_name`: Name of the runner, will be used in the runner `config.toml`
- `runners_concurrent`: Concurrent value for the runners, will be used in the runner `config.toml`.
- `runners_idle_count`: Idle count of the runners, will be used in the runner `config.toml`.
- `instance_type`: Instance type used for the GitLab runner.
- `gitlab_url`: URL of the gitlab instance to connect to.
- `registration_token`: GitLab token used for registering CI runners.
- `max_timeout`: The maximum amount of seconds that the runner can spend on a single run.

## Outputs

https://registry.terraform.io/modules/npalm/gitlab-runner/aws/latest#outputs
