terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.14"
    }
  }
}

# Set the access key and secret key via environment variables:
# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
# Note that if you change the region, you have to destroy the runner first.
provider "aws" {
  region = var.aws_region
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc-grove-${var.environment}-gitlab-runners"
  cidr = "10.0.0.0/16"

  azs             = [data.aws_availability_zones.available.names[0]]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
    Environment = "grove-${var.environment}-gitlab-runners"
  }
}

module "gitlab-runner" {
  source  = "npalm/gitlab-runner/aws"
  version = "5.9.0"

  aws_region  = var.aws_region
  environment = "grove-${var.environment}-gitlab-runners"

  vpc_id                   = module.vpc.vpc_id
  subnet_ids_gitlab_runner = module.vpc.private_subnets
  subnet_id_runners        = element(module.vpc.private_subnets, 0)

  instance_type = var.instance_type

  # See https://github.com/npalm/terraform-aws-gitlab-runner/issues/680#issuecomment-1414372838
  # The below lines should be removed once the above is fixed.
  docker_machine_version = "0.16.2-gitlab.15"
  docker_machine_options = [
    "engine-install-url='https://releases.rancher.com/install-docker/20.10.sh'"
  ]

  # Enable accessing the runner through AWS SSM
  enable_runner_ssm_access = true

  runners_name       = var.runner_name
  runners_gitlab_url = var.gitlab_url
  runners_concurrent = var.runners_concurrent
  runners_idle_count = var.runners_idle_count

  # Enable docker-in-docker integration
  runners_add_dind_volumes = true

  # By setting a spot price bid price the runner agent will be created via a spot request.
  # Be aware that spot instances can be stopped by AWS.
  runner_instance_spot_price = "on-demand-price"

  # To read more about the autoscaling configuration, please visit
  # https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnersmachine-section
  runners_machine_autoscaling = [
    {
      periods    = ["\"* * * * * * *\""]
      idle_count = 0
      idle_time  = 60
      timezone   = "UTC"
    }
  ]

  runner_ami_filter = {
    name = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-20211129*"]
  }

  gitlab_runner_registration_config = {
    registration_token = var.registration_token
    tag_list           = "grove-docker-runner"
    description        = "Docker based GitLab runner for Grove"
    locked_to_project  = "true"
    run_untagged       = "true"
    maximum_timeout    = var.max_timeout
  }

  gitlab_runner_security_group_description  = "Security group containing Grove gitlab-runner agent instances"
  docker_machine_security_group_description = "Security group containing Grove docker-machine agent instances"
}

