## Description

_Describe what this pull request changes, and why. Include implications for people using this change._

## Supporting information

_Link to other information about the change, such as GitLab issues, GitHub issues, forum discussions. Be sure to check they are publicly readable, or if not, repeat the information here._

## Testing instructions

Steps to test the changes:

1. _TBD_

## Dependencies

_List the dependencies required for this change, if any. Do not forget to link [grove-template](https://gitlab.com/opencraft/dev/grove-template) merge request here if that's affected by this change._

## Screenshots

_If applicable, add screenshots to help explain your feature._

## Checklist

If any of the items below is not applicable, do **not** remove them, but put a check in it.

- [ ] All providers include the new feature/change
- [ ] All affected providers can provision new clusters
- [ ] Unit tests are added/updated
- [ ] Documentation is added/updated
- [ ] The `TOOLS_CONTAINER_IMAGE_VERSION` in [ci_vars.yml](https://gitlab.com/opencraft/dev/grove/-/blob/main/tools-container/ci_vars.yml) is updated
- [ ] The [grove-template repository](https://gitlab.com/opencraft/dev/grove-template) is updated

## Additional context

_Add any other context about the merge request here._
