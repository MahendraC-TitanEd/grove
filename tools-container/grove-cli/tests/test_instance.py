from pathlib import Path
from unittest.mock import MagicMock, patch

import pytest

from grove.const import INSTANCES_DIR
from grove.exceptions import GroveError
from grove.instance import Instance
from grove.utils import slugify_instance_name

from .utils import TEST_INSTANCE_NAME_A, GroveTestCase


class InstanceTestCase(GroveTestCase):
    """
    Test grove.instance.Instance
    """

    def test_instance_init(self):
        """
        Test if Instance initializes correctly.
        """
        test_instance_name = TEST_INSTANCE_NAME_A
        instance = Instance(test_instance_name)

        assert slugify_instance_name(test_instance_name) == instance.instance_name
        assert INSTANCES_DIR / instance.instance_name == instance.root_dir
        assert INSTANCES_DIR / instance.instance_name / "env" == instance.env_dir
        assert INSTANCES_DIR / instance.instance_name / "grove.yml" == instance.grove_config_file
        assert INSTANCES_DIR / instance.instance_name / "config.yml" == instance.tutor_config_file

    def test_instance_create(self):
        instance = Instance(TEST_INSTANCE_NAME_A)

        assert instance.exists() is False

        instance.create()

        assert instance.exists()

        assert Path.exists(instance.root_dir)
        assert Path.exists(instance.grove_config_file)
        assert Path.exists(instance.tutor_config_file)

        instance.load_instance_configs()

        assert "COMPREHENSIVE_THEME_SOURCE_REPO" in instance.grove_config["THEMES"][0]
        assert "PLUGINS" in instance.tutor_config.keys()

        instance.delete()
        assert not Path.exists(instance.root_dir)

    def test_instance_double_create_error(self):
        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.create()

        with pytest.raises(GroveError) as exc:
            instance.create()
            assert str(exc) == f"Instance {TEST_INSTANCE_NAME_A} already exists. Can't create."

        assert instance.exists() is True

    def test_instance_create_error_invalid_name(self):
        instance = Instance("monitoring")

        with pytest.raises(GroveError) as exc:
            instance.create()
            assert str(exc) == 'Instance name "monitoring" is not valid. Can\'t create.'

        assert instance.exists() is False

    def test_instance_prepare(self):
        test_instance_name = TEST_INSTANCE_NAME_A
        instance = Instance(test_instance_name)
        instance.prepare(
            {
                "grove": {
                    "THEMES": [
                        {
                            "COMPREHENSIVE_THEME_SOURCE_REPO": "repo",
                            "SIMPLE_THEME_SCSS_OVERRIDES": {
                                "bg-color": "#000000",
                            },
                        }
                    ],
                },
                "tutor": {
                    "MFE_HOST": "app.test.com",
                },
            }
        )

        self.mock_tf.is_infrastructure_ready.assert_called_with(instance.instance_name)
        self.mock_tf.get_env.assert_called_with(instance)

        themes_config = instance.grove_config["THEMES"][0]
        assert "COMPREHENSIVE_THEME_SOURCE_REPO" in themes_config
        assert "SIMPLE_THEME_SCSS_OVERRIDES" in themes_config
        assert len(themes_config["SIMPLE_THEME_SCSS_OVERRIDES"]) == 1
        assert themes_config["SIMPLE_THEME_SCSS_OVERRIDES"]["bg-color"] == "#000000"

        assert "ID" in instance.tutor_config.keys()
        assert "JWT_RSA_PRIVATE_KEY" in instance.tutor_config.keys()
        assert instance.tutor_config["MFE_HOST"] == "app.test.com"

        instance.delete()
        assert not Path.exists(instance.root_dir)

    @patch("grove.instance.os")
    def test_instance_name_validity(self, mock_os):
        scenarios = (
            # Instance names
            ("testinstance", "testinstance", [], True),
            ("test-instance", "test-instance", [], True),
            ("test_instance", "test-instance", [], True),
            ("test instance", "test-instance", [], True),
            # Instance already registered
            ("testinstance", "testinstance", ["testinstance"], False),
            # Restricted instance names (because of namespace name)
            ("default", "default", [], False),
            ("gitlab-kubernetes-agent", "gitlab-kubernetes-agent", [], False),
            ("kube-node-lease", "kube-node-lease", [], False),
            ("kube-public", "kube-public", [], False),
            ("kube-system", "kube-system", [], False),
            ("monitoring", "monitoring", [], False),
            ("openfaas", "openfaas", [], False),
            ("openfaas-fn", "openfaas-fn", [], False),
            # Illegal instance name
            ("1testinstance", "1testinstance", [], False),
            # Instance name length issues
            ("a", "a", [], False),
            ("ab", "ab", [], False),
            (
                "this instance name is soooooooooooooooooooooo long",
                "this-instance-name-is-soooooooooooooooooooooo-long",
                [],
                False,
            ),
        )

        for scenario in scenarios:
            instance_name, instance_slug, registered, expected = scenario

            mock_os.listdir.return_value = registered
            mock_os.is_dir.return_value = True

            instance = Instance(instance_name)

            assert instance.instance_name == instance_slug, f"Scenario {scenario}"
            assert instance.is_name_valid() == expected, f"Scenario {scenario}"

    def test_instance_generate_common_scss_overrides(self):
        test_instance_name = TEST_INSTANCE_NAME_A
        instance = Instance(test_instance_name)

        scss_dir = Path("some/path")
        autogenerated_notice = "test notice"
        theme = {
            "SIMPLE_THEME_SCSS_OVERRIDES": {
                "bg-color": "#000000",
                "links-color": "#FFFFFF",
            },
        }

        mock_lms_overrides_file_path = MagicMock()
        with patch("grove.instance.Path", return_value=mock_lms_overrides_file_path):
            instance.generate_common_scss_overrides(theme, scss_dir, autogenerated_notice)

            mock_lms_overrides_file_path.write_text.assert_called_with(
                f"{autogenerated_notice}\n$bg-color: #000000;\n\n$links-color: #FFFFFF;\n", encoding="utf-8"
            )
