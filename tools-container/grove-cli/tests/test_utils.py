import json
import os
from unittest.case import TestCase

from grove.const import CI_REGISTRY_IMAGE
from grove.utils import (
    RepoRelease,
    instance_customization_from_trigger_payload,
    slugify_instance_name,
)


def test_slugify_instance_name():
    normal = "new-instance"
    assert slugify_instance_name(normal) == normal

    with_leading_space = " test"
    assert slugify_instance_name(with_leading_space) == "test"

    with_trailing_space = "test   "
    assert slugify_instance_name(with_trailing_space) == "test"

    with_spaces_on_both_ends = "   hello "
    assert slugify_instance_name(with_spaces_on_both_ends) == "hello"

    space_in_between = "hello world"
    assert slugify_instance_name(space_in_between) == "hello-world"

    underscore_in_between = "hello_world"
    assert slugify_instance_name(underscore_in_between) == "hello-world"


def test_instance_customization_from_trigger_payload():
    TRIGGER_PAYLOAD = "/tmp/trigger_payload"
    with open(TRIGGER_PAYLOAD, "w") as file:
        json.dump(
            {
                "TUTOR_KEY1": "VAL1",
                "TUTOR_KEY2": "VAL2",
                "GROVE_KEY1": "VAL1",
                "GROVE_KEY2": "VAL2",
            },
            file,
        )
    os.environ["TRIGGER_PAYLOAD"] = TRIGGER_PAYLOAD

    customization = instance_customization_from_trigger_payload()

    assert customization["grove"]["KEY1"] == "VAL1"
    assert customization["grove"]["KEY2"] == "VAL2"
    assert customization["tutor"]["KEY1"] == "VAL1"
    assert customization["tutor"]["KEY2"] == "VAL2"


class TestRepoRelease(TestCase):
    def setUp(self) -> None:
        https_repo = "https://github.com/open-craft/edx-platform.git"
        http_repo = "http://github.com/open-craft/edx-platform.git"
        git_repo = "git@github.com:open-craft/edx-platform.git"
        release = "opencraft-release/maple.1"

        self.http_rr = RepoRelease(http_repo, release)
        self.https_rr = RepoRelease(https_repo, release)
        self.git_rr = RepoRelease(git_repo, release)

    def test_equals(self):
        self.assertTrue(self.http_rr.equals(self.https_rr))
        self.assertTrue(self.http_rr.equals(self.git_rr))
        self.assertTrue(self.https_rr.equals(self.http_rr))
        self.assertTrue(self.https_rr.equals(self.git_rr))
        self.assertTrue(self.git_rr.equals(self.http_rr))
        self.assertTrue(self.git_rr.equals(self.https_rr))

    def test_not_equals(self):
        # different repo path should not match
        repo = "https://github.com/edx/edx-platform.git"
        release = "opencraft-release/maple.1"

        repo_release = RepoRelease(repo, release)

        self.assertFalse(self.http_rr.equals(repo_release))
        self.assertFalse(self.https_rr.equals(repo_release))
        self.assertFalse(self.git_rr.equals(repo_release))

        # different hostname should not match
        repo = "https://gitlab.com/open-craft/edx-platform.git"
        release = "opencraft-release/maple.1"

        repo_release = RepoRelease(repo, release)

        self.assertFalse(self.http_rr.equals(repo_release))
        self.assertFalse(self.https_rr.equals(repo_release))
        self.assertFalse(self.git_rr.equals(repo_release))

        # different release should not match
        repo = "https://github.com/open-craft/edx-platform.git"
        release = "opencraft-release/maple.2"

        repo_release = RepoRelease(repo, release)

        self.assertFalse(self.http_rr.equals(repo_release))
        self.assertFalse(self.https_rr.equals(repo_release))
        self.assertFalse(self.git_rr.equals(repo_release))

    def test_image_tag(self):
        image_tag = f"{CI_REGISTRY_IMAGE}/base_images/openedx:github_com_open_craft_edx_platform_opencraft_release_maple_1"  # noqa: E501
        self.assertEqual(self.https_rr.get_tagged_base_image(), image_tag)
        self.assertEqual(self.http_rr.get_tagged_base_image(), image_tag)
        self.assertEqual(self.git_rr.get_tagged_base_image(), image_tag)
