import hashlib
import json
import os
from urllib.parse import parse_qs, quote, urlparse

import typer

import grove
from grove.const import SCRIPTS_DIR
from grove.exceptions import GroveError
from grove.type_defs import TutorEnv
from grove.utils import execute

TF_GROVE_DEBUG = os.environ.get("TF_GROVE_DEBUG", "false") == "true"
TF_SCRIPT = str(SCRIPTS_DIR / "tf.sh")
TF_ALIAS = f"bash {TF_SCRIPT}"

_tf_output_cache: dict | None = None


def get_terraform_output(key: str) -> dict:
    """
    Get a Terraform state output.
    """
    global _tf_output_cache
    if not _tf_output_cache:
        typer.echo("Loading outputs from Terraform")
        try:
            # It's faster to load them via one call than many separate calls.
            # And we don't run Terraform from/during python code so we can cache this.
            result = execute(f"{TF_ALIAS} output -json", hide_output=TF_GROVE_DEBUG, capture_output=True)
        except GroveError as e:
            typer.echo("Unable to get outputs from terraform")
            typer.echo(e)
            raise e
        _tf_output_cache = json.loads(result)
    try:
        return _tf_output_cache[key]["value"]
    except KeyError:
        return {}


def is_infrastructure_ready(instance_name: str) -> bool:
    """
    Determine if infrastructure ready for an instance.
    """
    provider = os.environ.get("TF_VAR_cluster_provider")
    mongodb_state = get_terraform_output("mongodb")
    return provider == "minikube" or mongodb_state and mongodb_state.get("instances", {}).get(instance_name, False)


def _cloudfront_state(instance: "grove.instance.Instance") -> dict:
    """
    Fetch any Cloudfront configuration from the terraform state.
    """
    provider = instance.cluster_config["variables"]["TF_VAR_cluster_provider"]
    cloudfront_env = {}

    if provider == "aws":
        cloudfront_lms_state = get_terraform_output("cloudfront_lms_domains")
        cloudfront_cms_state = get_terraform_output("cloudfront_cms_domains")

        instance_name = instance.instance_name

        if instance_name in cloudfront_lms_state:
            cloudfront_env["TUTOR_GROVE_LMS_STATIC_URL"] = f"https://{cloudfront_lms_state[instance_name]}/static/"

        if instance_name in cloudfront_cms_state:
            cloudfront_env["TUTOR_GROVE_CMS_STATIC_URL"] = (f"https://{cloudfront_cms_state[instance_name]}"
                                                            "/static/studio/")

    return cloudfront_env


def _elasticsearch_state(instance: "grove.instance.Instance") -> dict:
    """
    Fetch any Elasticsearch-related configuration from the terraform state.
    """
    elasticsearch_state = get_terraform_output("elasticsearch")

    if not elasticsearch_state:
        return {}

    if not instance.tutor_config.get("GROVE_ENABLE_SHARED_ELASTICSEARCH"):
        return {}

    es_instance = elasticsearch_state[0]["instances"][instance.instance_name]

    username = es_instance["username"]
    password = es_instance["password"]
    prefix = es_instance["index_prefix"] + "-"

    k8s_host = "elasticsearch-master.elasticsearch.svc.cluster.local"

    ca_cert_pem = elasticsearch_state[0]["ca_cert_pem"].replace("\n", "\\n")

    return {
        "TUTOR_RUN_ELASTICSEARCH": "false",
        "TUTOR_ELASTICSEARCH_INDEX_PREFIX": prefix,
        "TUTOR_ELASTICSEARCH_HOST": k8s_host,
        "TUTOR_ELASTICSEARCH_HTTP_AUTH": f"{username}:{password}",
        "TUTOR_ELASTICSEARCH_SCHEME": "https",
        "TUTOR_ELASTICSEARCH_CA_CERT_PEM": ca_cert_pem,
    }


def _mongodb_state(instance_name: str) -> dict:
    """
    Fetch any MongoDB-related configuration from the terraform state.
    """
    mongodb_state = get_terraform_output("mongodb")

    if not mongodb_state:
        return {}

    mongodb_instance = mongodb_state["instances"][instance_name]
    mongodb_url = urlparse(mongodb_state["connection_string"])

    mongodb_host = (
        f"{mongodb_url.scheme}://"
        f"{mongodb_instance['username']}:{quote(mongodb_instance['password'])}@{mongodb_url.hostname}"
    )
    mongodb_params = parse_qs(mongodb_url.query)

    use_ssl = mongodb_params.get("ssl", mongodb_params.get("tls", ["true"]))[0]

    return {
        "TUTOR_FORUM_MONGODB_DATABASE": mongodb_instance["forum_database"],
        "TUTOR_MONGODB_HOST": mongodb_host,
        "TUTOR_MONGODB_DATABASE": mongodb_instance["database"],
        # MongoDB setup uses basic auth, they don't need to be
        # duplicated otherwise loading DOC_STORE_CONFIG will fail.
        "TUTOR_MONGODB_USERNAME": "",
        "TUTOR_MONGODB_PASSWORD": "",
        "TUTOR_MONGODB_USE_SSL": use_ssl,
        "TUTOR_MONGODB_AUTH_SOURCE": mongodb_params.get("authSource", ["admin"])[0],
        "TUTOR_MONGODB_AUTH_MECHANISM": "SCRAM-SHA-1",
        "TUTOR_MONGODB_REPLICA_SET": mongodb_params.get("replicaSet", [""])[0],
    }


def _s3_static_files_state(instance: "grove.instance.Instance") -> dict:
    bucket_state = get_terraform_output("edxapp_bucket")

    if not instance.tutor_config.get("GROVE_ENABLE_S3_STATIC_FILES"):
        return {}

    if not bucket_state or instance.instance_name not in bucket_state:
        return {}

    provider = os.environ.get("TF_VAR_cluster_provider")
    if provider == "aws":
        instance = bucket_state[instance.instance_name]["s3_bucket"]
    elif provider == "digitalocean":
        instance = bucket_state[instance.instance_name]["spaces_bucket"]
    else:
        return {}

    bucket_domain = instance["bucket_domain_name"]

    return {
        "TUTOR_GROVE_LMS_STATIC_URL": f"https://{bucket_domain}/static/",
        "TUTOR_GROVE_CMS_STATIC_URL": f"https://{bucket_domain}/static/studio/",
    }


def _s3_state(instance_name: str) -> dict:
    """
    Fetch any S3-related configuration from the terraform state.
    """
    provider = os.environ.get("TF_VAR_cluster_provider")
    bucket_state = get_terraform_output("edxapp_bucket")

    if not bucket_state or instance_name not in bucket_state:
        return {}

    instance = bucket_state[instance_name]

    if provider == "aws":
        access_key = instance["s3_user_access_key"]["id"]
        secret_access_key = instance["s3_user_access_key"]["secret"]
        bucket_host = ""
        bucket_name = instance["s3_bucket"]["id"]
        bucket_region = instance["s3_bucket"]["region"]
    elif provider == "digitalocean":
        access_key = os.environ.get("SPACES_ACCESS_KEY_ID")
        secret_access_key = os.environ.get("SPACES_SECRET_ACCESS_KEY")
        bucket_region = instance["spaces_bucket"]["region"]
        # Unlike s3, we can't use the bucket domain to run any S3 query.
        # We have to use the region,
        # https://docs.digitalocean.com/products/spaces/reference/s3-sdk-examples/#configure-a-client
        bucket_host = f"{bucket_region}.digitaloceanspaces.com"
        bucket_name = instance["spaces_bucket"]["name"]

    return {
        "TUTOR_OPENEDX_AWS_ACCESS_KEY": access_key,
        "TUTOR_OPENEDX_AWS_SECRET_ACCESS_KEY": secret_access_key,
        "TUTOR_S3_HOST": bucket_host,
        "TUTOR_S3_STORAGE_BUCKET": bucket_name,
        "TUTOR_S3_REGION": bucket_region,
    }


def _mysql_state(instance_name: str, tutor_config: TutorEnv) -> dict:
    """
    Fetch any MySQL-related configuration from the terraform state.
    """
    instance_prefix = instance_name.replace("-", "_")

    mysql_state = get_terraform_output("mysql")
    if not mysql_state:
        return {}

    # set MySQL application user. Ensure that the username is not larger than 32 characters
    mysql_username = tutor_config.get("TUTOR_OPENEDX_MYSQL_USERNAME", f"{instance_prefix}_openedx")[:32]
    # set MySQL databases names. Database names can be at most 64 character long
    mysql_database = tutor_config.get("TUTOR_OPENEDX_MYSQL_DATABASE", f"{instance_prefix}_openedx")[:64]

    # Derive OPENEDX_MYSQL_PASSWORD in a deterministic manner so it is not regenerated
    key_derivation_pass = mysql_state["root_password"].encode("utf8")
    key_derivation_salt = mysql_state["root_username"].encode("utf8")
    mysql_password = hashlib.pbkdf2_hmac('sha256', key_derivation_pass, key_derivation_salt, 500_000).hex()[:32]

    return {
        "TUTOR_MYSQL_HOST": mysql_state["host"],
        "TUTOR_MYSQL_PORT": str(mysql_state["port"]),
        "TUTOR_MYSQL_ROOT_USERNAME": mysql_state["root_username"],
        "TUTOR_MYSQL_ROOT_PASSWORD": mysql_state["root_password"],
        "TUTOR_OPENEDX_MYSQL_USERNAME": mysql_username,
        "TUTOR_OPENEDX_MYSQL_PASSWORD": mysql_password,
        "TUTOR_OPENEDX_MYSQL_DATABASE": mysql_database,
    }


def get_env(instance: "grove.instance.Instance") -> TutorEnv:
    """
    Prepare env variables from terraform state for an instance.
    """
    env: TutorEnv = {}
    env.update(_cloudfront_state(instance))
    env.update(_mysql_state(instance.instance_name, instance.tutor_config))
    env.update(_mongodb_state(instance.instance_name))
    env.update(_elasticsearch_state(instance))

    env.update(_s3_state(instance.instance_name))
    env.update(_s3_static_files_state(instance))

    return env
