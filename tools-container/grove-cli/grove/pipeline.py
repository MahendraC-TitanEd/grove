import logging
import os
import re
from typing import Dict, Iterable, List

import requests
import typer
import yaml

from grove.const import GROVE_TEMPLATES_DIR
from grove.instance import Instance, load_all_instances, load_all_repo_releases
from grove.utils import RepoRelease

logger = logging.getLogger(__name__)


class Pipeline:
    """
    This class serves as a abstract Pipeline.
    """

    template = None

    def load_template(self) -> Dict:
        """
        Load template from $GROVE_TEMPLATES_DIR/pipelines directory.

        Returns:
            Dict - yml template loaded as Dict
        """
        with open(GROVE_TEMPLATES_DIR / "pipelines" / self.template) as f:
            config = yaml.safe_load(f)
        return config

    def build(self) -> Dict:
        """
        Pipeline builder will call this method to build a pipeline.

        Returns:
            Dict - a dict represending the pipeline.
        """
        return self.load_template()


class CommonPipeline(Pipeline):
    """
    A pipeline that exists for all kind of pipeline.
    """

    template = "common.yml"

    def __init__(self, has_kubectl_context: bool = True) -> None:
        """
        Builds the yaml to be included in all pipelines.

        Args:
            has_kubectl_context (bool) -> Turn on/off retrieving kubectl context
        """
        self.has_kubectl_context = has_kubectl_context

    def build(self):
        """
        Override default build method to remove kubectl contexts if needed.
        """
        pipeline = super().build()
        if not self.has_kubectl_context:
            del pipeline[".common"]["before_script"]
        return pipeline


class TerraformPipeline(Pipeline):
    """
    Terraform based infrastructure pipeline.
    """

    template = "terraform.yml"


class TerraformDestroyPipeline(Pipeline):
    """
    Terraform based infrastructure pipeline.
    """

    template = "terraform-destroy.yml"


class OpenedXPipeline(Pipeline):
    """
    Open edX pipeline. It contains jobs for -
        - build preparation
        - building instance image
        - deploy instance
    """

    template = "openedx.yml"

    def __init__(self, instances: List[Instance]) -> None:
        """
        Builds pipeline for only given instances.

        Args:
            instances (List[Instance])
        """
        self.instances: List[Instance] = instances

    def build(self) -> Dict:
        """
        Override default build method to add extra jobs.
        """
        pipeline = super().build()
        pipeline.update(self.build_instance_jobs())
        return pipeline

    def common_env(self, instance: Instance) -> Dict:
        """
        Get common env for each job - for a given instance.

        Args:
            instance (Instance)
        Returns:
            Dict - key value map of env variables
        """
        variables = instance.instance_image_envs()
        variables["INSTANCE_NAME"] = instance.instance_name
        variables["DOCKER_BUILDKIT"] = 1
        variables["EDX_PLATFORM_REPOSITORY"] = instance.tutor_config["EDX_PLATFORM_REPOSITORY"]
        variables["EDX_PLATFORM_VERSION"] = instance.tutor_config["EDX_PLATFORM_VERSION"]
        return variables

    def build_instance_jobs(self) -> Dict:
        """
        Builds jobs for all available instances.

        Returns:
            Dict - a dict representing all jobs
        """
        jobs = {}
        for instance in self.instances:
            # generate preparation jobs
            prepare_job_name = f"Prepare {instance.instance_name}"
            prepare_job = self.build_prepare_job(instance)
            jobs[prepare_job_name] = prepare_job

            # generate image build job for each image
            image_job_names = []
            for image_name, build_config in instance.instance_images():
                image_job_name = f"Build {instance.instance_name} - {image_name}"
                image_job_names.append(image_job_name)
                image_job = self.build_image_job(instance, image_name, build_config, [prepare_job_name])
                jobs[image_job_name] = image_job

            # generate instance deploy job
            deploy_job_name = f"Deploy {instance.instance_name}"
            deploy_job = self.build_deploy_job(instance, image_job_names)
            jobs[deploy_job_name] = deploy_job

        return jobs

    def build_prepare_job(self, instance: Instance) -> Dict:
        """
        Helper method to generate build preparation job
        """
        return {
            "extends": ".grove_prepare_common",
            "variables": self.common_env(instance),
        }

    def build_image_job(
        self,
        instance: Instance,
        image_name: str,
        build_config: dict,
        depends_on: List[str],
    ) -> Dict:
        """
        Helper method to generate image build job
        """
        env = self.common_env(instance)
        env["IMAGE_NAME"] = image_name
        docker_image_var = build_config["DOCKER_IMAGE_VAR"]
        env["IMAGE_LATEST_TAG"] = env[docker_image_var]
        return {
            "extends": ".grove_build_common",
            "variables": env,
            "needs": depends_on,
        }

    def build_deploy_job(self, instance, depends_on: List[str]):
        """
        Helper method to generate instance deployment job
        """
        return {
            "extends": ".grove_deploy_common",
            "variables": self.common_env(instance),
            "needs": depends_on,
        }


class OpenedXArchivePipeline(Pipeline):
    """
    Open edX archive pipeline. It contains jobs for
        - archive instance
    """

    template = "openedx_archive.yml"

    def __init__(self, instances: List[Instance]) -> None:
        """
        Builds pipeline for only given instances.

        Args:
            instances (List[Instance])
        """
        self.instances: Instance = instances

    def build(self) -> Dict:
        """
        Override default build method to add extra jobs.
        """
        pipeline = super().build()
        pipeline.update(self.build_instance_jobs())
        return pipeline

    def build_instance_jobs(self) -> Dict:
        """
        Builds jobs for all available instances.

        Returns:
            Dict - a dict representing all jobs
        """
        jobs = {}
        for instance in self.instances:
            job_name = f"Archive {instance.instance_name}"
            jobs[job_name] = self.build_archive_job(instance)

        return jobs

    @staticmethod
    def build_archive_job(instance: Instance) -> Dict:
        """
        Helper method to generate build archive job
        """
        return {
            "extends": ".grove_archive",
            "variables": {
                "INSTANCE_NAME": instance.instance_name,
            },
        }


class BaseImagePipeline(Pipeline):
    """
    Pipeline for building Base Images
    """

    template = "base_image.yml"

    def __init__(self, build_for: List[RepoRelease], only_available: bool = True) -> None:
        """
        Initialize with a key value map for available repository and release.

        Args:
            build_for (List[RepoRelease])
        """
        super().__init__()
        self.build_for: List[RepoRelease] = build_for
        self.only_available: bool = only_available

    def build(self) -> Dict:
        """
        Override build method to include base image build jobs.
        """
        pipeline = super().build()
        pipeline.update(self.build_base_image_jobs())
        return pipeline

    def build_base_image_jobs(self) -> Dict:
        """
        Generate image build jobs.
        """
        available_releases = load_all_repo_releases()
        typer.echo("Available releases -")
        typer.echo(available_releases)
        jobs = {}
        for repo_release in self.build_for:
            if repo_release.is_available(available_releases) or not self.only_available:
                typer.echo(f"Building job for {repo_release}")
                jobs[f"build {repo_release.release} from {repo_release.repo}"] = {
                    "extends": ".base_common",
                    "variables": {
                        "TUTOR_DOCKER_IMAGE_OPENEDX": repo_release.get_tagged_base_image(),
                        "EDX_PLATFORM_REPOSITORY": repo_release.repo,
                        "EDX_PLATFORM_VERSION": repo_release.release,
                    },
                }
            else:
                typer.echo(f"Repo release {repo_release} is not available.")
        return jobs


class DummyPipeline(Pipeline):
    """
    Pipeline for dummy job.
    """

    template = "dummy.yml"


class PipelineBuilder:
    """
    An utility class to merge & build multiple pipelines.
    """

    def __init__(self) -> None:
        self.pipelines: List[Pipeline] = []

    def add(self, pipeline: Pipeline):
        """
        Add a pipeline to merge.
        """
        self.pipelines.append(pipeline)

    def build(self) -> Dict:
        """
        Build and merge all pipelines in self.pipelines

        Returns:
            Dict - merged pipelines
        """

        # we need to tread stages specially
        stages = []
        result = {}

        for pipeline in self.pipelines:
            pbuild = pipeline.build()
            result.update(pbuild)

            # append each stage in stages array if that stage is not already present.
            # the order of stage respects the order of pipeline added to builder.
            for stage in pbuild.get("stages", []):
                if stage not in stages:
                    stages.append(stage)

        result["stages"] = stages
        return result


def filter_active_instances(instances: Iterable[Instance]) -> List[Instance]:
    """
    Filter out inactive instances.
    """
    return [i for i in instances if i.grove_config.get("ARCHIVED", False) is False]


def filter_archived_instances(instances: Iterable[Instance]) -> List[Instance]:
    """
    Filter out archived instances.
    """
    return [i for i in instances if i.grove_config.get("ARCHIVED", False) is True]


def parse_instances(text: str) -> List[Instance]:
    """
    Parse Instances from a formated text.
    Text Format - INSTANCE_NAME|DEPLOYMENT_ID,INSTANCE_NAME|DEPLOYMENT_ID...

    Args:
        text (str) - formatted text
    Returns:
        List[Instance]
    """
    instances = []

    for instance_info in text.strip().split(","):
        instance_name = instance_info.split("|")[0].strip()
        if instance_name:
            instance = Instance(instance_name)
            if instance.exists():
                instances.append(Instance(instance_name))

    return instances


def parse_repository_release(text: str, only_available: bool = True) -> List[RepoRelease]:
    """
    Parse Repo and Release from a formated text.
    Text Format - REPO_URL|RELEASE,REPO_URL|RELEASE...

    Args:
        text (str) - formatted text
    Returns:
        List[RepoRelease]
    """
    available_releases = load_all_repo_releases()
    repo_releases: List[RepoRelease] = []
    for release_info in text.strip().split(","):
        repo, release = release_info.split("|")
        repo = repo.strip()
        release = release.strip()
        if repo and release:
            repo_release = RepoRelease(repo, release)
            # check if already added
            if len([rr for rr in repo_releases if rr.equals(repo_release)]) == 0:
                # if the repo release used by any instance in the private repository
                if repo_release.is_available(available_releases) or not only_available:
                    repo_releases.append(repo_release)
    return repo_releases


def infrastructure_pipeline() -> Dict:
    """
    Generates infrastructure (terraform) pipeline.
    """
    pipeline_builder = PipelineBuilder()
    pipeline_builder.add(CommonPipeline(has_kubectl_context=False))
    pipeline_builder.add(TerraformPipeline())
    return pipeline_builder.build()


def infrastructure_destroy_pipeline() -> Dict:
    """
    Generates infrastructure (terraform) pipeline.
    """
    pipeline_builder = PipelineBuilder()
    pipeline_builder.add(CommonPipeline(has_kubectl_context=False))
    pipeline_builder.add(TerraformDestroyPipeline())
    return pipeline_builder.build()


def instance_create_pipeline(instances: List[Instance]) -> Dict:
    """
    Generates instance create pipeline.
    infrastructure pipeline + Open edX pipeline
    """
    if len(instances) == 0:
        typer.echo("Instances list is empty!")
        return DummyPipeline().build()

    pipeline_builder = PipelineBuilder()
    pipeline_builder.add(CommonPipeline())
    pipeline_builder.add(TerraformPipeline())
    pipeline_builder.add(OpenedXPipeline(instances))
    return pipeline_builder.build()


def instance_update_pipeline(instances: List[Instance]) -> Dict:
    """
    Generates instance update pipeline.
    """
    instances = filter_active_instances(instances)

    pipeline_builder = PipelineBuilder()
    pipeline_builder.add(CommonPipeline())
    pipeline_builder.add(OpenedXPipeline(instances))
    return pipeline_builder.build()


def instance_archive_pipeline(instances: List[Instance]) -> Dict:
    """
    Generate instance archive pipeline.
    """

    instances = filter_archived_instances(instances)

    # We do not know at this point when was the instance archived. Just a minute ago
    # by the prepare step or a month ago. Therefore, we need to call the archive
    # pipeline anyway.
    if instances and not os.getenv("ARCHIVE_INSTANCE_TRIGGER"):
        pipeline_builder = PipelineBuilder()
        pipeline_builder.add(CommonPipeline())
        pipeline_builder.add(OpenedXArchivePipeline(instances))
        return pipeline_builder.build()

    return DummyPipeline().build()


def instance_delete_pipeline(instances: List[Instance]) -> Dict:
    """
    Generate instance delete pipeline.

    The "delete" pipeline is twisted in the sense that we are not deleting the
    instances when the pipeline is initially triggered, but when the merge request is
    merged.
    """
    if len(instances) == 0:
        if not os.getenv("DELETE_INSTANCE_TRIGGER"):
            return infrastructure_pipeline()

    return DummyPipeline().build()


def batch_update_pipeline(repos: List[RepoRelease]):
    """
    Generates batch update pipeline.
    base image pipeline + Open edX pipeline
    """
    if len(repos) == 0:
        typer.echo("RepoReleases list is empty!")
        return DummyPipeline().build()

    pipeline_builder = PipelineBuilder()
    pipeline_builder.add(CommonPipeline())
    pipeline_builder.add(BaseImagePipeline(repos))

    # find affected instances
    affected_instances = []
    instances = [instance for instance in load_all_instances() if instance.batch_upgrade_enabled()]
    instances = filter_active_instances(instances)

    for instance in instances:
        for repo in repos:
            if repo.equals(instance.edx_platform_repo_release()):
                affected_instances.append(instance)
                break

    # generate Open edX jobs for only affected instances
    pipeline_builder.add(OpenedXPipeline(affected_instances))
    return pipeline_builder.build()


def build_image_pipeline(repos: List[RepoRelease]):
    """
    Generates image builder pipeline.

    Similarly to batch_update_pipeline, this pipeline build the base images,
    but won't trigger instance deployments.
    """
    if len(repos) == 0:
        typer.echo("RepoReleases list is empty!")
        return DummyPipeline().build()

    pipeline_builder = PipelineBuilder()
    pipeline_builder.add(BaseImagePipeline(repos, False))
    return pipeline_builder.build()


def commit_based_pipeline(commit_text: str) -> Dict:
    """
    Generate pipeline from a formatted commit text. Check all supported commit text formats here -

    https://grove.opencraft.com/discoveries/0002-commit-based-deployment-pipeline/

    Returns:
        Dict - representing a pipeline
    """
    commit_text = commit_text.strip()

    # Sometimes we hit a merge commit. Ex. commit -
    # -----------------------------------------------------------------
    # Merge branch 'deployment/testinstance/1' into 'shimulch/bb-5265' [AutoDeploy][Create] testinstance|1
    # or
    # Merge branch 'archive/testinstance' into 'shimulch/bb-5265' [AutoDeploy][Archive] testinstance
    # See merge request opencraft/dev/grove-development!68
    # -----------------------------------------------------------------
    # so we need to take only AutoDeploy part
    commit_text = re.sub(r"^Merge branch '.*' into '.*'", "", commit_text)
    commit_text = re.sub(r"See merge request .*$", "", commit_text)
    commit_text = commit_text.strip()
    trigger_keyword = commit_text.split(" ")[0]

    pipeline_builders = {
        # infrastructure pipeline
        "[AutoDeploy][Infrastructure][Destroy]": lambda _: infrastructure_destroy_pipeline(),
        # infrastructure pipeline
        "[AutoDeploy][Infrastructure]": lambda _: infrastructure_pipeline(),
        # instance create pipeline
        "[AutoDeploy][Create]": lambda text: instance_create_pipeline(parse_instances(text)),
        # instance update pipeline
        "[AutoDeploy][Update]": lambda text: instance_update_pipeline(parse_instances(text)),
        # instance archive pipeline
        "[AutoDeploy][Archive]": lambda text: instance_archive_pipeline(parse_instances(text)),
        # instance delete pipeline
        "[AutoDeploy][Delete]": lambda text: instance_delete_pipeline(parse_instances(text)),
        # batch upgrade pipeline
        "[AutoDeploy][Batch][Upgrade]": lambda text: batch_update_pipeline(parse_repository_release(text)),
        # build base image pipeline
        "[AutoDeploy][Build][Image]": lambda text: build_image_pipeline(parse_repository_release(text, False)),
    }

    pipeline_builder = pipeline_builders.get(trigger_keyword, lambda _: DummyPipeline().build())

    commit_text = commit_text.replace(trigger_keyword, "")
    return pipeline_builder(commit_text)


class AbortPipelineExecutor:
    """
    AbortPipelineExecutor provides a way to cancel any running batch deployment
    jobs. It calls the Gitlab API to fetch all the child pipelines of the given
    pipeline and send cancel request to each, and finally cancels the parent
    pipeline.

    When run from the Gitlab CI using a trigger, this uses CI_PROJECT_ID and
    the GITLAB_TOKEN from the CI environment variables and pipeline_id it is
    passed.

    When run from local, it uses the GITLAB_PROJECT_NUMERIC_ID and
    GITLAB_PASSWORD env vars exposed via the private.yml file from the
    `grove-template` configuration.

    """

    def __init__(self, pipeline_id: int):
        self.pipeline_id = pipeline_id
        self.project_id = os.getenv("CI_PROJECT_ID")  # Run from the CI
        if not self.project_id:
            self.project_id = os.getenv("GITLAB_PROJECT_NUMERIC_ID")  # Run from Local

        self.gitlab_token = os.getenv("GITLAB_TOKEN")  # Run from the CI
        if not self.gitlab_token:
            self.gitlab_token = os.getenv("GITLAB_PASSWORD")  # Run from local

        self.base_url = "https://gitlab.com/api/v4"

    def cancel_pipeline(self, pipeline_id: int) -> bool:
        """
        Sends the API request to cancel the child pipeline with the `child_id`

        Arguments:
        pipeline_id (int): Gitlab Pipeline ID to cancel

        Returns:
            Boolean - flag indicating if the cancel request is successful
        """
        logger.info(f"Cancelling pipeline {pipeline_id}")
        res = requests.post(
            f"{self.base_url}/projects/{self.project_id}/pipelines/{pipeline_id}/cancel",
            headers={"PRIVATE-TOKEN": self.gitlab_token},
        )

        if res.status_code != 200:
            logger.warning(f"API Failure: Cancel pipeline {pipeline_id}")
            logger.warning(f"{res.status_code} {res.reason} {res.content}")

        return res.status_code == 200

    def process(self) -> int:
        """
        Function that cancels the jobs of all the child pipelines of the
        current pipeline. This is used for cancelling any active deployments
        that might be happening due to auto-deployment but needs to be stopped.

        Reference:
        https://grove.opencraft.com/discoveries/0001-batch-redeployment/

        Returns:
            Boolean - if the pipeline was cancelled successfully
        """
        if not self.pipeline_id:
            raise ValueError("Pipeline ID cannot be empty.")
        if not self.project_id:
            raise ValueError("Project ID cannot be empty.")
        if not self.gitlab_token:
            raise ValueError("Gitlab Token missing. Cannot make API calls.")

        logger.info(f"Cancelling the pipeline {self.pipeline_id}")
        pipeline = self.cancel_pipeline(self.pipeline_id)
        return pipeline


def abort_pipeline(pipeline_id: int) -> int:
    """
    Abort a Gitlab CI pipeline with the given ID and all of its child pipelines

    Parameters:
        pipeline_id - ID of the Gitlab Pipeline

    Returns:
        Int - the number of pipelines aborted in total (parent + children)
    """
    exe = AbortPipelineExecutor(pipeline_id)
    return exe.process()
