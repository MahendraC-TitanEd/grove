# -*- coding: utf-8 -*-
"""
Open edX instance monitoring
"""
import textwrap
from collections import namedtuple

import typer

from grove.newrelic import NewRelicApi

NewRelicMonitor = namedtuple("NewRelicMonitor", ["policy_id", "monitor_ids"])
CHANNEL_PREFIX = "grove-"


class Monitoring:
    """
    Sets up availability monitoring with NewRelic for an Open Edx instance.
    """

    def __init__(self, emails, urls, policy_name, api_key, region):
        self.emails = emails
        self.policy_name = policy_name
        self.api = NewRelicApi(api_key, region)
        self.urls = urls

    @staticmethod
    def _generate_nrql_condition_name(url: str, instance_name: str) -> str:
        """
        Generate monitor condition name for the given URL and short name of the instance.

        Based on the documentation of New Relic, the monitor name should be less than
        or equal to 64 chars. The shortening will not split a word into two, instead it
        will skip the word which would be splitted.

        Also, based on New Relic's documentation, the names cannot contain angle brackets
        ("<" and ">"), so we need to remove them in case the instance names would contain any.
        """

        monitoring_name = f"{url} of {instance_name}"

        shortened_name = textwrap.shorten(monitoring_name, 64, placeholder="...")

        return shortened_name.replace("<", "").replace(">", "")

    def _get_existing_alert_policy(self):
        policies = self.api.get_alert_policies(self.policy_name)
        for policy in policies["policies"]:
            if policy["name"] == self.policy_name:
                typer.echo("Found existing alert policy %s" % policy)
                return policy
        return None

    def get_or_create_alert_policy(self):
        """
        Creates the main alert policy everything is linked to.
        """
        typer.echo(f"Adding an alert policy: {self.policy_name}...")
        existing_policy = self._get_existing_alert_policy()
        if existing_policy is not None:
            return existing_policy["id"]
        typer.echo("No active alert policy found. Creating a new one...")
        return self.api.create_alert_policy(self.policy_name)["policy"]["id"]

    def _update_email_notification_channel(self, channel, policy_id):
        """
        Checks and updates the supplied notification channel depending
        on whether the channel is still needed or not.

        If the recipients in the channel overlap with self.emails, then
        the policy_id is added to the channel if required.

        If there is no overlap, the policy_id is removed from the channel
        if needed.

        Returns the emails in self.emails that aren't part of the channel's
        recipients.
        """

        recipients = channel["configuration"].get("recipients", [])
        policy_ids = set(channel["links"]["policy_ids"])

        if isinstance(recipients, str):
            recipients = [recipients]

        valid_emails_in_channel = set(self.emails).intersection(recipients)

        channel_contains_policy = policy_id in policy_ids

        is_grove_channel = channel["name"].startswith(CHANNEL_PREFIX)
        emails_to_add = set()

        # Don't modify channels that aren't managed by Grove
        if not is_grove_channel:
            return False, self.emails

        if channel_contains_policy:
            if not valid_emails_in_channel:
                typer.echo(f"Removing policy_id from channel {channel}.")

                self.api.remove_policy_from_channel(policy_id, channel["id"])
            else:
                emails_to_add = set(self.emails).difference(recipients)

        elif valid_emails_in_channel:
            typer.echo(f"Adding notification channel {channel['id']} to policy.")
            self.api.add_notification_channels_to_policy(policy_id, [channel["id"]])
            emails_to_add = set(self.emails).difference(recipients)
        else:
            emails_to_add = set(self.emails)

        return channel_contains_policy, emails_to_add

    def update_email_notification_channels(self, policy_id):
        """
        Adds notification channels for self.emails.
        """
        typer.echo("Retrieving email notification channels.")

        current_channels = [
            channel
            for channel in self.api.get_email_notification_channels()["channels"]
            if channel["name"].startswith(CHANNEL_PREFIX)
        ]

        if current_channels:
            emails_to_add = set()
        else:
            emails_to_add = set(self.emails)

        channels_in_policy = []

        for channel in current_channels:
            (
                channel_contains_policy,
                emails_not_in_channel,
            ) = self._update_email_notification_channel(channel, policy_id)

            if channel_contains_policy:
                emails_to_add.update(emails_not_in_channel)
                channels_in_policy.append(channel)

        for email in emails_to_add:
            typer.echo(f"Adding email {email}.")
            channel_response = self.api.add_email_notification_channel(email, [policy_id], CHANNEL_PREFIX)
            channels_in_policy.append(channel_response["channels"][0]["id"])

        return channels_in_policy

    def delete_alert_condition_for_monitor(self, alert_conditions, monitor):
        """
        Deletes any associated conditions related to the provided monitor
        """
        nrql_condition_name = self._generate_nrql_condition_name(monitor["uri"], self.policy_name)

        for condition in alert_conditions:
            if condition["name"] == nrql_condition_name:
                typer.echo(f"Deleting alert condition {condition['name']}.")
                self.api.delete_alert_nrql_condition(condition["id"])

    def update_monitors(self, policy_id):
        """
        Create synthetic monitors where needed.

        Remove any outdated ones.
        """
        typer.echo("Creating synthetic monitors if required.")

        # Find existing monitors so we can delete the ones no longer needed.
        existing_monitors = self.api.get_monitors_for_policy(policy_id)
        valid_monitor_ids = [z["id"] for z in existing_monitors if z["uri"] in self.urls]
        alert_conditions = self.api.get_alert_nrql_conditions(policy_id)

        new_urls = self.urls[:]
        for monitor in existing_monitors:
            if monitor["uri"] in self.urls:
                new_urls.remove(monitor["uri"])
                valid_monitor_ids.append(monitor["id"])
                typer.echo(f"Skipping monitor {monitor['uri']} as it is already added.")
            else:
                typer.echo(f"Removing monitor: {monitor['uri']}.")
                self.api.delete_synthetics_monitor(monitor["id"])
                self.delete_alert_condition_for_monitor(alert_conditions["nrql_conditions"], monitor)

        for url in new_urls:
            typer.echo(f"Adding monitor: {url}.")
            monitor_name = self._generate_synthetic_monitor_name(policy_id, url)
            valid_monitor_ids.append(self.api.create_synthetics_monitor(monitor_name, url))

            nrql_condition_name = self._generate_nrql_condition_name(url, self.policy_name)
            self.api.add_alert_nrql_condition(policy_id, url, nrql_condition_name)
            typer.echo(f"Monitor {url} added successfully.")

        if new_urls:
            typer.echo("Monitors created successfully.")
        else:
            typer.echo("No new monitors created.")
        return valid_monitor_ids

    def _generate_synthetic_monitor_name(self, policy_id, url):
        return f"{policy_id}-{url}"

    def _get_policy_id_from_synthetic_monitor(self, monitor):
        try:
            monitor_policy_id, _ = monitor["name"].split("-", 1)
        except ValueError:
            return

        try:
            return int(monitor_policy_id)
        except ValueError:
            pass

    def enable_monitoring(self):
        """
        Enable monitoring for the provided instance.

        Creates a New Relic alert policy and the associated notification
        channels.
        """
        typer.echo("Enabling monitoring for %s" % self.policy_name)
        policy_id = self.get_or_create_alert_policy()
        self.update_email_notification_channels(policy_id)
        monitor_ids = self.update_monitors(policy_id)
        return NewRelicMonitor(policy_id, monitor_ids)

    def disable_monitoring(self):
        """
        Disables monitoring for the provided instance.

        Removes any synthetic alerts that have been added.

        Deletes the monitors that have been created first, then
        the policy. Returns `True` if successful, otherwise `False`
        """
        existing_policy = self._get_existing_alert_policy()
        if existing_policy is None:
            typer.echo("No policy ids found to disable")
            return False

        policy_id = existing_policy["id"]
        typer.echo("Disabling monitoring for %s" % self.policy_name)

        existing_monitors = self.api.get_monitors_for_policy(policy_id)
        alert_conditions = self.api.get_alert_nrql_conditions(policy_id)

        for monitor in existing_monitors:
            typer.echo(f"Removing monitor: {monitor['uri']}.")
            self.api.delete_synthetics_monitor(monitor["id"])
            self.delete_alert_condition_for_monitor(alert_conditions["nrql_conditions"], monitor)

        typer.echo(f"Removing policy_id: {policy_id}")
        self.api.delete_alert_policy(policy_id)
        return True
