class GroveError(Exception):
    pass


class CommandFailedError(GroveError):
    def __init__(self, message: str, output: list[str]):
        """
        Instantiate a CommandFailedError, storing the full output of the command.
        """
        super().__init__(message)
        self.output = output
