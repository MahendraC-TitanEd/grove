import os
import re
import shutil
import venv
from pathlib import Path
from typing import Dict, List, Optional, Tuple
from urllib.error import URLError
from urllib.request import urlopen

import typer
import yaml
from tutor.config import load as tutor_config_loader
from tutor.env import Renderer as TutorEnvRenderer

from grove import terraform
from grove.const import (
    CI_REGISTRY_IMAGE,
    GROVE_DEFAULTS_DIR,
    INSTANCES_DIR,
    VENV_DIR_NAME,
    WORKSPACE_DIR,
)
from grove.exceptions import GroveError
from grove.k8s import get_v1apps_api, restart_deployment
from grove.type_defs import (
    ClusterConfig,
    GroveConfig,
    InstanceCustomize,
    Theme,
    TutorEnv,
)
from grove.utils import (
    RepoRelease,
    ensure_ssh_key,
    execute,
    get_ssh_key_path,
    slugify_instance_name,
    tutor_env_pull,
    tutor_env_push,
)

MONITORED_HOSTS = ("LMS_HOST", "CMS_HOST", "PREVIEW_LMS_HOST")


class Instance:
    """
    This class represents a single Open edX instance in Grove.
    """

    def __init__(self, instance_name: str) -> None:
        """
        Initialize an instance with instance_name.
        """
        # name of the instance
        self.instance_name: str = slugify_instance_name(instance_name)

        # instance root directory
        self.root_dir: Path = INSTANCES_DIR / self.instance_name

        # instance tutor env directory
        self.env_dir: Path = self.root_dir / "env"

        # instance tutor env-overrides directory
        self.env_override_dir: Path = self.root_dir / "env-overrides"

        # cluster.yml file path
        self.cluster_config_file: Path = WORKSPACE_DIR / "cluster.yml"

        # grove.yml file path
        self.grove_config_file: Path = self.root_dir / "grove.yml"

        # config.yml file path
        self.tutor_config_file: Path = self.root_dir / "config.yml"

        # custom scripts directory
        self.scripts_dir: Path = self.root_dir / "scripts"
        self.grove_scripts_dir: Path = self.env_dir / "build" / "openedx" / "grove" / "scripts"

        # root directory for placing custom theme
        self.edxapp_theme_root_dir: Path = self.env_dir / "build" / "openedx" / "themes"

        # MFE directory for placing custom theme
        self.mfe_theme_root_dir: Path = self.env_dir / "plugins" / "mfe" / "build" / "mfe" / "themes"

        # Plugins directory
        self.edxapp_plugins_root_dir: Path = self.env_dir / "build" / "openedx" / "requirements" / "private"

        # SSH key for the instance
        self.ssh_key_path: Path = get_ssh_key_path(self.instance_name)

        # cluster.yml file loaded as dictionary
        self._cluster_config: ClusterConfig = None

        # grove.yml file loaded as dictionary
        self._grove_config: GroveConfig = None

        # config.yml file loaded as dictionary
        self._tutor_config: Optional[dict] = None

        # Gitlab project id
        self.gitlab_project_id = os.getenv("CI_PROJECT_ID")  # Run from the CI
        if not self.gitlab_project_id:
            self.gitlab_project_id = os.getenv("GITLAB_PROJECT_NUMERIC_ID")  # Run from Local

        # Virtual environment directory
        self.venv_dir = self.root_dir / VENV_DIR_NAME

        # Tutor executable path directory
        self.tutor_executable = self.venv_dir / "bin" / "tutor"

    def tutor_env_sync(self):
        """
        Utility method to sync tutor env directory with s3.
            - Pull from s3
            - runs `tutor config save`
            - renders `env-overrides` dir
            - Push to s3
        """
        self.tutor_env_pull()
        self.tutor_save_config()
        self.tutor_env_override()
        self.tutor_env_push()

    def tutor_save_config(self, env: Optional[dict] = None):
        """
        Utility method to run `tutor config save` command
        """
        if env is None:
            env = {}

        self.tutor_command("config save", env=env)

    def load_instance_configs(self):
        """
        Load Grove and Tutor config for this instance.
        """
        with open(self.cluster_config_file) as file:
            self._cluster_config = yaml.safe_load(file)
        with open(self.grove_config_file) as file:
            self._grove_config = yaml.safe_load(file)
        with open(self.tutor_config_file) as file:
            self._tutor_config = yaml.safe_load(file)

    def save_instance_configs(self):
        """
        Save Grove and Tutor config for this instance.
        """
        # disabling sorting of the keys on dump, because some functionality,
        # e.g. SCSS overrides requires the keys to be dumped in the same order
        # they were read
        with open(self.grove_config_file, "w") as file:
            yaml.safe_dump(self._grove_config, file, sort_keys=False)
        with open(self.tutor_config_file, "w") as file:
            yaml.safe_dump(self._tutor_config, file, sort_keys=False)

    @property
    def cluster_config(self):
        if self._cluster_config is None:
            self.load_instance_configs()
        return self._cluster_config

    @property
    def grove_config(self):
        if self._grove_config is None:
            self.load_instance_configs()
        return self._grove_config

    @property
    def tutor_config(self):
        if self._tutor_config is None:
            self.load_instance_configs()
        return self._tutor_config

    def exists(self) -> bool:
        """
        Check if this instance exists.
        """
        return Path.exists(self.root_dir)

    def is_name_valid(self) -> bool:
        """
        Return if the instance name is valid.

        The instance name is valid if the name is:
        - not managed by Grove, and
        - not a reserved K8s namespace, and
        - not starts with an illegal (other than a-z) character, and
        - not longer than 49 characters

        At this point, the instance name will be normalized, hence we don't
        need to check for that.

        The 49 characters limit is coming from that DigitalOcean cannot handle
        bucket names that have more than 63 characters, and we add 14 chars of
        pre- and suffix to the instance name for buckets.
        """
        restricted_names = {
            "default",
            "gitlab-kubernetes-agent",
            "kube-node-lease",
            "kube-public",
            "kube-system",
            "monitoring",
            "openfaas",
            "openfaas-fn",
        }

        registered_instance_names = {instance for instance in os.listdir(INSTANCES_DIR) if os.path.isdir(instance)}

        is_not_restricted = self.instance_name not in restricted_names
        is_not_registered = self.instance_name not in registered_instance_names
        starts_with_az = re.match(r"^[^a-z].*", self.instance_name) is None
        within_length_limit = 49 >= len(self.instance_name) >= 3

        return is_not_restricted and is_not_registered and starts_with_az and within_length_limit

    def tutor_default_env(self) -> TutorEnv:
        """
        Return common Tutor env for current instance.
        """
        return {
            "TUTOR_ID": self.instance_name,
            "TUTOR_ROOT": str(self.root_dir),
            "TUTOR_K8S_NAMESPACE": self.instance_name,
        }

    def tutor_env(self) -> Dict:
        """
        Build Tutor environment
        """
        env = self.tutor_default_env()
        env.update(self.instance_image_envs())
        if terraform.is_infrastructure_ready(self.instance_name):
            env.update(terraform.get_env(self))
        else:
            typer.echo(f"Infrastructure is not ready yet for {self.instance_name}. Terraform state is not available.")
        return env

    def create(self):
        """
        Create current instance. Raises GroveError if instance is already created.
        """
        if self.exists():
            raise GroveError(f"Instance {self.instance_name} already exists. Can't create.")

        if not self.is_name_valid():
            raise GroveError(f'Instance name "{self.instance_name}" is not valid. Can\'t create.')

        typer.echo(f"Copying {GROVE_DEFAULTS_DIR} to {self.root_dir}...")
        shutil.copytree(GROVE_DEFAULTS_DIR, self.root_dir)

    def prepare(self, customization: InstanceCustomize):
        """
        If current instance doesn't exist creates it.
        Then updates grove.yml and config.yml with given customization.
        Finally, runs `tutor config save`
        """
        is_existing_instance = self.exists()

        if not is_existing_instance:
            typer.echo(f"Instance {self.instance_name} doesn't exist, creating...")
            self.create()
            typer.echo(f"Instance {self.instance_name} created.")

        self.customize(customization)

        if is_existing_instance:
            self.tutor_save_config()
            self.load_instance_configs()

    def _get_customized_instance_url(self, url_key: str, default_prefix: str | None = None):
        """
        Return the customized instance URL if the customization is provided.
        """

        default_url_parts = [self.instance_name, self.cluster_config["variables"]["TF_VAR_cluster_domain"]]
        if default_prefix is not None:
            default_url_parts.insert(0, default_prefix)

        return self.tutor_config.get(url_key, ".".join(default_url_parts))

    def customize(self, customization: InstanceCustomize):
        """
        Given a customization, it will override and save grove.yml and config.yml for Tutor.

        Args:
            customization (InstanceCustomize)
        """
        typer.echo(f"Applying customization for {self.instance_name}.")
        self.load_instance_configs()

        self.grove_config.update(customization.get("grove", {}))
        self.tutor_config.update(customization.get("tutor", {}))
        self.tutor_config["ID"] = self.instance_name
        self.tutor_config["K8S_NAMESPACE"] = self.instance_name
        self.tutor_config["ELASTICSEARCH_INDEX_PREFIX"] = self.instance_name
        self.tutor_config["MYSQL_ROOT_PASSWORD"] = "to-be-set-via-environment"  # avoid tutor generating secrets

        # set the default values for the hosts
        self.tutor_config["LMS_HOST"] = self._get_customized_instance_url("LMS_HOST")
        self.tutor_config["PREVIEW_LMS_HOST"] = self._get_customized_instance_url("PREVIEW_LMS_HOST", "preview")
        self.tutor_config["CMS_HOST"] = self._get_customized_instance_url("CMS_HOST", "studio")
        self.tutor_config["DISCOVERY_HOST"] = self._get_customized_instance_url("DISCOVERY_HOST", "discovery")
        self.tutor_config["ECOMMERCE_HOST"] = self._get_customized_instance_url("ECOMMERCE_HOST", "ecommerce")
        self.tutor_config["MFE_HOST"] = self._get_customized_instance_url("MFE_HOST", "apps")

        typer.echo(f"Applying env configs for {self.instance_name}")

        self.save_instance_configs()

    def delete(self):
        """
        Deletes an instance. Don't use this unless sure. Currently used only for testing.
        """
        typer.echo(f"Deleting {self.instance_name}!")
        shutil.rmtree(self.root_dir)

    def tutor_env_pull(self):
        """
        Pull env directory from s3
        """
        use_terraform = terraform.is_infrastructure_ready(self.instance_name)
        if use_terraform:
            tutor_env_pull(self.tutor_env())
        else:
            typer.echo(f"Unable to pull tutor env from s3 for {self.instance_name}. Infrastructure is not ready.")

    def tutor_env_push(self):
        """
        Push env directory to s3
        """
        use_terraform = terraform.is_infrastructure_ready(self.instance_name)
        if use_terraform:
            tutor_env_push(self.tutor_env())
        else:
            typer.echo(f"Unable to push tutor env to s3 for {self.instance_name}. Infrastructure is not ready.")

    def pre_deploy(self):
        """
        Prepare current instance for deployment.
        """
        if terraform.is_infrastructure_ready(self.instance_name):
            self.tutor_env_pull()
            self.tutor_save_config()
            self.tutor_env_override()
            self.prepare_ssh()
            self.prepare_scripts_dir()
            self.prepare_themes()
            self.tutor_env_push()
        else:
            raise GroveError(f"Infrastructure is not ready for {self.instance_name}.")

    def prepare_ssh(self):
        """
        Prepare the ssh key and configure ssh.
        """
        ssh_key_content = self.grove_config.get("PRIVATE_SSH_KEY")

        if not ssh_key_content:
            typer.echo("No ssh key provided. Skipping writing ssh key.")
            return

        typer.echo(f"Writing ssh key to {self.ssh_key_path}...")
        ensure_ssh_key(self.ssh_key_path, ssh_key_content)

        ssh_config_command = f"""
        echo "Host *\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile /dev/null\n\tIdentityFile {self.ssh_key_path}" \
            > ~/.ssh/config
        """
        execute(ssh_config_command)

        ssh_add_command = "eval $(ssh-agent -s)"
        execute(ssh_add_command)

    def prepare_themes(self):
        """
        Iterate over all themes and prepare them.
        """
        for theme in self.grove_config.get("THEMES", []):
            self.prepare_theme(theme)

    def prepare_theme(self, theme: Theme):
        """
        Install custom theme for this instance
        """
        theme_name = theme.get("COMPREHENSIVE_THEME_NAME")
        theme_repo = theme.get("COMPREHENSIVE_THEME_SOURCE_REPO")
        theme_version = theme.get("COMPREHENSIVE_THEME_VERSION")

        if not theme_name:
            typer.echo("No theme repository provided. Skipping installing theme.")
            return

        theme_dir = self.edxapp_theme_root_dir / theme_name

        if Path(theme_dir).exists():
            shutil.rmtree(theme_dir)

        if not all([theme_name, theme_repo, theme_version]):
            typer.echo("No theme repository provided. Skipping installing theme.")
            return

        self.clone_theme_into_tutor_env(theme_version, theme_repo, theme_name)

        if theme.get("ENABLE_SIMPLE_THEME_SCSS_OVERRIDES"):
            typer.echo("Simple theme SCSS override is enabled.")

            scss_dir = theme_dir / "lms" / "static" / "sass"

            # a notice to append on top of the generated files
            # noqa: E501
            autogenerated_notice = (
                "// This file has been autogenerated by Grove.\n" "// Don't modify this file manually.\n"
            )

            self.generate_common_scss_overrides(theme, scss_dir, autogenerated_notice)

            self.generate_lms_scss_overrides(theme, scss_dir, autogenerated_notice)

            self.download_and_set_static_files(theme, theme_dir)
        else:
            typer.echo("Simple theme SCSS override is disabled.")

        self.copy_theme_into_mfe_build_dir()

    def prepare_scripts_dir(self):
        """
        Prepare scripts directory for this instance
        """

        # recreate the grove scripts directory if exists
        if self.grove_scripts_dir.exists():
            shutil.rmtree(self.grove_scripts_dir)

        typer.echo(f"Creating {self.grove_scripts_dir} directory")
        os.makedirs(self.grove_scripts_dir)

        if self.scripts_dir.exists():
            typer.echo(f"Copying scripts directory to {self.grove_scripts_dir}")
            shutil.copytree(self.scripts_dir, self.grove_scripts_dir, dirs_exist_ok=True)
        else:
            # Create an empty file for the grove scripts directory if it is empty
            # to prevent being ignored.
            if len(os.listdir(self.grove_scripts_dir)) == 0:
                typer.echo(f"No scripts found in {self.scripts_dir}. Creating an empty file.")
                Path(self.grove_scripts_dir / ".gitkeep").touch()

    def clone_private_plugins(self):
        """
        Clone private plugins into the instance's edxapp plugins directory
        """

        # Setup private ssh key
        self.prepare_ssh()

        # Clean up old checked-out plugins
        cleanup_command = f"rm -rf {self.edxapp_plugins_root_dir}/*"
        execute(cleanup_command)

        plugins = self.grove_config.get("OPENEDX_PRIVATE_REQUIREMENTS", [])
        if not plugins:
            typer.echo("No private plugins provided. Skipping installing private plugins.")
            return

        for plugin in plugins:
            self.clone_plugin_into_tutor_env(plugin)

    def clone_plugin_into_tutor_env(self, plugin):
        plugin_name: str = plugin.split("/")[-1].split(".")[0]

        # Add `platform-plugin-` prefix because this is need to keep Tutor happy
        directory_name = f"platform-plugin-{plugin_name}"
        typer.echo(f"Cloning {plugin_name} into {self.edxapp_plugins_root_dir} as {directory_name}")

        git_command = f"git clone {plugin} {self.edxapp_plugins_root_dir / directory_name}"
        execute(git_command)

        self.tutor_command(f"mounts add {self.edxapp_plugins_root_dir / directory_name}")

    def clone_theme_into_tutor_env(self, theme_version, theme_repo, theme_name):
        typer.echo(f"Cloning {theme_repo}#{theme_version} into {theme_name}")

        git_command = f"git clone --branch {theme_version} {theme_repo} {theme_name}"
        cd_command = f"cd {self.edxapp_theme_root_dir}"
        execute(f"{cd_command} && {git_command}")

    def copy_theme_into_mfe_build_dir(self):
        typer.echo("Copying theme files into build/mfe/themes folder.")

        execute(f"mkdir -p {self.mfe_theme_root_dir}")
        execute(f"cp -r {self.edxapp_theme_root_dir}/. {self.mfe_theme_root_dir}")

    def generate_common_scss_overrides(self, theme: Theme, scss_dir: Path, autogenerated_notice: str):
        scss_overrides = theme.get("SIMPLE_THEME_SCSS_OVERRIDES", {})
        # generate common-variables.scss file
        common_variables_file = scss_dir / "common-variables.scss"
        scss_content = "\n".join([f"${variable}: {value};\n" for variable, value in scss_overrides.items()])
        Path(common_variables_file).unlink(missing_ok=True)
        Path(common_variables_file).write_text(f"{autogenerated_notice}\n{scss_content}", encoding="utf-8")
        typer.echo("Generated common-variables.scss")

    def generate_lms_scss_overrides(self, theme: Theme, scss_dir: Path, autogenerated_notice: str):
        # generates _lms-overrides.scss
        lms_overrides_file = scss_dir / "_lms-overrides.scss"
        extra_scss = theme.get("SIMPLE_THEME_EXTRA_SCSS", "")
        Path(lms_overrides_file).unlink(missing_ok=True)
        Path(lms_overrides_file).write_text(
            f"{autogenerated_notice}@import 'common-variables';\n{extra_scss}",
            encoding="utf-8",
        )
        typer.echo("Generated _lms-overrides.scss")

    def download_and_set_static_files(self, theme: Theme, theme_dir: Path):
        """
        Download static files from their url and save them in the theme repo.
        To override the default static image files such as favicon, header/footer
        logo, banner image, etc. we need to have a corresponding image file in
        the correct directory of `edx-simple-theme`.
        This method downloads the static files from the given urls and saves them
        in the given file path inside the cloned copy of the edx-simple-theme in
        the tutor env directory which is then complied during the image build stage.
        """
        static_files = theme.get("SIMPLE_THEME_STATIC_FILES_URLS")
        if static_files:
            for static_file in static_files:
                dest = theme_dir / static_file["dest"]
                dest.parent.mkdir(parents=True, exist_ok=True)
                try:
                    response = urlopen(static_file["url"])
                    with open(dest, "wb") as file:
                        file.write(response.read())
                except URLError as e:
                    typer.echo(f"Error {e} while downloading file from url {static_file['url']}")

        typer.echo("Downloaded all static files")

    def tutor_env_override(self, env: Optional[dict] = None):
        """
        Render configs from env-overrides dir.
        """
        if env is None:
            env = {}

        if Path(self.env_dir).exists():
            if Path(self.env_override_dir).exists():
                env_overrides_dir = str(self.env_override_dir)
                env_dir = str(self.env_dir)

                config = tutor_config_loader(self.root_dir)
                renderer = TutorEnvRenderer(config)
                renderer.template_roots = [env_overrides_dir]
                renderer.render_all_to(env_dir)
            else:
                typer.echo(f"No env-overrides directory found for {self.instance_name}.")
        else:
            typer.echo(f"No env directory found for {self.instance_name}. Skipping env-overrides config rendering!")

    def tutor_command(self, argstr: str, env=None) -> str:
        """
        Run Tutor for this instance.
        """
        if env is None:
            env = {}

        tutor_env = self.tutor_env()
        tutor_env.update(env)
        if not self.tutor_executable.exists():
            typer.echo("Instance virtual environment is outdated. Updating...")
            self.venv_update()

        if not self.tutor_executable.exists():
            raise GroveError(
                "The tutor executable could not found. Please check your instance's requirements.txt"
                f" and run `./grove venv update {self.instance_name}`."
            )
        return execute(f"{self.tutor_executable} {argstr}", capture_output=True, env=tutor_env)

    def tutor_version(self):
        """
        Get the major version of Tutor used by the instance in the form of an integer.
        """
        version_output = self.tutor_command("--version")
        match = re.search(r"(\d+)\.(\d+)\.(\d+)", version_output)
        return int(match[1])

    def deploy(self):
        """
        Deploy this instance to Kubernetes.
        """
        if terraform.is_infrastructure_ready(self.instance_name):
            self.tutor_env_pull()
            self.tutor_save_config()
            self.tutor_env_override()
            launch_command = "launch" if self.tutor_version() >= 15 else "quickstart"
            self.tutor_command(f"k8s {launch_command} --non-interactive")
            self.tutor_env_push()
        else:
            raise GroveError(f"Infrastructure is not ready for {self.instance_name}.")

    def archive(self):
        """
        Remove instance namespace and all its resources, but keep the infrastructure
        components.
        """
        execute(f"kubectl delete namespace --ignore-not-found=true {self.instance_name}")

    def enable_theme(self):
        """
        Enable comprehensive theme. Instance have to be deployed before enabling theme.

        Get the default theme from the grove config and enable it. If no default theme
        is found, enable the first theme in the list of themes.

        If no themes are set, or the theme name is not defined for the selected theme,
        no changes are made and the theme will not be enabled.
        """

        themes = self.grove_config.get("THEMES", [])
        if not themes:
            return

        default_theme = next(filter(lambda theme: theme.get("DEFAULT") is True, themes), themes[0])

        theme_name = default_theme.get("COMPREHENSIVE_THEME_NAME")
        if theme_name is None:
            return

        set_theme_command = "do settheme" if self.tutor_version() >= 15 else "settheme"
        self.tutor_command(
            f"k8s {set_theme_command} {theme_name} "
            f"-d $({self.tutor_executable} config printvalue LMS_HOST) "
            f"-d $({self.tutor_executable} config printvalue CMS_HOST)"
        )

    def create_demo_data(self):
        """
        Create demo user and course for the instance.

        Tutor does not support the old-fashioned demo users, hence we simply
        create a superuser.
        """

        if not self.grove_config.get("PR_WATCHER"):
            typer.echo("No PR watcher config found. Skipping creating demo data.")
            return

        tutor_exec_env = "k8s do" if self.tutor_version() >= 15 else "k8s"
        # self.tutor_command(f"{tutor_exec_env} importdemocourse")
        self.tutor_command(
            f"{tutor_exec_env} createuser --staff --superuser "
            "--password openedx openedx openedx@example.com"
        )

    def force_update_k8s_images(self):
        """
        After deployment we need to update docker images in k8s.
        """
        # updating docker images in kubernetes
        # https://docs.tutor.overhang.io/k8s.html#updating-docker-images
        v1_apps = get_v1apps_api()
        deployments = v1_apps.list_namespaced_deployment(self.instance_name)

        # patch redis first
        for deployment in deployments.items:
            if 'redis' in deployment.metadata.name:
                restart_deployment(deployment.metadata.name, self.instance_name)

        # patch other deployments
        for deployment in deployments.items:
            if 'redis' not in deployment.metadata.name:
                restart_deployment(deployment.metadata.name, self.instance_name)

    def post_deploy(self):
        """
        Post deployment steps.
        """
        self.enable_theme()
        self.create_demo_data()
        self.force_update_k8s_images()

    def instance_images(self) -> List[Tuple[str, Dict]]:
        """
                Get list of images and their build config for a given instance.
        `
                Returns:
                    List[Tuple[str, Dict]] - (image name, build config)
        """
        return [
            (image_name, build_config)
            for image_name, build_config in self.grove_config.get("IMAGE_BUILD_CONFIG", {}).items()
        ]

    def instance_image_envs(self) -> Dict:
        """
        Return custom docker image related environment variables for Tutor.
        """
        env = {}
        for image_name, build_config in self.instance_images():
            docker_image_var = build_config["DOCKER_IMAGE_VAR"]
            env[docker_image_var] = f"{CI_REGISTRY_IMAGE}/{self.instance_name}/{image_name}:latest"
            env[f"{docker_image_var}_CACHE"] = f"{CI_REGISTRY_IMAGE}/{self.instance_name}/{image_name}:build-cache"
        return env

    def edx_platform_repo_release(self):
        """
        Returns RepoRelease instance for LMS & CMS

        Returns:
            RepoRelease
        """
        return RepoRelease(
            self.tutor_config["EDX_PLATFORM_REPOSITORY"],
            self.tutor_config["EDX_PLATFORM_VERSION"],
        )

    def batch_upgrade_enabled(self):
        return not self.grove_config.get("BATCH_UPGRADE_DISABLED", False)

    def venv_update(self) -> None:
        """
        Create or update the instance's virtual environment.
        """
        builder = venv.EnvBuilder(with_pip=True)
        if not self.venv_dir.exists():
            builder.create(self.venv_dir)
        self.venv_install_requirements()

    def venv_install_requirements(self) -> None:
        """
        Install the Python requirements for the virtual environments.

        Assumes there's a requirements.txt file in the instance's
        directory.
        """
        python_exe = self.venv_dir.joinpath("bin/python")
        requirements_path = self.root_dir / "requirements.txt"

        if requirements_path.exists():
            execute(f"{python_exe} -m pip install -r {requirements_path}")
        else:
            raise GroveError(
                "Your instance does not have a requirements.txt file."
                "Please add the file from your `defaults` directory."
            )

    def venv_reset(self) -> None:
        """
        Delete and recreate the instance's virtual environment.
        """
        if self.venv_dir.exists():
            shutil.rmtree(self.venv_dir)
        self.venv_update()


def load_all_instances() -> List[Instance]:
    """
    Load all available instances.
    """
    return [Instance(name) for name in os.listdir(INSTANCES_DIR) if os.path.isdir(f"{INSTANCES_DIR}/{name}")]


def load_all_repo_releases() -> List[RepoRelease]:
    """
    Load all available RepoReleases.
    """
    return [instance.edx_platform_repo_release() for instance in load_all_instances()]
