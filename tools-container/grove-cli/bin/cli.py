#!/usr/bin/python3
"""
Grove CLI commands are placed here.
"""
import json
import os
from typing import List, Optional

import typer
import yaml

from grove import const
from grove.exceptions import CommandFailedError, GroveError
from grove.handlers import base_image_upgrade_handler
from grove.instance import Instance
from grove.pipeline import OpenedXPipeline, abort_pipeline, commit_based_pipeline
from grove.utils import (
    check_docker_is_running,
    execute,
    instance_customization_from_trigger_payload,
)
from grove.venv_cli import app as virtual_environment_app

app = typer.Typer()
tutor_app = typer.Typer()
app.add_typer(
    tutor_app,
    name="tutor",
    help="Tutor wrapper commands, run `grove tutor --help` for more.",
)

run_pipeline_app = typer.Typer()
app.add_typer(
    run_pipeline_app,
    name="run-pipeline",
    help="Pipeline commands, run grove run-pipeline --help for more.",
)

app.add_typer(
    virtual_environment_app,
    name="venv",
    help="Virtual environment management commands.",
)


@app.command()
def new(instance_name: str):
    """
    Create a new Open edX instance

    This command will copy the default configuration and create a new instance
    $INSTANCE_NAME in the instances/$INSTANCE_NAME directory. This command
    should not have any dependency on Terraform. But it uses Tutor to prepare
    the initial configuration.
    """
    typer.echo(f"Creating new instance: {instance_name}")
    try:
        instance = Instance(instance_name)
        instance.create()
        typer.echo("Done!")
    except GroveError as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def prepare(instance_name: str):
    """
    Prepares a new Open edX instance, creates if not exists. Primarily used by CI.
    """
    try:
        instance = Instance(instance_name)
        instance.prepare(instance_customization_from_trigger_payload())
        typer.echo(f"Instance {instance_name} prepared!")
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def pipeline(
    commit_text: str,
    output_file: str,
):
    """
    Generate Pipelines based on commit text.

    Given a `commit_text`, Grove CLI will generate the pipeline for it and write
    it on `output_file` path. This command will be used by GitLab CI to
    generate child pipeline based on commit messages.
    Check commit based pipeline[0] for more info.

    [0] - https://grove.opencraft.com/discoveries/0002-commit-based-deployment-pipeline
    """
    pipeline = commit_based_pipeline(commit_text)
    with open(output_file, "w") as file:
        yaml.safe_dump(pipeline, file)


@app.command()
def webhookpipeline(
    payload_file: str = typer.Argument(None, envvar="TRIGGER_PAYLOAD"),
    output: str = typer.Option(None, help="Output File Path for pipeline"),
):
    """
    Create pipelines from GitLab and GitHub webhook payload from $TRIGGER_PAYLOAD.
    Only to be used from CI.
    """
    if payload_file:
        with open(payload_file) as file:
            payload = json.load(file)
        pipeline = base_image_upgrade_handler(payload)
        if output:
            with open(output, "w") as file:
                yaml.safe_dump(pipeline, file)
        else:
            typer.echo("No file provided to write pipeline.")
            typer.echo(pipeline)
    else:
        typer.echo("payload file is required!")
        raise typer.Abort()


@app.command()
def abortpipeline(pipeline_id: int = typer.Argument(None, envvar="PIPELINE_ID")):
    """
    Cancels all the deployment jobs of the specified pipeline when called from
    the CI using the $ABORT_DEPLOYMENT_TRIGGER and the $PIPELINE_ID.

    Or when called from local using `./grove abortpipeline <pipeline_id>`
    """
    if pipeline_id:
        typer.echo(f"Aborting the pipeline {pipeline_id}")
        stopped = abort_pipeline(pipeline_id)

        if stopped:
            typer.secho("The pipeline was aborted successfully", fg=typer.colors.GREEN)
        else:
            raise typer.Abort()
    else:
        typer.echo("Pipeline ID is required to abort deployment")
        raise typer.Abort()


@app.command()
def archive(instance_name: str):
    """
    Archive an instance by removing its namespace, but keep infrastructure components.
    """

    try:
        instance = Instance(instance_name)
        instance.archive()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def predeploy(instance_name: str):
    """
    Perform steps to be done before image build and deployment.
    Ex - clone custom theme, override scss, etc.
    """
    try:
        instance = Instance(instance_name)
        instance.pre_deploy()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def deploy(instance_name: str):
    """
    Deploy an Open edX instance
    """
    typer.echo(f"Deploying instance {instance_name}")
    try:
        instance = Instance(instance_name)
        instance.deploy()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@app.command()
def postdeploy(instance_name: str):
    """
    Do post-deployment work, ex: enable the theme, update images, etc.
    """
    typer.echo(f"Deploying instance {instance_name}")
    try:
        instance = Instance(instance_name)
        instance.post_deploy()
    except Exception as error:
        typer.echo(error)
        raise typer.Abort() from error


@tutor_app.command()
# pylint: disable=redefined-builtin
def exec(
    instance_name: str,
    command_str: str,
    format: str = typer.Option("string", help="Format of command_str. JSON supported."),
):
    """
    Runs any tutor command.

    Supports passing command_str as JSON string. This helps escaping different characters
    while running from Grove ruby wrapper.
    """
    instance = Instance(instance_name)
    if format == "json":
        command_str = json.loads(command_str)
    instance.tutor_env_pull()
    instance.tutor_save_config()
    instance.tutor_env_override()
    instance.tutor_command(command_str)
    instance.tutor_env_push()


@tutor_app.command()
def sync(instance_name: str):
    """
    Build and sync tutor env directory with s3
    """
    instance = Instance(instance_name)
    instance.tutor_env_sync()


@tutor_app.command()
def buildimage(
    instance_name: str,
    image_name: str,
    edx_platform_repository: Optional[str] = None,
    edx_platform_version: Optional[str] = None,
    cache_from: Optional[List[str]] = typer.Option([], help="Use cache from provided images."),
    cache_to: Optional[List[str]] = typer.Option([], help="The location to cache images to."),
):
    """
    Build an image using Tutor.
    """
    if not check_docker_is_running():
        typer.echo("Docker is not running / failed to reach it within this container.")
        raise typer.Abort()
    instance = Instance(instance_name)
    instance.tutor_env_pull()
    instance.clone_private_plugins()

    tutor_cache_from_args = []
    tutor_cache_to_args = []
    for cache_image in cache_from:
        tutor_cache_from_args.append(f'--docker-arg="--cache-from" --docker-arg="{cache_image}"')

    for cache_image in cache_to:
        tutor_cache_to_args.append(f'--docker-arg="--cache-to" --docker-arg="{cache_image}"')

    if instance.tutor_version() >= 16:
        build_args = ['--output="type=registry"']
    else:
        build_args = ['--docker-arg="--push"']
    # Make sure we always build amd64 images, even if we're on an ARM host machine:
    build_args.append('--docker-arg="--platform" --docker-arg="linux/amd64"')

    if edx_platform_repository:
        build_args.append(f'--build-arg EDX_PLATFORM_REPOSITORY="{edx_platform_repository}"')

    if edx_platform_version:
        build_args.append(f'--build-arg EDX_PLATFORM_VERSION="{edx_platform_version}"')

    tutor_cache_from_arg = " ".join(tutor_cache_from_args)
    tutor_cache_to_arg = " ".join(tutor_cache_to_args)
    platform_build_args = " ".join(build_args)
    tutor_command = f"images build {image_name} {tutor_cache_to_arg} {tutor_cache_from_arg} {platform_build_args}"
    try:
        instance.tutor_command(tutor_command)
    except CommandFailedError as err:
        if "Please switch to a different driver" in "\n".join(err.output):
            typer.echo("Buildx builder is missing; trying to force creation of buildx driver")
            # On MacOS for some reason, the buildx builders from the host aren't available within the container, so
            # we have to create a new one now, within this temporary container.
            execute("docker buildx create --bootstrap --use")
            # Then we can try again:
            instance.tutor_command(tutor_command)
        else:
            raise err


def exec_pipeline_command(command: str, env: None):
    if env is None:
        env = {}

    env["CI_REGISTRY"] = os.getenv("CI_REGISTRY_IMAGE")
    env["CI_SERVER_HOST"] = os.getenv("CI_SERVER_HOST", "gitlab.com")
    env["CI_REGISTRY_USER"] = os.getenv("GITLAB_USERNAME")
    env["CI_REGISTRY_PASSWORD"] = os.getenv("GITLAB_PASSWORD")

    vars_path = const.TOOLS_CONTAINER_DIR / "ci_vars.yml"
    with vars_path.open("r", encoding="utf8") as vars_file:
        ci_vars = yaml.safe_load(vars_file)["variables"]

    ci_vars["SCRIPTS_DIR"] = str(const.SCRIPTS_DIR)
    ci_vars["TOOLS_CONTAINER_DIR"] = str(const.TOOLS_CONTAINER_DIR)

    ci_vars.update(env)
    return execute(command, ci_vars)


@run_pipeline_app.command("build-instance-image")
def build_instance_image_pipeline(instance_name: str, image_name: str):
    """
    Runs the pipeline to build and push an Open edX image.

    Can be used to precache an image or to test locally.
    """
    env = {"INSTANCE_NAME": instance_name}
    instance = Instance(instance_name)
    build_config = dict(instance.instance_images()).get(image_name)
    image_pipeline = OpenedXPipeline([instance])
    image_job = image_pipeline.build_image_job(instance, image_name, build_config, [f"Prepare {image_name}"])
    env.update(image_job["variables"])
    return exec_pipeline_command(str(const.CI_SCRIPTS_DIR / "build-instance-image.sh"), env)


@run_pipeline_app.command("build-tools-container")
def build_tools_container_pipeline(default_tag: str):
    """
    Runs the pipeline to build and push the tools-container image
    tagged with the value provided in default_tag.
    """
    vars_path = const.TOOLS_CONTAINER_DIR / "ci_vars.yml"
    with vars_path.open("r", encoding="utf8") as vars_file:
        ci_vars = yaml.safe_load(vars_file)["variables"]

    ci_registry_image = os.environ["CI_REGISTRY_IMAGE"]
    tools_container_image_name = ci_vars["TOOLS_CONTAINER_IMAGE_NAME"]
    tools_container_image_version = ci_vars["TOOLS_CONTAINER_IMAGE_VERSION"]

    env = {
        "IMAGE_NAME": f"{ci_registry_image}/{tools_container_image_name}",
        "DEFAULT_TAG": default_tag,
        "LATEST_TAG": "latest",
        "VERSIONED_TAG": f"{tools_container_image_version}",
        "DOCKER_BUILDKIT": 1,
    }

    return exec_pipeline_command(str(const.CI_SCRIPTS_DIR / "build-tools-container.sh"), env)


if __name__ == "__main__":
    app(prog_name="grove")
