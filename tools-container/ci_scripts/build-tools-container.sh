#!/usr/bin/env bash
set -eou pipefail

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
cd $TOOLS_CONTAINER_DIR

docker build --build-arg BUILDKIT_INLINE_CACHE=1 --build-arg TERRAFORM_VERSION="${TERRAFORM_VERSION}" --build-arg GITLAB_TERRAFORM_IMAGES_VERSION="${GITLAB_TERRAFORM_IMAGES_VERSION}" --build-arg KUBECTL_VERSION="${KUBECTL_VERSION}" --build-arg HELM_VERSION="${HELM_VERSION}" --build-arg VELERO_VERSION="${VELERO_VERSION}" --cache-from $IMAGE_NAME:$LATEST_TAG -t $IMAGE_NAME:$DEFAULT_TAG .
docker push $IMAGE_NAME:$DEFAULT_TAG
