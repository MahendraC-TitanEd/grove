#!/usr/bin/env bash
# This is a wrapper for gitlab-terraform


set -e
set -u

WORKSPACE=${CI_PROJECT_DIR:-/workspace}

# get space seperated string of instances
cd "$WORKSPACE/instances/"
instance_dirs=$(find * -maxdepth 0 -printf "\"%p\", ")

# export required instances list variable for terraform
export TF_VAR_tutor_instances="[${instance_dirs::-2}]"

# re-export aws credentials for terraform if possible to avoid requiring them
# to be set twice
export TF_VAR_aws_access_key=${AWS_ACCESS_KEY_ID:-}
export TF_VAR_aws_secret_access_key=${AWS_SECRET_ACCESS_KEY:-}

# ensure that additional, custom terraform resource definitions are copied
if [ -d "$WORKSPACE/infrastructure" ]; then
    git config --global --add safe.directory "$WORKSPACE"
    git clean -df "$TF_ROOT/../provider-plugin/"
    cp -R $WORKSPACE/infrastructure/* "$TF_ROOT/../provider-plugin/"
fi

# move to TF_ROOT and use gitlab-terraform
cd "$TF_ROOT" && gitlab-terraform "$@"
