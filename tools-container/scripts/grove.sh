#!/usr/bin/env bash
# This is a wrapper for grove CLI


set -e
set -u

PYTHONPATH=$CI_PROJECT_DIR/grove/tools-container/grove-cli/ python3 $CI_PROJECT_DIR/grove/tools-container/grove-cli/bin/cli.py "$@"
