# Infrastructure Pipeline

Infrastructure pipeline triggers terraform run for the configured cloud provider.

## Use case

Infrastructure creation can be done [locally](/user-guides/working-locally), though that requires more setup and depends on the user's machine. To avoid this dependency and fully automate cluster creation, you can use the `[AutoDeploy][Infrastructure] ...` commit message.

## Commit message

The following commit message pattern is used to trigger infrastructure creation or updates.

`[AutoDeploy][Infrastructure] <ANY COMMIT MESSAGE>`

This commit message does not require any specific format following the `[AutoDeploy][Infrastructure]` prefix.

Example commit message:

`[AutoDeploy][Infrastructure] Create new cluster`

!!! warning

    Remember, you allowed resource creation, update, and deletion permissions for your repository. Make sure you reviewed the changes in the `grove` submodule before you apply any infrastructure changes.
