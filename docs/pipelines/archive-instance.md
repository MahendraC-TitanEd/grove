# Archive Instance Pipeline

Archive instance pipeline triggers a new instance archiving.

## Use case

Instance archiving can be done [locally](../user-guides/working-locally), though that requires more setup and depends on the user's machine. To avoid this dependency and fully automate instance creation, you can use the `[AutoDeploy][Archive] ...` commit message.

## Commit message

The following commit message pattern is used to trigger instance archiving for a single instance.

`[AutoDeploy][Archive] <INSTANCE NAME>`

The arguments of the pipeline:

* `<INSTANCE NAME>` - The name of the instance to archive

Example commit message:

`[AutoDeploy][Archive] opencraft-courses`

!!! note

    Using whitespaces and special character other than hyphens (`-`) and underscores (`_`) will result in deployment errors.

!!! warning

    The archive and corresponding create pipelines are special pipelines that are triggered by a script and requires additional CI variable existence. The expected way of calling the create and archive pipelines is calling a GitLab pipeline trigger. 

    In order to let the CI archive the instance and commit the changes, set the following at pipeline run, or use pipeline trigger parameters:
    
    - `ARCHIVE_INSTANCE_TRIGGER` to any value
    - `INSTANCE_NAME` to the name of the instance

    This means that both commit message and CI variables must contain the same instance name.
