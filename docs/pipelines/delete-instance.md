# Delete Instance Pipeline

Delete instance pipeline triggers a new instance removal.

## Use case

To fully automate instance creation, you can use the `[AutoDeploy][Delete] ...` commit message.

## Commit message

The following commit message pattern is used to trigger instance delete for a single instance.

`[AutoDeploy][Delete] <INSTANCE NAME>`

The arguments of the pipeline:

* `<INSTANCE NAME>` - The name of the instance to delete

Example commit message:

`[AutoDeploy][Delete] opencraft-courses`

!!! note

    Using whitespaces and special character other than hyphens (`-`) and underscores (`_`) will result in deployment errors.

!!! warning

    The delete and corresponding create pipelines are special pipelines that are triggered by a script and requires additional CI variable existence. The expected way of calling the create and archive pipelines is calling a GitLab pipeline trigger.

    In order to let the CI archive the instance and commit the changes, set the following at pipeline run, or use pipeline trigger parameters:

    - `DELETE_INSTANCE_TRIGGER` to any value
    - `INSTANCE_NAME` to the name of the instance

    This means that both commit message and CI variables must contain the same instance name.
