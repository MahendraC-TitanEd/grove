# Build Image Pipeline

Build image pipeline triggers an image build for the given git repository and branch, tag, or commit hash.

Although images are built during instance deployment, there are multiple scenarios when a Grove operator would like to pre-build an image. Pre-building images not just improves first time builds and deployments, but it allows easier testing and continuous security releases.

## Commit message

The following commit message pattern is used to trigger an image build for a single image build.

`[AutoDeploy][Build][Image] <EDX_PLATFORM_REPOSITORY>|<EDX_PLATFORM_VERSION>`

To build multiple images with a single commit message, list the `<EDX_PLATFORM_REPOSITORY>|<EDX_PLATFORM_VERSION>` pairs, separated by comma.

`[AutoDeploy][Build][Image] <EDX_PLATFORM_REPOSITORY>|<EDX_PLATFORM_VERSION>,<EDX_PLATFORM_REPOSITORY>|<EDX_PLATFORM_VERSION>,...`

The arguments of the pipeline:

* `<EDX_PLATFORM_REPOSITORY>` - Git repository URL
* `<EDX_PLATFORM_VERSION>` - Git branch, tag, commit hash that Grove and Tutor checks out during image build

Example commit message:

`[AutoDeploy][Build][Image] https://github.com/edx/edx-platform.git|open-release/maple.3`

## Working locally

When working locally, the command can be invoked with `grove`:

```bash
./grove run-pipeline build-instance-image [instance-name] [image-name]
```
