# Automated PR Sandbox

The `pr_watcher` [OpenFAAS](https://grove.opencraft.com/addons/openfaas/) function in Grove automatically provisions sandboxes for PRs created by authorized users in the configured repositories. This function is triggered periodically every 15 minutes and can also be triggered using Github PR comments (through webhooks).

Here are some of the things this function does:

1. Periodically checks for new PRs created by authorized users in watched repos and provisions sandboxes for them.
2. Updates PR sandboxes if changes are detected in the PR.
3. Notifies the status of sandbox provisioning as comments on the PR.
4. Enables PR authors to configure their sandbox using the PR description.
5. Enables authorized users to manually trigger sandbox create, update or destroy by posting commands as comments on the PR.
6. Automatically destroys the sandbox when a PR is closed or merged.

This guide is intended to help PR authors understand the automated sandbox workflow and show how to configure and interact with the sandbox and with the `pr_watcher` function.

## Watched Repositories

There are two ways to configure watched repositories with this function:

1. Individual forks can be configured directly with the function during deployment using the `EDX_WATCHED_FORKS` and `MFE_FORKS` variables.
2. Forks can be configured dynamically using a pre-defined topic added to those repos that need to be watched for a given set of orgs. The `GITHUB_WATCHED_TOPIC` variable is used to configure the custom topic along with the `GITHUB_WATCHED_ORGS` variable to configure which orgs these repos would belong to.

We differentiate two different categories that can be configured with the `pr_watcher` function:

1. Forks of [edx-platform](https://github.com/openedx/edx-platform) repo. Sandboxes for PRs from these forks have the deployment branch (`EDX_PLATFORM_REPOSITORY`/`EDX_PLATFORM_VERSION`) configured as the PR source repo/branch.
2. Forks for MFEs. Sandboxes for PRs of these forks have the `edx-platform` [master](https://github.com/openedx/edx-platform) branch configured as the deployment branch by default unless [overridden](#custom-configuration) by the PR author. The PR source repo/branch is configured as a [custom MFE](https://github.com/open-craft/tutor-mfe#adding-new-mfes).

!!! warning

    For MFE forks configured using a custom Github topic, it's important that the repo name starts with `frontend-app-`. Otherwise, the function will treat it as an `edx-platform` fork. This assumption is based on the naming convention of MFEs.

## Authorized Users and Team

To prevent abuse of resources, only PRs from authorized users are considered for sandbox creation. Authorized users can either be configured individually or thorugh Github teams. All users belonging to these authorized teams would be treated as authorized users. By using the GitHub teams as the source of users, the user management can be dynamic.

Also, only authorized users are allowed to interact with sandboxes [using PR comments](#manual-trigger-commands).

For PRs authored by other users, any authorized user can trigger sandbox creation/update on their behalf, using the [manual trigger commands](#manual-trigger-commands).

## Manual Trigger Commands

A few commands are available to manually trigger certain actions with the `pr-watcher` function. An [authorized user](#authorized-users-and-team) can post these commands as comments in the concerned PR. Github webhooks are used to pass these comments to the `pr-watcher` function.

Here's the full list of commands accepted by the function:

* `/grove sandbox create` -- Triggers sandbox creation for the PR
* `/grove sandbox deploy` -- Alias for `/grove sandbox create`
* `/grove sandbox update` -- Triggers new deployment for an existing sandbox
* `/grove sandbox destroy` -- Triggers deletion of sandbox and destroy all resources related to it.
* `/grove sandbox remove` -- Alias for `/grove sandbox destroy`
* `/grove sandbox delete` -- Alias for `/grove sandbox destroy`

## Sandbox Configuration

### Default Configurations

Based on the target branch of a PR, the `pr_watcher` function tries to determine which named release of Open edX the PR is intended for. Certain things such as `OPENEDX_COMMON_VERSION` and the release version of Tutor and its plugins are configured based on this.

For a PR to be recognized as targting a named release, its target branch should be of the format

    <release-prefix>/<named_release>.<version>

`open-release/palm.master`, `opencraft-release/palm.1` are examples of valid names of target branches.

!!! note

    Recognized named releases are limited to `nutmeg`, `olive` and `palm`. Named releases older than `nutmeg` are not recognized.

If a named release cannot be determined from the target branch, then `OPENEDX_COMMON_VERSION` is set to `master` and the `nightly` version of Tutor and it's plugins are used. This assumption is applied in order to cover the vast majority of PRs. The `OPENEDX_COMMON_VERSION` can be overridden anytime in the PR sandbox configuration upon conflicts.

### Custom Configuration

PR authors can either override existing default configurations or set other configurations values for their sandbox directly from the PR body.

The custom configurations are of two different types:

#### 1. Tutor Configurations

Configurations passed directly to tutor and its plugins, such as `EDX_PLATFORM_REPOSITORY`, `OPENEDX_EXTRA_PIP_REQUIREMENTS`, `SITE_CONFIG` etc.

These configuration settings need to be added to the PR body in the following format:

    **Settings**

    ```yaml
    EDX_PLATFORM_REPOSITORY: https://github.com/openedx/edx-platform.git
    EDX_PLATFORM_VERSION: master

    OPENEDX_EXTRA_PIP_REQUIREMENTS:
    - dnspython
    - openedx-scorm-xblock<13.0.0,>=12.0.0

    SITE_CONFIG:
      version: 0
    ```

#### 2. Grove Configurations

These configuration settings are used by Grove itself to configure the sandbox instances, such as `TUTOR_GROVE_COMMON_SETTINGS`, `TUTOR_GROVE_COMMON_ENV_FEATURES`, `TUTOR_GROVE_WAFFLE_FLAGS`, etc. The plugin configuration is described in [Grove's Tutor plugin](https://gitlab.com/opencraft/dev/tutor-contrib-grove).

Similar to tutor configurations, these can be added to the PR body under `Settings` and can be mixed with other tutor configurations. Example:

    **Settings**

    ```yaml
    EDX_PLATFORM_REPOSITORY: https://github.com/openedx/edx-platform.git
    EDX_PLATFORM_VERSION: master

    GROVE_COMMON_ENV_FEATURES: |
      ASSUME_ZERO_GRADE_IF_ABSENT_FOR_ALL_TESTS: true

    GROVE_LMS_ENV: |
      REGISTRATION_VALIDATION_RATELIMIT: "100/s"
      REGISTRATION_RATELIMIT: "100/s"
      RATELIMIT_RATE: "100/s"
    ```

#### 3. Requirements

These are used to set the release-version/branch of Tutor and it's plugins to be used with the sandbox. Although the requirements below are necessary for the PR sandboxes, the PR author could add any other tutor plugins that is needed for the given PR. For example, if a new plugin is developed.

These can be added to the PR body in the following format:

    **Tutor requirements**

    ```txt
    git+https://github.com/overhangio/tutor.git@nightly
    git+https://github.com/overhangio/tutor-discovery.git@nightly
    git+https://github.com/overhangio/tutor-ecommerce.git@nightly
    git+https://github.com/overhangio/tutor-mfe.git@nightly
    git+https://github.com/overhangio/tutor-xqueue.git@nightly
    git+https://github.com/overhangio/tutor-forum.git@nightly
    git+https://gitlab.com/opencraft/dev/tutor-contrib-grove.git@main
    git+https://github.com/hastexo/tutor-contrib-s3.git@main
    ```

### Enabling/Disabling Sandboxes

The PR sandbox management is enabled for every non-draft PR that is configured for the `pr_watcher`. However, there are scenarios when a non-draft PR should not have sandboxes or draft PRs should have sandboxes.

For these scenarios, the PR watcher is looking out for a `GROVE_AUTOMATIC_SANDBOX_ENABLED` key in the settings section of the PR body. This variable controls whether a sandbox should be created/updated/deleted for the given PR.

By setting the variable to `True` or `False`, one can override the decision of managing/not managing sandboxes.

Example to set a non-draft PR ignored:

    **Settings**

    ```yaml
    GROVE_AUTOMATIC_SANDBOX_ENABLED: False
    ```

Example to set a draft PR watched:

    **Settings**

    ```yaml
    GROVE_AUTOMATIC_SANDBOX_ENABLED: True
    ```

### Notifications

The `pr_watcher` function posts notifications as comments in the PR to notify PR authors about the success/failure status of the sandbox :

1. If deployment succeeds, then the LMS URL, Studio URL, current instance configs and current instance requirements are posted along with the success notification
2. If deployment fails, then the failure logs, current instance configs and current instance requirements are posted with the failure notification.

Apart from these, a notification is also posted when the sandbox destroy pipeline is triggered.

### Destroying IDLE Sandboxes

The sandboxes, especially if the PRs are lasting for many weeks, can consume huge amount of resources. This is mitigated by destroying the sandboxes after a period of time. This time is set to 14 days by default. When PRs are reviewed and merged relatively in a timely manner, it is enough. However, it may happen that the team needs more time in general to review PRs.

To extend (or shorten) this cut off deadline, you can set the `SANDBOX_DESTROY_CUTOFF_DAYS` environment variable when deploying the PR watcher. Note that the automatic destroy cannot be turned off nor extended per sandbox.
