# GitLab Runners

Grove provides helper scripts to start private GitLab runners using Terraform.

!!! note

    GitLab Runners are the only resources not supported by both AWS and DigitalOcean. It does not utilize the wrapper scripts under the `control` directory and, in addition, uses a separate Terraform state file.

    Treat GitLab Runners as a standalone collection of scripts that are packaged with Grove for easier use, rather than as part of Grove itself.

    Setting up GitLab runners is only available on AWS at the moment.

## Prerequisites

* Docker installation
* GitLab runner registration token obtained from your repository's CI/CD settings
* GitLab username and belonging Private Access Token
* AWS Access Key ID and Secret Access Key

## Usage

Before running any of the commands below, ensure the following environment variables are set and docker is running.

``` shell
export AWS_ACCESS_KEY_ID="<YOUR AWS ACCESS KEY ID>"
export AWS_SECRET_ACCESS_KEY="<YOUR AWS SECRET ACCESS KEY>"
export GITLAB_PROJECT_ID="<YOUR GITLAB PROJECT ID>"
export GITLAB_USERNAME="<YOUR GITLAB USERNAME>"
export GITLAB_ACCESS_TOKEN="<YOUR GITLAB ACCESS TOKEN>"
export GITLAB_REGISTRATION_TOKEN="<GITLAB TOKEN FOR REGISTERING CI RUNNERS>"
```

If you would like to change the Grove environment's name, set the following in addition to the previous exports:

``` shell
export GROVE_ENVIRONMENT="<YOUR ENVIRONMENT>"
```

Do not include `grove` as a prefix to your variables as it is automatically added by the terraform scripts. For example, setting `GROVE_ENVIRONMENT="digitalocean-staging` will create a `grove-digitalocean-staging` EC2 instance.

### Initialize terraform

Before the first usage, terraform must be initialize. To initialize terraform, run:

``` shell
# Change directory to the appropriate directory
cd ./gitlab-ci/aws-runners

# Initialize terraform
./scripts/terraform.sh init
```

### Creating runners

To create GitLab runners on AWS, run the following within the `gitlab-ci` directory:

``` shell
# Check the plan output before applying it
./scripts/terraform.sh plan

# Apply the plan if you find everything correct
./scripts/terraform.sh apply
```

!!! warning
    There is an [existing bug](https://github.com/npalm/terraform-aws-gitlab-runner/issues/591) in the runner where the first run will fail with the error below. If encountered, run the terraform plan and apply again.

    ```shell
    │ Error: Error putting IAM role policy grove-kgaws-gitlab-runners-logging: NoSuchEntity: The role with name grove-name-gitlab-runners-instance cannot be found.
    │       status code: 404, request id: c11ac2f9-5bef-41d8-96cc-97adff59ecf5
    │
    │   with module.gitlab-runner.aws_iam_role_policy.instance[0],
    │   on .terraform/modules/gitlab-runner/logging.tf line 1, in resource "aws_iam_role_policy" "instance":
    │    1: resource "aws_iam_role_policy" "instance" {

    ```

### Destroying runners

To destroy runners, simple run the following within the `gitlab-ci` directory:

``` shell
./scripts/terraform.sh destroy
```

### Accessing runners

To access a runner for debugging purposes, you can use AWS Session Manager.
