# Setup redirection using Caddy

Grove supports setting up URL redirections through Caddy via the `GROVE_REDIRECTS` setting in each instance's `config.yml`.

This uses Caddy's [redir directive](https://caddyserver.com/docs/caddyfile/directives/redir)

`GROVE_REDIRECTS` is a list of dictionaries with the following keys.

- `domain` can be any domain, not necessarily a subdomain of the LMS/CMS. Set up the DNS to redirect to your cluster. This is mandatory.
- `rules` is a list of dictionaries defining the redirect rules to be setup for this domain. This key is mandatory. Each dictionary listed here can have the following keys:
  - `to` is used to set the target location. Becomes the response's Location header. This can be just a path such as `/example` or a full url such as `https://example.com`. This is a mandatory key.
  - `path` This is an optional key. If set the redirect rule will apply to only this particular path. If not set, then this rule will apply to all paths of the domain.
  - `code` Defaults to `301` if nothing provided. Can be used to set the HTTP status code to use for the redirect.

Here's a sample usage of the `GROVE_REDIRECTS` setting.

```yaml
GROVE_REDIRECTS:
- domain: lms.example.net
  rules:
    - to: https://example.org
      code: 302
- domain: lms.example.com
  rules:
    - path: /account
      to: /dashboard
    - path: /u/*
      to: /dashboard
```
