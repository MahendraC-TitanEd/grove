# Cluster backups

The cluster backups are not optional. They are required for the cluster to recover from a disaster. The backups are done by [Velero](https://velero.io/) that is meant for backing up Kubernetes resources.

The backup system is configured to backup all Kubernetes resources including volumes to an S3 compatible storage. Depending on the provider, the backups are stored in AWS S3 or DigitalOcean Spaces. The bucket is created automatically by the cluster setup.

The backups are done hourly, daily and weekly and are kept respectively for 24 hours, 7 days (168h) and 30 days (720h).

## Database backups

However we backup the Kubernetes cluster and cluster resources, nothing is backed up outside of the cluster. The managed services should define their own backup strategy. For example, the MongoDB Atlas database is backed up by MongoDB Atlas.

## Interacting with Velero

The backups are restored using the [Velero CLI](https://velero.io/docs/) located in the `control` directory. To interact with Velero CLI, use the `./control/velero` script.

## Manual backups and restores

This guide is not meant to be used for manual backups and restores. It is only meant to describe the backup system. For manual backups and restores, please refer to the [Velero documentation](https://velero.io/docs/).
