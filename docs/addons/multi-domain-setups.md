# Multi-domain setups

Multi-domain setup can be achieved by using:

- [Site Configuration](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/sites/configure_site.html), which allows us to set configuration properties independently for individual sites, and
- [eox-tenant](https://github.com/eduNEXT/eox-tenant), which allows us to override django settings for individual tenant/site.

## Using only site-configuration

To setup site-configuration, set `GROVE_ADDITIONAL_DOMAINS` setting in each instance's `config.yml`.

`GROVE_ADDITIONAL_DOMAINS` is a list of dictionaries with the keys `domain` and `proxy`.

- `domain` can be any domain, not necessarily a subdomain of the LMS/CMS. Set up the DNS to redirect to your cluster.
- `proxy` is of the form of the form `{container_name}:{port}`. The provided value will be inserted into your instance's Caddyfile using tutor's [`proxy`](https://github.com/overhangio/tutor/blob/master/tutor/templates/apps/caddy/Caddyfile#L18) directive.
- `site_configuration`: A dictionary containing the keys expected of the [Site Configuration](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/configuration/sites/configure_site.html). For a proxy value of `lms:8000` Grove will generate a default `Site Configuration` if not specified.

To wit, the below configuration will direct both `example.com` and `example.net` to your LMS. While `studio.example.com` and `studio.example.net` will direct to your Studio backend.

```yaml
LMS_HOST: example.com
CMS_HOST: studio.example.com
GROVE_ADDITIONAL_DOMAINS:
- domain: example.net
  proxy: lms:8000
  site_configuration:
      SITE_NAME: example.net
      SESSION_COOKIE_DOMAIN: example.net
- domain: studio.example.net
  proxy: cms:8000
```

!!! warning
    Using only `site-configuration` provides limited support for multi-domain setup, as it is not guaranteed that all features will be site configuration aware.

**Caveats**:

- Studio preview mode depends on [FEATURES\["PREVIEW_LMS_BASE"\]](https://github.com/open-craft/edx-platform/blob/8509194316dd94cbdffd4c79ab20c1cfb34a9851/lms/djangoapps/courseware/access_utils.py#L96-L102) and [HOSTNAME_MODULESTORE_DEFAULT_MAPPINGS](https://github.com/openedx/edx-platform/blob/16955828e7dc88322cfaaef5f95a5daa3816e84e/lms/envs/production.py#L424-L431) settings, which are not site configuration aware. Although we can set `PREVIEW_LMS_BASE` via site configuration and `course_org_filter` like so:

  ```json
  {
      "SESSION_COOKIE_DOMAIN": "lacolhost.com",
      "SITE_NAME": "lacolhost.com",
      "PREVIEW_LMS_BASE": "preview.lacolhost.com",
      "LMS_BASE": "lacolhost.com",
      "course_org_filter": [
          "edX"
      ]
  }
  ```

  This sets the `Preview` link to the correct link, but the site displayed is not in preview mode, i.e., we can only see published content.

- [SOCIAL_AUTH_EDX_OAUTH2_PUBLIC_URL_ROOT](https://github.com/open-craft/edx-platform/blob/8509194316dd94cbdffd4c79ab20c1cfb34a9851/docs/guides/studio_oauth.rst?plain=1#L35-L35) is not site configuration aware. So all studio sites will be redirected to a single LMS URL for login as well as redirected to the same URL on logout.
- The `View in Studio` button depends on the [settings.CMS_BASE](https://github.com/open-craft/edx-platform/blob/8509194316dd94cbdffd4c79ab20c1cfb34a9851/lms/templates/preview_menu.html#L39-L39) variable which is not also site configuration aware. So all sites will direct to the same studio URL.

## Using [eox-tenant](https://github.com/eduNEXT/eox-tenant)

This plugin allows us to override Django settings for individual tenants, which means we can also override configuration variables that are not site-configuration aware. **This method overcomes the caveats mentioned in the above option.**

!!! warning
    `Site-Configuration` is also overridden by this plugin by [proxying](https://github.com/open-craft/eox-tenant/blob/7ccb97c5be238c3db264692c75c72070755afea7/eox_tenant/tenant_wise/proxies.py#L60-L70) the model. It fetches the configuration value from tenant settings. As tenant settings inherit all base settings, we can add settings common to all tenants under `GROVE_LMS_ENV` or `GROVE_CMS_ENV` depending on the environment.

To setup `eox-tenant`, set `GROVE_USE_EOX_TENANT` variable to `true` and set `GROVE_ADDITIONAL_DOMAINS` as shown in the example below:

```yaml
LMS_HOST: example.com
CMS_HOST: studio.example.com
GROVE_ADDITIONAL_DOMAINS:
- domain: example.net
  external_key: example.net
  proxy: lms:8000
  site_configuration:
    PLATFORM_NAME: example.net
    SITE_NAME: example.net
- domain: studio.example.net
  external_key: example.net
  proxy: cms:8000
  site_configuration:
    PLATFORM_NAME: example.net Studio
- domain: university.example.com
  external_key: university.example.com
  proxy: lms:8000
- domain: studio.university.example.com
  external_key: university.example.com
  proxy: cms:8000
```

The `external_key` is a unique identifier for a tenant configuration, which is required to link it to a route. In the above example, we have two additional tenants. `example.net` with external_key as `example.net` (note this can be set to any unique string) and it is linked to two routes, LMS (example.net) and studio (studio.example.net). Similarly, `university.example.net` is also setup with external_key as `university.example.com` and it is linked to two routes, LMS (university.example.com) and studio (studio.university.example.com).

The configuration under LMS or CMS is set separately for each environment, for example, in the above shown configuration, the `PLATFORM_NAME` for `example.net` LMS and CMS is set to different values.
