# Instance Backups

At the moment, we are supporting only the AWS deployments. Therefore, we are going to use the AWS backup service,
as described in [this document] (the link is private).

[this document]: https://private.doc.opencraft.com/ops/backups/

## AWS

### RDS

RDS backups are on by default. We are [already allowing the configuration] of [the retention period], but we should make
it default to the max value (35).

[already allowing the configuration]: https://gitlab.com/opencraft/dev/grove/-/blob/611dcda16f152941f66508a1dba9fdbdb8d52030/provider-aws/mysql/variables.tf#L18
[the retention period]: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance#backup_retention_period

### MongoDB Atlas

We [are already supporting cloud backups] with MongoDB Atlas in our Terraform scripts. In addition to this, we should
also set up [the backup schedule] and keep its [restore window] in sync with [the retention period] from AWS RDS.

[are already supporting cloud backups]: https://github.com/open-craft/terraform-scripts/blob/53d4504fa0e03ee8ef3758d45278105df028a359/modules/openedx/mongodb/variables.tf#L84-L94
[the backup schedule]: https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/cloud_backup_schedule
[restore window]: https://registry.terraform.io/providers/mongodb/mongodbatlas/latest/docs/resources/cloud_backup_schedule#restore_window_days

### S3

We have already done a discovery about this over 3 years ago - [BB-182] (private link).
The results are described in [this discovery document]. We have decided to use the bucket versioning for S3 backups.
The approach hasn't changed too much since then, and we have been using it with Ocim for almost 3 years.
This approach protects the data only from accidental deletion and modification.

#### How the bucket versioning works

* When we add an object (via PUT, POST, or COPY), S3 adds a unique ID that determines its version:  
![S3 versioning PUT request](images/s3_versioning_put.png)
* When we GET the object with the specific key, the one with the most recent version is returned:  
![S3 versioning GET request](images/s3_versioning_get.png)
* When we DELETE an object, a "Delete Marker" is added as the current version of that object; the previous version becomes the non-current version:  
![S3 versioning DELETE request](images/s3_versioning_delete.png)
* "Delete Markers" are placeholders with their own Key and ID. If we DELETE an object that has the "Delete Marker" on its "top", a new marker will be added:  
![S3 versioning double DELETE](images/s3_versioning_double_delete.png)

#### How to restore overwritten objects

* COPY (duplicate) the object:  
![S3 versioning restore COPY](images/s3_versioning_restore_copy.png)
* DELETE all newer versions (or "Delete Markers", but they can be deleted only by the bucket owner):  
![S3 versioning restore DELETE](images/s3_versioning_restore_delete.png)

For now, we are going to use the following lifecycle rules for data cleanup (we will [set them up with Terraform]):

```xml
<LifecycleConfiguration>
    <Rule>
       <ID>Clean up</ID>
        <Filter>
          <Prefix></Prefix>
        </Filter>
        <Status>Enabled</Status>
        <Expiration>
           <ExpiredObjectDeleteMarker>true</ExpiredObjectDeleteMarker>
        </Expiration>
        <NoncurrentVersionExpiration>
            <NoncurrentDays>30</NoncurrentDays>
        </NoncurrentVersionExpiration>
    </Rule>
</LifecycleConfiguration>
```

If we want archive old file versions, we can set up AWS Glacier transitions with the following rules.
However, we need to remember that this is not going ot be a full backup, but only an archive of old file versions.

```xml
<LifecycleConfiguration>
   <Rule>
       <ID>Archive and clean up</ID>
       <Filter>
          <Prefix></Prefix>
       </Filter>
       <Status>Enabled</Status>
       <Expiration>
           <ExpiredObjectDeleteMarker>true</ExpiredObjectDeleteMarker>
        </Expiration>
       <NoncurrentVersionTransition>
           <NoncurrentDays>30</NoncurrentDays>     
           <StorageClass>STANDARD_IA</StorageClass>  
       </NoncurrentVersionTransition>
       <NoncurrentVersionTransition>
           <NoncurrentDays>60</NoncurrentDays>     
           <StorageClass>GLACIER</StorageClass>  
       </NoncurrentVersionTransition>
       <NoncurrentVersionExpiration>
           <NoncurrentDays>90</NoncurrentDays>
       </NoncurrentVersionExpiration>
   </Rule>
</LifecycleConfiguration>
```

[BB-182]: https://tasks.opencraft.com/browse/BB-182
[this discovery document]: https://docs.google.com/document/d/13lbG5rgxMCdIzocl7jcjUtKZQNTSBBXhoyLMGzD4Jn0
[set them up with Terraform]: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#lifecycle_rule

#### Potential future improvements

Amazon [has recently announced] an alternative way of backing up S3 data. [AWS Backup] is a centralized backup solution.
It already supports RDS through its native database snapshots. However, the S3 support is currently in the Preview
version and works in the US West region. Therefore, it doesn't make sense to look into this now, but it could be a way
of making full backups of S3 buckets in the future.

[has recently announced]: https://aws.amazon.com/about-aws/whats-new/2021/11/aws-backup-amazon-s3-backup/
[AWS Backup]: https://docs.aws.amazon.com/aws-backup/latest/devguide/whatisbackup.html

## DigitalOcean

### Spaces

DigitalOcean's Spaces [support lifecycle rules], just like AWS S3 buckets. We can reuse the proposed configuration here.

Note: Spaces do not support version-related transitions at the moment, so archiving old versions is not possible.

[support lifecycle rules]: https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/spaces_bucket#lifecycle_rule

### MySQL

Managed MySQL instances are [backed up] daily, with 7-day retention. This is not configurable at the moment.

[backed up]: https://docs.digitalocean.com/products/databases/mysql/#mysql-limits

### MongoDB

Managed MongoDB instances are [also backed up] daily, with 7-day retention. This is not configurable at the moment.

[also backed up]: https://docs.digitalocean.com/products/databases/mongodb/#managed-database-cluster-features
