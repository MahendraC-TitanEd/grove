# Releases

## Use cases

1. Continuous delivery.
2. Deployment and operations.
3. Increase testability

## Current state

At the time of writing, we are not doing any releases for Grove, other than using a specific `tools-container` version. This version does not tell much about the given Grove release, hence it is only useful for pipelines, but not developers or operators. To know what changed between two `tools-container` versions, a developer must check the `ci_vars.yml`'s history, and step through every commit.

## Proposed solution

### Conventional commits

Although we follow conventional commits as much as necessary, having conventional commits for easy changelog maintenance is crucial. Since (in most cases) commits are done by humany, we should automate the validation of commit message formats.

The most used practice is including an additional step in the CI/CD system for running a commit message validator using [commitlint](https://commitlint.js.org/#/).

### Tagging

Following best practices, the bare minimum is having tagged versions for the repository. For tagging, it is expected to follow [semantic versioning](https://semver.org/). Having semantic versioning for tags and conventional commits for the reposiotry helps at later stages to automate changelog generation.

### Changelog

We should note all changes in a changelog file, called `CHANGELOG.md` as many other projects doing in the open-source community. A properly maintained changelog helps both the developers and users.

The recommended format for the changelog is detailed by [keepachangelog.com](https://keepachangelog.com/en/1.0.0/). As this became the defacto standard for changelogs, we should follow the recommendations described in v1.0.0 of the website.

When it comes to changelog maintenance, it can be cumbersome to keep a proper changelog, not missing any important commit and categorizing them properly (as described by keepachangelog). To make this work less error prone, there are tools for generating `CHANGELOG.md` from commit history, utilizing the conventional commit formatting to group the commits, complying to guidelines described by keepachangelog.

The most flexible tool for doing the job is [`git-cliff`](https://github.com/orhun/git-cliff), which is an open-source git plugin. The tool allows modifications for the generated changelog, providing the flexibility to format commit group entries as we would like to have them (ex: adding the commit hash as a reference to the given commit). Integrating `git-cliff` to our workflow is as simple as installing the tool and calling `git cliff > CHANGELOG.md` command.

### Release notes

Putting it simple, release notes is the relevant section of the changelog, extended with extra information about the release. This extra infromation is heavily depending on the given release, though it may contain migration information, extra notice about breaking changes and more.

Adding release note for GitLab repositories requires having `tags`. For more information about Releases on GitLab, please check GitLab's [Releases documentation](https://docs.gitlab.com/ee/user/project/releases/).

This is a manual job that should not be automated.

### Release cycles

The release cycles are depending on the development status of Grove. The release cycle frequency could be split into three parts:

1. Heavy development without a stable release, ie. `v1.0.0` (current state of Grove)
2. Heavy development after the first stable release
3. Maintenance mode (this will not happen soon)

These cycles should have different release cycles detailed below, though before reading on, highlight an important note: Grove is a git submodule that users could update at anytime, they are not necessarily depending on releases.

#### Heavy development without a stable release

This stage of the development is the most "noisy". Releasing a new version every major change could be overwhelming. Not just in terms of following the release workflow, but for users of Grove too.

As a solution to this, we would release a new version at the end of every sprint, but every month at least -- depending on what epic the sprint focused on.

#### Heavy development after the first stable release

When Grove reaches its first stable release, it means we can reliably spin up clusters and instances using all supported infrastructure proveders. Hence, it means we reached the production ready version of Grove.

Naturally, the development would continue in a varying pace, though breaking changes will be less frequent. At this stage we could release

#### Maintenance mode

We could fallback to a "one release per quarter" approach, which would contain tooling version upgrades and bug fixes. This stage will not happen soon in the development workflow.

_Note: In case of a major bug fix we wouldn't wait for the next release._

### Necessary changes in Grove

Grove uses Merge commits that are not complying to conventional commits. We should limit the merge strategies to "squash and merge". Of course, this needs us merging the merge requests with proper squashed commits. Otherwise we need to amend commit message after merging but before generating the changelog. This affects the commit parsing in Grove that should be adjusted too.

### Development and release workflow

Based on the above, the recommended development/release workflow is the following:

1. Work on a branch
2. Merge the branch with a conventional squash commit
3. Run `git-cliff` to update the `CHANGELOG.md`
4. Push `CHANGELOG.md` changes on the `main` branch
5. Cut a new release tag with `vX.X.X` formatting
6. Run `git-cliff` to update the `CHANGELOG.md`
7. Push `CHANGELOG.md` changes on the `main` branch
8. Push `vX.X.X` tag
9. Form "Release notes" from the relevant part of the `CHANGELOG.md` and add extra release information
10. Publish a new release on GitLab
11. Update `grove` submodule in the [grove-template](https://gitlab.com/opencraft/dev/grove-template) repository
12. Update `grove` submodule in OpenCraft and client clusters

It is expected to repeat the workflow from step 1 to step 4 until all release blockers are resolved.
